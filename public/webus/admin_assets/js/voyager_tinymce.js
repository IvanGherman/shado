function setImageValue(url){
    $('.mce-btn.mce-open').parent().find('.mce-textbox').val(url);
}

$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    init_visual_editor();

});


function init_visual_editor() {
    tinymce.init({
        menubar: false,
        selector:'textarea.richTextBox',
        skin: 'voyager',
        min_height: 350,
        branding: false,
        resize: 'vertical',
        plugins: 'autolink, lists, link, image, paste, code, youtube, giphy, table, textcolor',
        extended_valid_elements : 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
        document_base_url : site_url,
        relative_urls: false,
        paste_data_images: true,
        file_browser_callback: function(field_name, url, type, win) {

            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = path_absolute + '/media/frame-manager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Image";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        },
        toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table youtube giphy | code | shortcodes',
        // convert_urls: false,
        image_caption: true,
        image_title: true,
        setup : function(editor) {
            editor.on('change keyup', function () {

                var lang = localStorage.getItem('translatable_lang');

                var name_input = editor['id'];

                //var to_lang = $('input[name="translatable['+ name_input +'][' + lang + ']"], textarea[name="translatable['+ name_input +'][' + lang + ']"]');
                var to_lang = $('#translatable_'+ name_input +'_' + lang + '');
                //register to specific language input
                to_lang.val(editor.getContent());
            });
            //Custom buttons (webus/settings/buttonsTinyMce)
            tinyButtons(editor);
        }

    });
}
