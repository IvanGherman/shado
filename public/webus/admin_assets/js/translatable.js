translate_all();
function translate_all(repeatable=false) {

    if(repeatable){
        //init repeatable
        var lang = localStorage.getItem('translatable_lang');
        set_active_lang(lang);
        set_lang_to_input(lang);
        change_lang();
        translate_this();
    }

    $( document ).ready(function() {
        var lang = localStorage.getItem('translatable_lang');

        if(lang == null || lang == 'undefined'){
            var get_first_lang_from_list = $('.change_langs li:first-child').attr('data_lang');
            localStorage.setItem('translatable_lang', get_first_lang_from_list);

        }
    });

    $( window ).load(function(){

        var lang = localStorage.getItem('translatable_lang');

        if(lang == null || lang == 'undefined'){
            var get_first_lang_from_list = $('.change_langs li:first-child').attr('data_lang');
            localStorage.setItem('translatable_lang', get_first_lang_from_list);

        }

        //add active class to current lang from localStorage
        set_active_lang(lang);


        //display to input text of current languages
        set_lang_to_input(lang);


        change_lang();


        translate_this();

        //for on keyup for tinymce visit voyager_tinymce.js ,in function setup
        /*
         setup : function(editor) {
         editor.on('change keyup', function () {

         var lang = localStorage.getItem('translatable_lang');

         var name_input = editor['id'];
         console.log(name_input)

         //var to_lang = $('input[name="translatable['+ name_input +'][' + lang + ']"], textarea[name="translatable['+ name_input +'][' + lang + ']"]');
         var to_lang = $('#translatable_'+ name_input +'_' + lang + '');
         //register to specific language input
         to_lang.val(editor.getContent());
         });
         }
         */

    });


    function set_active_lang(lang){
        $('.change_langs [data_lang="' + lang +'"]').addClass('active_lang');
    }


    function set_lang_to_input(lang){
        var get_translatable_fields = $('.translate_this');

        get_translatable_fields.each(function(index,item){

            //var from_lang = $('input[name="translatable['+ item['name'] +'][' + lang + ']"], textarea[name="translatable['+ item['name'] +'][' + lang + ']"]').val();
            var from_lang = $('#translatable_'+ item['id'] +'_' + lang + '').val();

            $('#' + item['id'] + '').val(from_lang);

            //for richtextBox
            if($(item).hasClass('richTextBox')){
                tinymce.get($(item).attr('id')).setContent(from_lang);
            }

        });
    }

    function change_lang() {
        $('.change_lang').on('click', function () {
            $('.change_langs li').removeClass('active_lang');
            var data_lang = $(this).attr('data_lang');
            //set active lang
            set_active_lang(data_lang);
            localStorage.setItem('translatable_lang', data_lang);
            //display to input text of current languages selected
            set_lang_to_input(data_lang);
        });
    }

    function translate_this() {
        $('.translate_this').change(function() {
            var lang = localStorage.getItem('translatable_lang');

            //var name_input = $(this).attr('name');
            var name_input = $(this).attr('id');

            //var to_lang = $('input[name="translatable['+ name_input +'][' + lang + ']"], textarea[name="translatable['+ name_input +'][' + lang + ']"]');
            var to_lang = $('#translatable_'+ name_input +'_' + lang + '');
            //register to specific language input
            to_lang.val($(this).val());
        });
    }

}
