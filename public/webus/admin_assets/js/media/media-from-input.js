(function( $ ){

    $.fn.filemanager = function(type = 'image') {

        // if (type === 'image' || type === 'images') {
        //     type = 'Images';
        // } else {
        //     type = 'Files';
        // }

        this.on('click', function(e) {
            let input_id = $(this).data('input');
            let preview_id = $(this).data('preview');

            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            localStorage.setItem('target_input', input_id);
            localStorage.setItem('target_preview', preview_id);
            localStorage.setItem('data-gallery-key', $(this).data('gallery-key')); //data gallery key is for gallery in repeatable
            localStorage.setItem('data-module', $(this).data('module')); //data module is for module page
            window.open(path_absolute + '/media/frame-manager?type=' + type, 'FileManager', 'width= ' + x * 0.8 + ',height=' + y * 0.8 + '');
            return false;
        });
    }

})(jQuery);


function SetUrl(url){
    //set the value of the desired input to image url
    let target_input = $('#' + localStorage.getItem('target_input'));
    target_input.val(url);

    //set or change the preview image src
    let target_preview = $('#' + localStorage.getItem('target_preview'));
    target_preview.attr('src',url).addClass('selected_image');
}

function select_images(checkedValues) {
    //add to images place
    for(var index in checkedValues) {
        var arr = checkedValues[index].split('||');
        var gallery_place_id = localStorage.getItem('target_input');
        //data gallery key is for gallery in repeatable
        var data_gallery_key = localStorage.getItem('data-gallery-key');
        var data_module = localStorage.getItem('data-module');
        if(data_gallery_key !== 'undefined'){
            $('.gallery-place_' + gallery_place_id).append('<li class="list_image"><div class="remove-image-galery">x</div><img src="' + arr[1].replace(/.*\/\/[^\/]*/, '') + '" /><input type="hidden" name="' + data_gallery_key +'" value="' +  arr[0].replace(/.*\/\/[^\/]*/, '') + '"></li>')
        }else if(data_module !== 'undefined'){
            $('.gallery-place_' + gallery_place_id).append('<li class="list_image"><div class="remove-image-galery">x</div><img src="' + arr[1].replace(/.*\/\/[^\/]*/, '') + '" /><input type="hidden" name="'+gallery_place_id.replace("input_","")+'[]" value="' +  arr[0].replace(/.*\/\/[^\/]*/, '') + '"></li>');
        }else{
            $('.gallery-place_' + gallery_place_id).append('<li class="list_image"><div class="remove-image-galery">x</div><img src="' + arr[1].replace(/.*\/\/[^\/]*/, '') + '" /><input type="hidden" name="metabox['+gallery_place_id.replace("input_","")+'][]" value="' +  arr[0].replace(/.*\/\/[^\/]*/, '') + '"></li>')
        }
    }
}

$('.input_image').filemanager('image');

$('.input_images').filemanager('images');

$('.input_gallery_repeatable').filemanager('images');
