<?php

use Illuminate\Database\Seeder;

class AdditionalTranslations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translations = [
            [
                'name' => 'ask_for_price',
                'key' => 'ask_for_price',
                'value' => '[:ru]Уточнить стоимость',
            ],
        ];

        foreach ($translations as $translation) {
            DB::table('translations')->insert([
                'key' => $translation['key'],
                'name' => $translation['name'],
                'value' => $translation['value'],
            ]);
        }
    }
}
