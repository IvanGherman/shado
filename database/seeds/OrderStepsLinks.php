<?php

use Illuminate\Database\Seeder;

class OrderStepsLinks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = App\WebusModels\WebusMenu::where('slug', 'backend')->first();
        // categories
        $parent = DB::table('menu_items')->insertGetId([
            'menu_id' => $menu->id,
            'parent_id' => null,
            'name' => 'All order steps',
            'url' => '/admin/order-steps',
            'icon' => 'voyager-receipt',
            'target' => '_self',
            'item_order' => '30',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('menu_items')->insert([
            'menu_id' => $menu->id,
            'parent_id' => $parent,
            'name' => 'All order steps',
            'url' => '/admin/order-steps',
            'icon' => 'voyager-list',
            'target' => '_self',
            'item_order' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('menu_items')->insert([
            'menu_id' => $menu->id,
            'parent_id' => $parent,
            'name' => 'New order step',
            'url' => '/admin/order-steps/create',
            'icon' => 'voyager-plus',
            'target' => '_self',
            'item_order' => '2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
