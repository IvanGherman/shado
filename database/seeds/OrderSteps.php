<?php

use Illuminate\Database\Seeder;

class OrderSteps extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_steps')->insert([
            'title' => '[:ru]Заказ[:ro]Заказ',
            'name' => 'Заказ',
            'image' => 'default',
            'position' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'post_status' => 1,
            'short_description' => '<div class="info-item__link" data-modal="#request-modal">Заявка на сайте</div>
                                <span> или Заказ по телефону </span><a href="tel:+37312123456">+373 12 123 456</a>',
        ]);
    }
}
