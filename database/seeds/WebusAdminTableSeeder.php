<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class WebusAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = DB::table('menu')->insertGetId([
            'name' => 'Backend Menu',
            'slug' => 'backend',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $dashboard_item =  DB::table('menu_items')->insert([
            'menu_id' => $menu,
            'parent_id' => null,
            'name' => 'Dashboard',
            'url' => '/admin',
            'icon' => 'voyager-helm',
            'target' => '_self',
            'item_order' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $media_item =  DB::table('menu_items')->insert([
            'menu_id' => $menu,
            'parent_id' => null,
            'name' => 'Media',
            'url' => '/admin/media',
            'icon' => 'voyager-folder',
            'target' => '_self',
            'item_order' => '2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $modules_item =  DB::table('menu_items')->insert([
            'menu_id' => $menu,
            'parent_id' => null,
            'name' => 'Modules',
            'url' => '/admin/modules',
            'icon' => 'voyager-star',
            'target' => '_self',
            'item_order' => '3',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $pages_item =  DB::table('menu_items')->insertGetId([
            'menu_id' => $menu,
            'parent_id' => null,
            'name' => 'Pages',
            'url' => '/admin/pages',
            'icon' => 'voyager-window-list',
            'target' => '_self',
            'item_order' => '4',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $pages_item_all_page =  DB::table('menu_items')->insert([
            'menu_id' => $menu,
            'parent_id' => $pages_item,
            'name' => 'All pages',
            'url' => '/admin/pages',
            'icon' => 'voyager-list',
            'target' => '_self',
            'item_order' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $pages_item_new_page =  DB::table('menu_items')->insert([
            'menu_id' => $menu,
            'parent_id' => $pages_item,
            'name' => 'New page',
            'url' => '/admin/pages/create',
            'icon' => 'voyager-plus',
            'target' => '_self',
            'item_order' => '2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $news_item =  DB::table('menu_items')->insertGetId([
            'menu_id' => $menu,
            'parent_id' => null,
            'name' => 'News',
            'url' => '/admin/news',
            'icon' => 'voyager-file-text',
            'target' => '_self',
            'item_order' => '5',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $news_item_all_page =  DB::table('menu_items')->insert([
            'menu_id' => $menu,
            'parent_id' => $news_item,
            'name' => 'All news',
            'url' => '/admin/news',
            'icon' => 'voyager-list',
            'target' => '_self',
            'item_order' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $news_item_new_page =  DB::table('menu_items')->insert([
            'menu_id' => $menu,
            'parent_id' => $news_item,
            'name' => 'New post',
            'url' => '/admin/news/create',
            'icon' => 'voyager-plus',
            'target' => '_self',
            'item_order' => '2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $tools_item =  DB::table('menu_items')->insertGetId([
            'menu_id' => $menu,
            'parent_id' => null,
            'name' => 'Tools',
            'url' => '/',
            'icon' => 'voyager-settings',
            'target' => '_self',
            'item_order' => '99999',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);


        $menu_builder_item =  DB::table('menu_items')->insert([
            'menu_id' => $menu,
            'parent_id' => $tools_item,
            'name' => 'Menu Builder',
            'url' => '/admin/menu',
            'icon' => 'voyager-list',
            'target' => '_self',
            'item_order' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);




    }
}
