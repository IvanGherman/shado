<?php

use Illuminate\Database\Seeder;

class AdditionalVariables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $variables = [
            [
                'name' => 'call_request_email',
                'key' => 'call_request_email',
                'value' => 'user@example.com',
            ],
        ];

        foreach ($variables as $variable) {
            DB::table('variables')->insert([
                'key' => $variable['key'],
                'name' => $variable['name'],
                'value' => $variable['value'],
            ]);
        }
    }
}
