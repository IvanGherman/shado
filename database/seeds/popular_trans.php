<?php

use Illuminate\Database\Seeder;

class popular_trans extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translations = [
            [
                'name' => 'popular',
                'key' => 'popular',
                'value' => '[:ru]Популярные',
            ],
            [
                'name' => 'common',
                'key' => 'common',
                'value' => '[:ru]Похожие',
            ],
        ];

        foreach ($translations as $translation) {
            DB::table('translations')->insert([
                'key' => $translation['key'],
                'name' => $translation['name'],
                'value' => $translation['value'],
            ]);
        }
    }
}
