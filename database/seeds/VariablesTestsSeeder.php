<?php

use Illuminate\Database\Seeder;

class VariablesTestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $variables = [
            [
                'name' => 'contact_email',
                'key' => 'contact_email',
                'value' => 'contact@shado.com',
            ],
            [
                'name' => 'review_email',
                'key' => 'review_email',
                'value' => 'review@shado.com',
            ],
            [
                'name' => 'request_master_email',
                'key' => 'request_master_email',
                'value' => 'request@shado.com',
            ],
            [
                'name' => 'map_zoom',
                'key' => 'map_zoom',
                'value' => 10,
            ],
            [
                'name' => 'map_default_lat',
                'key' => 'map_default_lat',
                'value' => 47.014065,
            ],
            [
                'name' => 'map_default_ltg',
                'key' => 'map_default_ltg',
                'value' => 28.849586,
            ],
            [
                'name' => 'contact_page_contact_email',
                'key' => 'contact_page_contact_email',
                'value' => 'shado.sales@gmail.com',
            ],
            [
                'name' => 'phone',
                'key' => 'phone',
                'value' => '02222222',
            ],
            [
                'name' => 'viber',
                'key' => 'viber',
                'value' => 'viber://chat?number=+37379668882',
            ],
            [
                'name' => 'whatsapp',
                'key' => 'whatsapp',
                'value' => 'https://wa.me/37379668882',
            ],
            [
                'name' => 'instagram',
                'key' => 'instagram',
                'value' => 'https://www.instagram.com/?hl=ru',
            ],
            [
                'name' => 'facebook',
                'key' => 'facebook',
                'value' => 'https://www.facebook.com/?',
            ],
            [
                'name' => 'phone2',
                'key' => 'phone2',
                'value' => '02555555',
            ],
        ];

        foreach ($variables as $variable) {
            DB::table('variables')->insert([
                'name' => $variable['name'],
                'key' => $variable['key'],
                'value' => $variable['value'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }
}
