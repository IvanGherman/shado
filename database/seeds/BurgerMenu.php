<?php

use Illuminate\Database\Seeder;

class BurgerMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = DB::table('menu')->insertGetId([
            'name' => 'Burger menu',
            'slug' => 'burger-menu',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
