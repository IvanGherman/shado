<?php

use Illuminate\Database\Seeder;

class ContactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = App\WebusModels\WebusMenu::where('slug', 'backend')->first();

        // categories
        $parent = DB::table('menu_items')->insertGetId([
            'menu_id' => $menu->id,
            'parent_id' => null,
            'name' => 'Contacts',
            'url' => '/admin/contacts',
            'icon' => 'voyager-helm',
            'target' => '_self',
            'item_order' => '23',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('menu_items')->insert([
            'menu_id' => $menu->id,
            'parent_id' => $parent,
            'name' => 'All contacts',
            'url' => '/admin/contacts',
            'icon' => 'voyager-list',
            'target' => '_self',
            'item_order' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('menu_items')->insert([
            'menu_id' => $menu->id,
            'parent_id' => $parent,
            'name' => 'New contact',
            'url' => '/admin/contacts/create',
            'icon' => 'voyager-plus',
            'target' => '_self',
            'item_order' => '2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
