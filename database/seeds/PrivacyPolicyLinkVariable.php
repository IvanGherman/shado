<?php

use Illuminate\Database\Seeder;

class PrivacyPolicyLinkVariable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('variables')->insert([
            'name' => 'privacy_policy_link',
            'key' => 'privacy_policy_link',
            'value' => 'http://shado.local/products',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
