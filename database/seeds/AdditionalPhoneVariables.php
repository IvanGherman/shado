<?php

use Illuminate\Database\Seeder;

class AdditionalPhoneVariables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $variables = [
            [
                'name' => 'facebook_phone',
                'key' => 'facebook_phone',
                'value' => '02555555',
            ],
            [
                'name' => 'facebook_phone2',
                'key' => 'facebook_phone2',
                'value' => '02555555',
            ],

            [
                'name' => 'instagram_phone',
                'key' => 'instagram_phone',
                'value' => '02555555',
            ],
            [
                'name' => 'instagram_phone2',
                'key' => 'instagram_phone2',
                'value' => '02555555',
            ],

            [
                'name' => 'google_phone',
                'key' => 'google_phone',
                'value' => '02555555',
            ],
            [
                'name' => 'google_phone2',
                'key' => 'google_phone2',
                'value' => '02555555',
            ],

            [
                'name' => '_999_phone',
                'key' => '_999_phone',
                'value' => '02555555',
            ],
            [
                'name' => '999_phone2',
                'key' => '999_phone2',
                'value' => '02555555',
            ],

            [
                'name' => 'allbiz_phone',
                'key' => 'allbiz_phone',
                'value' => '02555555',
            ],
            [
                'name' => 'allbiz_phone2',
                'key' => 'allbiz_phone2',
                'value' => '02555555',
            ],

            [
                'name' => 'prom_phone',
                'key' => 'prom_phone',
                'value' => '02555555',
            ],
            [
                'name' => 'prom_phone2',
                'key' => 'prom_phone2',
                'value' => '02555555',
            ],
        ];

        foreach ($variables as $variable) {
            DB::table('variables')->insert([
                'name' => $variable['name'],
                'key' => $variable['key'],
                'value' => $variable['value'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }
}
