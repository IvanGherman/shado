<?php

use Illuminate\Database\Seeder;

class HomePage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'slug' => 'home_page',
            'name' => 'Homepage',
            'template' => 'default',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'post_status' => 1,
        ]);

        DB::table('pages')->insert([
            'slug' => 'product',
            'name' => 'product',
            'template' => 'default',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'post_status' => 1,
        ]);
    }
}
