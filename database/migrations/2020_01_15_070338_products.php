<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name')->nullable(false);
            $table->string('title')->nullable(false);
            $table->string('model')->nullable(false);
            $table->string('model_image')->nullable(true);
            $table->text('description')->nullable(true);
            $table->text('production_duration')->nullable(true);
            $table->string('code')->nullable(false);
            $table->integer('position')->nullable(false);
            $table->integer('promotion')->nullable(false);
            $table->integer('price')->nullable(false);
            $table->integer('promo_price')->nullable(true);
            $table->integer('show_in_catalog')->nullable(true);
            $table->bigInteger('transparency_id')->unsigned()->index()->nullable(true);
            $table->bigInteger('tree_type_id')->unsigned()->index()->nullable(true);
            $table->foreign('transparency_id')->references('id')->on('transparencies')->onDelete('set null');
            $table->foreign('tree_type_id')->references('id')->on('tree_types')->onDelete('set null');
            $table->integer('post_status')->nullable();

            $table->longText('images')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
