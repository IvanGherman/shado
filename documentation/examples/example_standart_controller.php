<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;

class WebusMenuController extends Controller
{

    public function __construct()
    {
        $this->model_name = 'App\WebusMenu'; //model
        $this->slug = 'menu'; //id controller
        $this->title = 'Menu'; //title controller

        //Identical to migrated file
        $this->list_field = ['name', 'slug']; //in list template

        $this->form_fields = array( //in form template
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'translatable' => true
            ],

            'slug' => [
                'title' => 'Slug',
                'html_type' => 'text',
                'translatable' => false
            ],

        );
    }

    public function index(Request $request){

        $model_name = $this->model_name;
        $data = array(
            'title' => $this->title,
            'url_create' => route($this->slug.'.create'),
            'url_edit' => 'admin/'.$this->slug.'/edit/',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_build' => 'admin/'.$this->slug.'/build/',
            'rows' => $this->list_field,
            'list' => $model_name::paginate('10')
        );

        $view = 'webus.standart.index';

        if (view()->exists('webus.'.$this->slug.'.index')) {
            $view = 'webus.'.$this->slug.'.index';
        }

        return view($view, ['data' => $data]);
    }

    public function create(){

        $data = array(
            'title' => 'Add new '.$this->slug,
            'url_create' => '',
            'form_action' => route($this->slug.'.store'),
            'rows' => $this->form_fields,
        );

        $view = 'webus.standart.add-edit';

        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }

        return view($view, ['data' => $data]);
    }

    public function store(Request $request){

        $this->validate($request, [
            'title' => 'required|max:255',
            'slug' => 'required',
        ]);

        $model_name = $this->model_name;
        $post = new $model_name;

        foreach ($this->form_fields as $key => $field){
            $post->$key = $request->$key;
        }

        $post->save();

        Session::flash('flash_message','Successfully added.');

        return redirect(route($this->slug.'.edit', ['id' => $post->id]));
    }

    public function edit(Request $request, $id){
        $model_name = $this->model_name;
        $model = $model_name::findOrFail($id);

        $data = array(
            'title' => 'Edit '.$this->slug,
            'url_create' => '',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_build' => 'admin/'.$this->slug.'/menu/',
            'form_action' => route($this->slug.'.update', ['id' => $id]),
            'rows' => $this->form_fields,
        );

        $view = 'webus.standart.add-edit';

        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }

        return view($view, ['data' => $data, 'model' => $model]);

    }

    public function update(Request $request, $id){
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);

        foreach ($this->form_fields as $key => $field){
            $post->$key = $request->$key;
        }

        $post->save();

        Session::flash('flash_message','Successfully saved.');
        return redirect(route($this->slug.'.edit', ['id' => $id]));

    }

    public function delete($id){
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);
        $post->delete();
        Session::flash('flash_message','Successfully deleted.');
        return redirect(route($this->slug.'.menu'));
    }
    

}
