<?php 
//widget in sidebar 
static function filter_news(){
    $taxonomy_model = 'App\TaxonomyTerms';
    $group_filters = array(
        'country' => [
            'title' => 'Страны',
            'all' => 'Все страны',
            'terms' => $taxonomy_model::get_terms('country', true)
        ],
        'categories' => [
            'title' => 'Категории',
            'all' => 'Все новости',
            'terms' => $taxonomy_model::get_terms('categories', true)
        ]
    );

    $list_taxonomy = 'country,categories';
    $table = 'news';

    return view('front.widgets.filter-news', [
        'group_filters' => $group_filters,
        'list_taxonomy' => $list_taxonomy,
        'table' => $table
    ]);
}
?>

<!--front.widgets.filter-news Template-->

@if($group_filters)
<form id="filter-news" method="get">
    {{ csrf_field() }}
    {{ method_field('GET') }}
    @foreach($group_filters as $key=>$group)
    @if($group['terms'])
    <div class="right-info-container__item">
        <div class="item_name ">{{ $group['title'] }}</div>
        <div class="item__checkbox-zone">
            <div class="item__checkbox-container item__checkbox-clicked">
                <div class="item__checkbox">
                    <input type="checkbox" class="item__checkbox-unit" name="{{ $key }}" value="0">
                    <div class="checkbox-imitation transition"></div>
                </div>
                <div class="item__checkbox-country transition">{{ $group['all'] }}</div>
            </div>

            @foreach($group['terms'] as $item)
            <div class="item__checkbox-container">
                <div class="item__checkbox">
                    <input type="checkbox" class="item__checkbox-unit" name="{{ $key }}" value="{{ $item->id }}">
                    <div class="checkbox-imitation transition"></div>
                </div>
                <div class="item__checkbox-country transition">{{ $item->name }}</div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    @endforeach
</form>
@endif

@push('filters')

<script>
    $('#filter-news input[type="checkbox"]').change(function() {
        debounce_action(filter_news(), 550);
    });
    function filter_news() {
        jQuery.ajax({
            url: '/news-filter',
            type: 'GET',
            dataType: "json",
            data: $('#filter-news').serialize() + "&list_taxonomy=" + '{{ $list_taxonomy }}' + "&table=" + '{{ $table }}',
            success:function(json){
                console.log(json['response']);
                $(".news-container").animate({ scrollTop: 0 }, 600);;
            },
            beforeSend:function()
            {
                $('.news-container').html('Preloader....');
            },

            error:function (json, xhr, ajaxOptions, thrownError){
                console.log(json['responseText']);
            }
        });

    }

    function debounce_action(fn, duration) {
        var timer;
        return function(){
            clearTimeout(timer);
            timer = setTimeout(fn, duration);
        }
    }

</script>
@endpush

<!--front.widgets.filter-news Template-->

<?php

//AJAX HTTP URL FUNCTION
static function news_filter(Request $request){
    if($request->ajax()){
        $table = $request->table;
        // Start By Taxonomy
        $all_ids_from_taxonomy_query = TaxonomyTerms::get_posts_ids_filter_by_terms($request, $table);
        //End By Taxonomy

        $posts_query = DB::table($table);

        //Start By Field from table
        if(isset($request->activity) && $request->activity){
            $posts_query->where('activity', $request->activity);
        }
        //End By Field from table

        //Result filter
        $posts_query = $posts_query->whereIn('id', array_map('intval', explode(',', $all_ids_from_taxonomy_query)))
            ->get();

        return response()->json([
            'response' => $posts_query
        ]);
    }else{
        abort(404);
    }
}
?>
