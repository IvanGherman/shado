<?php
//Taxonomy News Category
Route::group([
    'as'     => 'category.',
    'prefix' => 'category',
], function () {
    Route::get('/', ['uses' => 'AdminCategoryController@index_taxonomy', 'as' => 'category']);
    Route::get('create', ['uses' => 'AdminCategoryController@create_taxonomy', 'as' => 'create']);
    Route::post('store', ['uses' => 'AdminCategoryController@store_taxonomy', 'as' => 'store']);
    Route::get('edit/{id}', ['uses' => 'AdminCategoryController@edit_taxonomy', 'as' => 'edit']);
    Route::put('update/{id}', ['uses' => 'AdminCategoryController@update_taxonomy', 'as' => 'update']);
    Route::post('delete/{id}', ['uses' => 'AdminCategoryController@delete_taxonomy', 'as' => 'delete']);
});
?>
