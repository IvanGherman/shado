<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminCategoryController extends WebusWordpressController
{
    public function __construct()
    {
        /******Example Construct for Your Controller (This example works for the wordpress controller., do not delete)******/
        $this->model_name = 'App\WordpressTaxonomy'; //model
//        $this->post_type = 'portfolio'; //model
        $this->slug = 'category'; //id controller
        $this->title = 'Category'; //title controller

        //Identical to migrated file
        $this->list_field = ['name' => 'translatable', 'slug']; //in list template
        $model = $this->model_name;
        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            'slug' => [
                'title' => 'Slug',
                'html_type' => 'text',
                'validate' => 'required|max:255',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'parent' => [
                'title' => 'Parent',
                'html_type' => 'select',
                'options' => $model::get_terms_array($this->slug),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],



        );
    }
}
