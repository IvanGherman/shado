<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusModulesController;
use DB;
class CustomHTML extends WebusModulesController
{

    public function __construct()
    {

        $this->title = 'Custom HTML';
        $this->module_filename = 'CustomHTML';

        //support only text,body,textarea
        $this->form_fields = array( //in form template

            //Required field (no translated)
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'html' => [
                'title' => 'HTML',
                'html_type' => 'textarea',
                'rows' => 30,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled'
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]

        );

    }

    static function front($module_id){
        $get = DB::table('modules')->where('id', $module_id)->where('module_status', 1)->first();
        if($get != null){
            $module = json_decode($get->setting);
        }else{
            $module = '';
        }
        return view('front.modules.CustomHTML', ['module' => $module]);
    }

    ///get on front
    // 1.$modules = webus_help::get_metabox(2, 'pages', 'modules')
    //2. {!! \App\Http\Helpers\webus_help::get_modules($modules) !!}

}


