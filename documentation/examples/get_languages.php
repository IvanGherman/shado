@foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)

<li class="lang-{{$localeCode}}">
    <a href="{{LaravelLocalization::getLocalizedURL($localeCode) }}" hreflang="{{$localeCode}}" title="{{ $properties['name'] }}" class="qtranxs_image qtranxs_image_{{$localeCode}}">
        <img src="/flags/{{$localeCode}}.png" alt="{{ $properties['name'] }}" />
        <span style="display:none">{{ $properties['name'] }}</span>
    </a>
</li>

@endforeach
