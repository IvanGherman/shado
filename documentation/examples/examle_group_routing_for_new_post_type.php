<?php

Route::group([
    'as'     => 'menu.',
    'prefix' => 'menu',
], function () {
    Route::get('/', ['uses' => 'WebusMenuController@index', 'as' => 'menu']);
    Route::get('create', ['uses' => 'WebusMenuController@create', 'as' => 'create']);
    Route::post('store', ['uses' => 'WebusMenuController@store', 'as' => 'store']);
    Route::get('edit/{id}', ['uses' => 'WebusMenuController@edit', 'as' => 'edit']);
    Route::put('update/{id}', ['uses' => 'WebusMenuController@update', 'as' => 'update']);
    Route::post('delete/{id}', ['uses' => 'WebusMenuController@delete', 'as' => 'delete']);
});

?>
