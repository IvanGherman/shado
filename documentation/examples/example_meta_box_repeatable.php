<?php
$this->meta_box = array(

    'accent_warranty_repair_menu_title' => [
        'title' => 'Menu title',
        'html_type' => 'text',
        'validate' => false,
        'custom_class' => '',
        'id' => '',
        'translatable' => true,
        //'show_on' => array(2),
        'metabox' => true

    ],


    'accent_warranty_repair_prices' => [
        'title' => 'Prices',
        'html_type' => 'repeatable',
        'start_with' => 1,
        'max' => 100,
        'options' => array(

            //fields for repeatable box
            'title' => array(
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => false,
                'translatable' => true,
                'custom_class' => 'col-md-6'
            ),

            'price' => array(
                'title' => 'Price - $',
                'html_type' => 'text',
                'validate' => false,
                'translatable' => false,
                'custom_class' => 'col-md-6'
            ),

            'description' => array(
                'title' => 'Description',
                'html_type' => 'textarea',
                'validate' => false,
                'translatable' => true,
                'custom_class' => 'col-md-12'
            )
        ),
        'validate' => false,
        'custom_class' => '',
        'id' => '',
        'translatable' => false,
        //'show_on' => array(2),
        'metabox' => true

    ],


);



//get repeatable
?>

@php $i = 0; $e = 0;@endphp
@foreach($meta_box_prices as $item)
<?php

$metabox_title = 'title_'.LaravelLocalization::getCurrentLocale();
$metabox_description = 'description_'.LaravelLocalization::getCurrentLocale();
?>
<div class="pricing-list__item">
    <span class="pricing-list__title">@if(isset($item->$metabox_title)) {{ $item->$metabox_title }} @endif </span>
    <span class="pricing-list__line"></span>
    <span class="pricing-list__price">@if(isset($item->price)) {{ $item->price }} MDL @endif</span>
    <p class="pricing-list__description">@if(isset($item->$metabox_description)) {!! $item->$metabox_description !!} @endif</p>
</div>
@endforeach
