<?php

//page route:

Route::get('{primary_slug}', 'CristisPageController@index');
Route::get('{secondslug}/{primary_slug}', 'CristisPageController@index');


///Page controller

public function index(Request $request, $primary_slug = false, $second_slug = false){

    $slug = '';

    if(!$second_slug){
        $slug = $primary_slug;
    }else{
        $slug = $second_slug;
    }


    if(!empty($slug)){

        $content = webus_help::get_post_content_by_slug($slug);
        return view('cristis.page', [
            'title' => webus_help::translate_automat($content->post_title),
            'content' => $content
        ]);

    }else{
        abort(404);
    }

}
?>
