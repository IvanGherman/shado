<?php 
return [
   'attention' => 'Attention!',
   'session_expired' => 'Your session has expired, click the OK button to reload the page'
];