<?php 

return [
    'text_admin' => 'Здравствуйте, вы получили новый заказ',
    'text_customer' => 'Здравствуйте, Ваш заказ был успешно отправлен',
    'text_payment_method_undefined' => 'К сожалению, выбранный Вами метод оплаты не существует. Свяжитесь с администрацией сайта.',
    'text_address_undefined' => 'К сожалению, выбранный адрес не существует в системе. Добавить и выберите адрес.',
    'text_exist_user' => 'К сожалению, этот электронный адрес уже зарегистрирован',

    'text_checkout_success' => 'Comanda a fost efectuata cu success!',
    'text_checkout_failed' => 'Comanda nu a fost efectuata, mai incercati odata sau contactati administatorul  site-ului.'
];