<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('front.partial.head')
    </head>
    <body>
        @include('front.partial.header')

        @php
            $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
            $variables = json_decode($variables);

            $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
            $translations = json_decode($translations);

            $locale = Config::get('app.locale');
        @endphp
        <main id="main">
            @yield('content')
        </main>
        @yield('modal_content')

        @include('front.partial.modals')
        @include('front.partial.footer')
        @include('front.partial.scripts')
    </body>
</html>