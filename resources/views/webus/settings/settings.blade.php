@extends('layouts.webus')
@section('css')
    <style>
        .panel-actions .voyager-trash {
            cursor: pointer;
        }

        .panel-actions .voyager-trash:hover {
            color: #e94542;
        }

        .panel hr {
            margin-bottom: 10px;
        }

        .panel {
            padding-bottom: 15px;
        }

        .sort-icons {
            font-size: 21px;
            color: #ccc;
            position: relative;
            cursor: pointer;
        }

        .sort-icons:hover {
            color: #37474F;
        }

        .voyager-sort-desc {
            margin-right: 10px;
        }

        .voyager-sort-asc {
            top: 10px;
        }

        .page-title {
            margin-bottom: 0;
        }

        .panel-title code {
            border-radius: 30px;
            padding: 5px 10px;
            font-size: 11px;
            border: 0;
            position: relative;
            top: -2px;
        }

        .new-setting {
            text-align: center;
            width: 100%;
            margin-top: 20px;
        }

        .new-setting .panel-title {
            margin: 0 auto;
            display: inline-block;
            color: #999fac;
            font-weight: lighter;
            font-size: 13px;
            background: #fff;
            width: auto;
            height: auto;
            position: relative;
            padding-right: 15px;
        }

        .new-setting hr {
            margin-bottom: 0;
            position: absolute;
            top: 7px;
            width: 96%;
            margin-left: 2%;
        }

        .new-setting .panel-title i {
            position: relative;
            top: 2px;
        }

        .new-settings-options {
            display: none;
            padding-bottom: 10px;
        }

        .new-settings-options label {
            margin-top: 13px;
        }

        .new-settings-options .alert {
            margin-bottom: 0;
        }

        #toggle_options {
            clear: both;
            float: right;
            font-size: 12px;
            position: relative;
            margin-top: 15px;
            margin-right: 5px;
            margin-bottom: 10px;
            cursor: pointer;
            z-index: 9;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .new-setting-btn {
            margin-right: 15px;
            position: relative;
            margin-bottom: 0;
            top: 5px;
        }

        .new-setting-btn i {
            position: relative;
            top: 2px;
        }

        .img_settings_container {
            width: 200px;
            height: auto;
            position: relative;
        }

        .img_settings_container > a {
            position: absolute;
            right: -10px;
            top: -10px;
            display: block;
            padding: 5px;
            background: #F94F3B;
            color: #fff;
            border-radius: 13px;
            width: 25px;
            height: 25px;
            font-size: 15px;
            line-height: 19px;
        }

        .img_settings_container > a:hover, .img_settings_container > a:focus, .img_settings_container > a:active {
            text-decoration: none;
        }

        textarea {
            min-height: 120px;
        }

        .for_translate{
            background: #9393b5;
            color: #fff;
        }
    </style>
@endsection
@section('content')

    <h1 class="page-title">
        <i class="voyager-settings"></i> Settings
    </h1>

    <div class="container-fluid">
        @if(Session::has('flash_message'))
            <div class="alert alert-success">{!! session('flash_message') !!}</div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="alert alert-info">
            <strong>How To Use:</strong>
            <p>You can get the value of each setting anywhere on your site by calling <code>webus_help::get_option('key', 'boolean translate')</code></p>
        </div>
    </div>

    <div class="page-content container-fluid">
        <form action="{{ route('save_options_settings') }}" method="POST" enctype="multipart/form-data">
            {{ method_field('POST') }}
            {{ csrf_field() }}
            @if(count($settings))
                <div class="panel">
                    @foreach($settings as $setting)
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{ $setting->name }}
                            </h3>
                            <div class="panel-actions">
                                <i class="voyager-trash" data-id="{{ $setting->id }}" data-display-key="{{ $setting->key }}" data-display-name=" {{ $setting->name }}"></i>
                            </div>
                        </div>
                        <div class="panel-body">
                            @php
                            if($setting->translatable == 'true'){
                            $classes = 'translate_this';
                            $active_translatable = true;
                            }else{
                            $classes = ' ';
                            $active_translatable = false;
                            }

                            $item =  array(
                            'title' => $setting->name,
                            'validate' => false,
                            'translatable' => $active_translatable,
                            );
                            $model[$setting->key] = $setting->value;
                            @endphp
                            @include('webus.partials.languages', ['key' => $setting->key])
                            @include('webus.fields.'.$setting->type.'', ['key' => $setting->key, 'item' => $item, 'model' => $model, 'classes' => $classes])
                        </div>
                        <hr>
                    @endforeach
                </div>
                <button type="submit" class="btn btn-primary pull-right">Save Settings</button>
            @endif
        </form>

        <div style="clear:both"></div>

        <div class="panel" style="margin-top:10px;">
            <div class="panel-heading new-setting">
                <hr>
                <h3 class="panel-title"><i class="voyager-plus"></i> New Setting</h3>
            </div>
            <div class="panel-body">
                <form action="{{ route('add_settings') }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="col-md-4">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="col-md-2">
                        <label for="key">Key</label>
                        <input type="text" class="form-control" name="key">
                    </div>
                    <div class="col-md-4">
                        <label for="asdf">Type</label>
                        <select name="type" class="form-control">
                            <option value="text">Text Box</option>
                            <option value="textarea">Text Area</option>
                            <option value="body">Rich Textbox</option>
                            {{--<option value="checkbox">Check Box</option>--}}
                            {{--<option value="radio_btn">Radio Button</option>--}}
                            {{--<option value="select_dropdown">Select Dropdown</option>--}}
                            <option value="file_input">File</option>
                            <option value="image_input">Image</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="translatable">Translatable</label>
                        <select name="translatable" class="form-control">
                            <option value="false">False</option>
                            <option value="true">True</option>
                        </select>
                    </div>

                    <div style="clear:both"></div>
                    <button type="submit" class="btn btn-primary pull-right new-setting-btn">
                        <i class="voyager-plus"></i> Add New Setting
                    </button>
                    <div style="clear:both"></div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> Are you sure you want to delete the <span id="delete_setting_title"></span> Setting?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ url('admin/settings/delete_option/__id') }}" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Yes, Delete This Setting">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('document').ready(function () {
            $('.voyager-trash').click(function () {
                var display = $(this).data('display-name') + '/' + $(this).data('display-key');

                $('#delete_setting_title').text(display);
                $('#delete_form')[0].action = $('#delete_form')[0].action.replace('__id', $(this).data('id'));
                $('#delete_modal').modal('show');
            });

        });
    </script>

@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/nestable.css">
@endsection
@section('javascript')
    <script src="/webus/admin_assets/lib/js/tinymce/tinymce.min.js"></script>
    <script src="/webus/admin_assets/js/voyager_tinymce.js"></script>
    <script type="text/javascript" src="/webus/admin_assets/js/jquery.nestable.js"></script>
    @stack('scripts')
    @stack('gallery_field')
@endsection
