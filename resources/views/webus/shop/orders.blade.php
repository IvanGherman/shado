@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-@if(!empty($data['icon'])){{ $data['icon'] }}@else{{ 'list-add' }}@endif"></i> {{ $data['title'] }}
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-12">
            <div id="dataTable_filter" class="dataTables_filter">
                <form role="form" action="" method="GET">
                    <label>Search by ID Orders:<input type="search" name="search" value="@php if(isset($_GET['search']) && $_GET['search']){echo $_GET['search'];} @endphp" class="form-control input-sm" placeholder="" aria-controls="dataTable"></label>
                </form>

            </div>
        </div>

        <table class="row table table-hover webus-table">
            <thead>
            <tr>
                <td>ID</td>
                <td>Total</td>
                <td>Date</td>
                <td>Payment</td>
                <td>Status</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($data['orders'] as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->total.' '.$order->currency }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>{{ \App\Http\HelpersShop\webus_shop::get_payment_methods()[$order->method_payment] }}</td>
                    <td>{{ \App\Http\HelpersShop\webus_shop::order_status($order->order_status) }}</td>
                    <td>
                    <a href="{{ route('admin_order_view', [$order->id]) }}" title="View" class="btn btn-sm btn-primary pull-right">
                    <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>
                    </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $data['orders']->links() }}

    </div>

@endsection