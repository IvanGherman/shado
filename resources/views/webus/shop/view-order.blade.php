@extends('layouts.webus')
@section('css')
    <style>
        .panel-heading {
            padding: 10px;
            font-size: 15px;
            background: #303841;
            color: #8e98a0;
        }
        .panel-bordered>.panel-body {
            padding-top: 15px;
            margin-top: 0;
        }
        .order-info-ul{
            list-style-type: none;
            padding: 0;
        }
    </style>
@endsection
@section('content')

    <h1 class="page-title">
        <i class="voyager-@if(!empty($data['icon'])){{ $data['icon'] }}@else{{ 'list-add' }}@endif"></i> {{ $data['title'] }}
    </h1>

    <div class="page-content container-fluid">
        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">General data</div>
                    <div class="panel-body">
                        {{ Form::model($record,[
                              'method' => 'POST',
                              'route' => ['admin_order_update', $record->id],
                              'files' => false,
                              'class'=> 'form',
                              ]) }}
                        <ul class="order-info-ul">
                            <li><strong>Customer email</strong>: <a href="mailto:{{ \App\User::find($record->user_id)->email }}">{{ \App\User::find($record->user_id)->email }}</a> </li>
                            <li><strong>Customer name</strong>: {{ \App\User::find($record->user_id)->name }} </li>
                            <li><strong>Total</strong>: {{ $record->total }}</li>
                            <li><strong>Currency</strong>: {{ $record->currency }}</li>
                            <li><strong>Date</strong>: {{ $record->created_at }}</li>
                            <li><strong>Payment method</strong>: {{ \App\Http\HelpersShop\webus_shop::get_payment_methodsAdmin()[$record->method_payment] }}</li>
                            @if($record->price_payment)
                                <li><strong>Payment money</strong>: {{ $record->price_payment }}</li>
                            @endif
                            <li><strong>Status</strong>:  {{ Form::select('order_status', \App\Http\HelpersShop\webus_shop::order_status(), null, ['class' => 'form-select']) }}</li>
                        </ul>
                        <button type="submit" class="btn btn-success">Update order</button>
                        <button type="button" class="delete_order btn btn-warning">Delete order</button>
                        {{ Form::close() }}
                    </div>
                    <div class="panel-heading">Address</div>
                    <div class="panel-body">
                        <ul class="order-info-ul">
                            <li><strong>Name</strong>: {{ $record->address_name }}</li>
                            <li><strong>Street</strong>: {{ $record->address_street }}</li>
                            <li><strong>Index</strong>: {{ $record->address_index }}</li>
                            <li><strong>Country</strong>: {{ $record->address_country }}</li>
                            <li><strong>State</strong>: {{ $record->address_state }}</li>
                            <li><strong>City</strong>: {{ $record->address_city }}</li>
                            <li><strong>Phone</strong>: <a href="tel:{{ $record->address_phone }}">{{ $record->address_phone }}</a> </li>
                        </ul>
                    </div>

                    <div class="panel-heading">Products</div>
                    <div class="panel-body">
                        <table class="row table table-hover webus-table">
                            <thead>
                            <tr>
                                <td>Name</td>
                                <td>Quantity</td>
                                <td>Price</td>
                                <td>Total price</td>
                                <td>Currency</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($record->products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->quantity }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $product->price*$product->quantity }}</td>
                                    <td>{{ $product->currency }}</td>
                                    <td>
                                        <a href="{{ url('admin/products/edit/'.$product->product_id) }}" title="View" class="btn btn-sm btn-default pull-right" target="_blank">
                                            <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View product in admin</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($record->method_payment == 'paypal' && isset($data_order_transactions->transactions) && $data_order_transactions->transactions->count())
                            <ul class="order-info-ul col-md-12">
                                @foreach($data_order_transactions->transactions as $transaction)
                                    <li>
                                        @php $data_transaction = json_encode($transaction);  @endphp
                                        <ul>
                                            @foreach(json_decode($data_transaction) as $key_trans=>$val_trans)
                                                @if($key_trans == 'debug_data')
                                                    <li><small><strong>{{ $key_trans }}</strong></small>
                                                     <textarea>
                                                        {{$val_trans}}
                                                    </textarea>
                                                    </li>

                                                @else
                                                    <li><small><strong>{{ $key_trans }}</strong> {{$val_trans}}</small> </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                        @if(!empty($record->info))
                            <textarea>{{ $record->info }}</textarea>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{  Form::open(['route' => ['admin_order_delete', $record->id], 'class' => 'hidden', 'id'=> 'delete_order']) }}
    <button type="submit">Удалить</button>
    {{ Form::close() }}
@endsection

@section('javascript')
    <script>
        $(document).on('click', '.delete_order', function (event) {
            if(confirm('Are you sure you want to delete this order?')){
                $('form#delete_order').trigger('submit');
            }
            return false;
        })
    </script>
@endsection