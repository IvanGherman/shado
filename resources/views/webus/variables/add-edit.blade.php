@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-@if(!empty($data['icon'])){{ $data['icon'] }}@else{{ 'list-add' }}@endif"></i> {{ $data['title'] }}
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">


                    <!-- form start -->
                    <form role="form" action="{{ $data['form_action'] }}" method="POST" enctype="multipart/form-data">


                        @if(isset($model))
                            {{ method_field("PUT") }}
                        @else
                            {{ method_field("POST") }}
                        @endif

                        {{ csrf_field() }}

                        <div class="panel-body">
                            @foreach($data['rows'] as $key => $item)
                                <div class="form-group">
                                    <label class="heading_field">{{ $item['title'] }}</label>
                                    <span class="required-field">*</span>
                                    <input type="text" class="form-control" name="{{ $key }}" value="{{ $model->{$key} }}" />
                                </div>
                            @endforeach

                        </div><!-- panel-body -->
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/nestable.css">
@endsection
@section('javascript')
    <script src="/webus/admin_assets/lib/js/tinymce/tinymce.min.js"></script>
    <script src="/webus/admin_assets/js/voyager_tinymce.js"></script>
    <script type="text/javascript" src="/webus/admin_assets/js/jquery.nestable.js"></script>
    @stack('scripts')
    @stack('gallery_field')
@endsection

