<select @if(isset($name_option))

        name="{{ $key }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $name_option }}]"
        id="{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>"
        class="form-control select2"
        @if($item['validate']) required @endif>

    @else
        @if(isset($item['metabox'])) name="metabox[{{ $key }}]@if($item['multiple'])[]@endif" @else name="{{ $key }}@if($item['multiple'])[]@endif" @endif
        placeholder="{{ $item['title'] }}"
        id="{{ $key }}" class="form-control select2" @if($item['validate']) required @endif
        @if($item['multiple']) multiple @endif>
    @endif

    @if($options)

        @if($item['multiple'])

            @if (!empty($item['is_entity']) && $item['is_entity'])
                @foreach($options as $key_option=>$option)
                    <option value="{{ $key_option }}"
                        @if (isset($model))
                            @if (isset($item['attribute']))
                                @foreach($model->values as $relation)
                                    @if ($relation->attributeValue->id == $key_option) selected
                                    @endif
                                @endforeach
                            @else
                                @if ($model->$key)
                                    @foreach($model->$key as $relation)
                                        @if ($relation->id === $key_option) selected
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        @endif
                    >{{ $option }}</option>
                @endforeach
            @else

                @foreach($options as $key_option=>$option)
                    <option value="{{ $key_option }}"

                            @if(isset($item['default_value_not_by_keys']) && $item['default_value_not_by_keys'] == true)

                                @if(isset($model) && isset($model['selected_'.$key]) && in_array($key_option, $model['selected_'.$key])) selected @endif
                                @if(isset($model) && !isset($model['selected_'.$key]) && null !== $model[$key] && in_array($key_option, $model[$key])) selected @endif

                            @else

                                @if(isset($model) && array_key_exists($key_option, $model['selected_'.$key])) selected @endif

                            @endif

                    >{{ $option }}</option>
                @endforeach

            @endif


        @else

            @foreach($options as $key_option=>$option)
                @if(isset($name_option))
                    @php
                        $module_edit_url = '';
                        if($key == 'modules' && isset($repeatable->$name_option)){
                        $module_edit_url = route('edit_'.\App\Http\Helpers\webus_help::get_code_module($key_option), ['setting_id' => $key_option]);
                        }
                    @endphp
                    <option value="{{ $key_option }}" @if(isset($repeatable->$name_option) && $repeatable->$name_option == $key_option){{ 'selected' }}@endif @if(!empty($module_edit_url)){{ 'data-module_edit_url='.$module_edit_url }}@endif>{{ $option }}</option>
                @elseif (isset($item['attribute']))
                    <option value="{{ $key_option }}"
                            @if (isset($model))
                                @if (isset($item['attribute']))
                                    @foreach($model->values as $relation)
                                        @if ($relation->attributeValue->id == $key_option) selected
                                        @endif
                                    @endforeach
                                @else
                                    @foreach($model->$key as $relation)
                                        @if ($relation->id === $key_option) selected
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                    >{{ $option }}</option>
                @else
                    <option value="{{ $key_option }}" @if(isset($model) && $model[$key] == $key_option) selected @endif @if(isset($data['role']) && $data['role'] == $key_option) selected @endif>{{ $option }}</option>
                @endif
            @endforeach

        @endif

    @endif
</select>


