@if(isset($name_option))
    <input type="checkbox"
           class="{{ $classes }}"
           <?php if(isset($model[$key]) && !empty($model[$key])) {if(isset($repeatable->$name_option)){echo 'checked';}} ?>
           <?php if(!isset($model[$key]) && isset($item['checked'])) {echo 'checked';} ?>
           name="{{ $key }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $name_option }}]"
           id="{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>"
           @if($item['validate']) required @endif>

@else
    <input type="checkbox"
           @if(isset($model) && !empty($model[$key]) && $item['translatable'] == false){{ 'checked' }}@endif
           @if(!isset($model) && isset($item['checked'])){{ 'checked' }}@endif
           @if(isset($item['metabox'])) name="metabox[{{ $key }}]" @else name="{{ $key }}" @endif
           id="{{ $key }}" class="{{ $classes }}"  @if($item['validate']) required @endif>
@endif

