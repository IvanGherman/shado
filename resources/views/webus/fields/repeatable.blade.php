<div class="row">
    <div class="col-md-12">
        <fieldset class="{{ $key }} dd">
            <ol class="repeatable dd-list">
                @if(isset($model[$key]) && $model[$key])
                    @php $i = 0; $list_count = collect($model[$key])->count(); @endphp

                    @if ($key === 'images')
                        @if (!empty($model[$key]))
                            @php $images = json_decode($model[$key]);@endphp
                            @if ($images)
                                @foreach($images as $repeatable)
                                    @include('webus.fields.repeatable_group', ['i' => $i++])
                                @endforeach
                            @endif
                        @endif
                    @else
                        @foreach($model[$key] as $repeatable)
                            @include('webus.fields.repeatable_group', ['i' => $i++])
                        @endforeach
                    @endif

                    @php unset($model[$key]); unset($i); //reset foreach  @endphp
                @endif
            </ol>
            <div class="form-group col-md-12">
                <input type="button" value="Add Item" class="btn btn-default add" />
            </div>
        </fieldset>
    </div>
</div>

@push('scripts')

    <script type="text/template" id="{{ $key }}">
    @include('webus.fields.repeatable_group', ['repeatable' => ''])
    </script>

    <script>
        $(function() {
            $(".{{ $key }} .repeatable").repeatable({
                addTrigger: ".{{ $key }} .add",
                deleteTrigger: ".{{ $key }} .delete",
                template: "#{{ $key }}",
                startWith: {{ $item['start_with'] }},
                max: {{ $item['max'] }},
                list_count: @if(isset($list_count)){{ $list_count }}@else{{ '0' }}@endif,
                afterLoad: function () {
                    $(".dd").nestable({handleClass: 'handle_repeatable', maxDepth:1});
                },
                afterAdd: function () {
                    init_visual_editor();
                    translate_all(true);
                    $('select.select2').select2({ width: '100%' });
                    $(".dd").nestable({handleClass: 'handle_repeatable', maxDepth:1});
                    $('.input_image').filemanager('image');
                    $('.input_images').filemanager('images');
                    $('.input_gallery_repeatable').filemanager('images');
                }
            });
        });
        @if($key == 'modules')

        $(".modules .select2").on("change", function(e) {
            var get_edit_url = $(this).find('option:selected')[0].dataset.module_edit_url;
            var parent_div = $(this).parent().find('a').attr('href', get_edit_url);
        });
        @endif
    </script>
@endpush
