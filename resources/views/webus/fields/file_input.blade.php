@if(isset($name_option))

    <input id="{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>" type="text"
           name="{{ $key }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $name_option }}]"
           value="<?php if(isset($model) && !empty($model[$key])){if(isset($repeatable->$name_option)){echo htmlentities($repeatable->$name_option);}} ?>"
           class="form-control {{ $classes }}" @if($item['validate']) required @endif>
    <button type="button" class="input_image btn btn-dark" data-input="{{ $name_option }}_{?}" data-preview="preview_{{ $name_option }}">Select file</button>
    @if(isset($model) && !empty($model[$key]))
        <a class="btn-animate btn btn-success" href="<?php if(isset($model) && !empty($model[$key])){if(isset($repeatable->$name_option)){echo htmlentities($repeatable->$name_option);}} ?>" download>Download file</a>
    @endif

@else

    <input id="input_{{ $key }}" type="text"
           @if(isset($item['metabox'])) name="metabox[{{ $key }}]" @else name="{{ $key }}" @endif
           @if(isset($item['metabox']))
           value="@if(isset($model) && !empty($model[$key])){{$model[$key] }}@endif"
           @else
           value="@if(isset($model) && !empty($model[$key])){{ $model[$key] }}@endif"
           @endif
           class="form-control {{ $classes }}" @if($item['validate']) required @endif>
    <button type="button" class="input_image btn btn-dark" data-input="input_{{ $key }}" data-preview="preview_{{ $key }}">Select file</button>
    @if(isset($model) && !empty($model[$key]))
        <a class="btn-animate btn btn-success" href="{{ $model[$key] }}" download>Download file</a>
    @endif


@endif
