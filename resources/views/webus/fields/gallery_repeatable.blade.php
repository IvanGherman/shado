<br>
<button type="button" class="input_gallery_repeatable btn btn-dark" data-input="input_{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>" data-preview="preview_{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>" data-gallery-key="{{ $key }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $name_option }}][]">Select images</button>
<ul class="place-photos gallery-place_input_{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>">
    <?php if(isset($model) && !empty($model[$key])){if(isset($repeatable->$name_option)){
    $images = json_decode($repeatable->$name_option);
    if(count($images) > 0){
    foreach ($images as $image){
    ?>
    <li class="list_image">
        <div class="remove-image-galery">x</div>
        <img width="150" src="{{ $image }}" />
        <input type="hidden" name="{{ $key }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $name_option }}][]" value="{{ $image }}">
    </li>
    <?php
    }
    }
    }} ?>
</ul>
@push('gallery_field')
    <script>
        $( function() {
            $( ".place-photos").sortable();
            $( ".place-photos").disableSelection();
        } );

        $(document).on("click", ".remove-image-galery", function() {
            var item_image = $(this).parent();
            item_image.remove();
        });

    </script>
@endpush