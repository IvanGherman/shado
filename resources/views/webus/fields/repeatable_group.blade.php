<li class="field-group controls-row background-repeatable-group dd-item">

    <div class="col-md-8">
        <div class="row">

            @if(isset($item['options']))

                @foreach($item['options'] as $name_option=>$option)
                    @php
                    if($option['translatable']){
                    $classes = 'translate_this ';
                    }else{
                    $classes = ' ';
                    }
                    if($option['custom_class']){
                    $classes .= $option['custom_class'];
                    }else{
                    $classes .= '';
                    }
                    @endphp

                    <div class="{{ $classes }}">
                        <label for="{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>" class="handle_repeatable">{{ $option['title'] }} @if($option['validate'])<span class="required-field">*</span>@endif</label>
                        @if($key == 'modules' && isset($repeatable->$name_option)) <a href="{{ route('edit_'.\App\Http\Helpers\webus_help::get_code_module($repeatable->$name_option), ['setting_id' => $repeatable->$name_option]) }}" target="_blank">Edit module</a> @endif
                        @include('webus.partials.languages', ['repeatable_group' => true, 'name_repeatable' => $key, 'key' => $name_option, 'item'=> $option, 'classes' => $classes])
                        @include('webus.partials.field_rules', ['item' => $option])
                    </div>

                @endforeach

            @else

                <h2>Set options please</h2>

            @endif

        </div>

    </div>

    <div class="col-md-4">
        <label></label>
        <input type="button" class="btn btn-danger delete" value="X" />
    </div>


</li>
