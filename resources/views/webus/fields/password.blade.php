<input type="password" value="" class="form-control"
       @if(isset($item['metabox'])) name="metabox[{{ $key }}]"  @else name="{{ $key }}"  @endif
       placeholder="{{ $item['title'] }}" id="{{ $key }}" @if($item['validate']) required @endif>
