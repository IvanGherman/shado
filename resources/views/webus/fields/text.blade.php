@if(isset($name_option))

    <input type="text" class="col-md-12 form-control {{ $classes }}"
           name="{{ $key }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $name_option }}]"
           value="<?php if(isset($model[$key]) && !empty($model[$key])) {if(isset($repeatable->$name_option)){echo htmlentities($repeatable->$name_option);}} ?>"
           id="{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>"
           @if($item['validate']) required @endif>

@else

    @if(isset($item['slug']) && $item['slug'])
        <input type="text" data-output="{{$key}}" value="@if(isset($model) && $item['translatable'] == false){{ $model[$key] }}@endif" id="slugme_{{ $key }}" class="form-control" />
        <script>
            $(function(){
                $('#slugme_{{ $key }}').slugIt({
                    output:    '#{{ $key }}'
                });
            });
        </script>
    @endif

    <input type="text" value="@if(isset($model) && !empty($model[$key]) && $item['translatable'] == false){{ $model[$key] }}@endif" class="{{ $classes }} form-control @if(isset($item['slug']) && $item['slug'])hidden @endif"
           @if(isset($item['metabox'])) name="metabox[{{ $key }}]" @else  name="{{ $key }}" @endif
           placeholder="{{ $item['title'] }}" id="{{ $key }}" @if($item['validate']) required @endif >

@endif


