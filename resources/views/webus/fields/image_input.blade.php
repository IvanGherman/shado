@if(isset($name_option))
       <input id="{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>" type="text"
              name="{{ $key }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $name_option }}]"
              value="<?php if(isset($model) && !empty($model[$key])){if(isset($repeatable->$name_option)){echo htmlentities($repeatable->$name_option);}} ?>"
              class="form-control {{ $classes }}"
              @if($item['validate']) required @endif>
       <button type="button" class="input_image btn btn-dark" data-input="{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>" data-preview="preview_{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>">Select image</button>
       <img id="preview_{{ $name_option }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>" src="<?php if(isset($model) && !empty($model[$key])){if(isset($repeatable->$name_option)){echo htmlentities($repeatable->$name_option);}} ?>" class="<?php if(isset($model) && !empty($model[$key])){if(isset($repeatable->$name_option)){echo 'selected_image';}else{ echo 'empty_image';}} ?>">
@else
       <input id="input_{{ $key }}" type="text"

              @if(isset($item['metabox'])) name="metabox[{{ $key }}]" @else name="{{ $key }}" @endif
              @if(isset($item['metabox']))
              value="@if(isset($model) && !empty($model[$key])){{$model[$key] }}@endif"
              @else
              value="@if(isset($model) && !empty($model[$key])){{ $model[$key] }}@endif"
              @endif
              class="form-control {{ $classes }}" @if($item['validate']) required @endif>
       <button type="button" class="input_image btn btn-dark" data-input="input_{{ $key }}" data-preview="preview_{{ $key }}">Select image</button>
       <img id="preview_{{ $key }}" src="@if(isset($model) && !empty($model[$key])){{ $model[$key] }}@endif" class="@if(isset($model) && !empty($model[$key])){{ 'selected_image' }}@else{{ 'empty_image' }}@endif">
@endif
