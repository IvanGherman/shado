@if(isset($name_option))
    <button type="button" class="input_images btn btn-dark" data-input="{{ $name_option }}_{?}" data-preview="preview_{{ $name_option }}">Select file</button>
    <div class="place-photos gallery-place_input_{{ $name_option }}_{?}">
        @if(isset($model) && !empty($model[$key]))
            @php $images = $model[$key]; @endphp
            @if(count($images) > 0)
                @foreach($images as $image)
                    <li class="list_image"><img width="150" src="{{ $image }}" /><input type="hidden" name="metabox[{{ $key }}][]" value="{{ $image }}"></li>
                @endforeach
            @endif
        @endif
    </div>
@else
    @if(isset($item['metabox']))
        <div class="space"></div>
        <button type="button" class="input_images btn btn-dark" data-input="input_{{ $key }}" data-preview="preview_{{ $key }}">Select images</button>
        <ul class="place-photos gallery-place_input_{{ $key }}">
            @if(isset($model) && !empty($model[$key]))
                @php $images = $model[$key]; @endphp
                @if(count($images) > 0)
                    @foreach($images as $image)
                        <li class="list_image">
                            <img src="{{ $image }}" />
                            <br/>
                            <input type="hidden" name="metabox[{{ $key }}][]" value="{{ $image }}">
                            <button class="remove-image-galery btn-danger btn">REMOVE</button>
                        </li>
                    @endforeach
                @endif
            @endif
        </ul>
    @endif

    @if(isset($item['modules']))
        <div class="space"></div>
        <button type="button" class="input_images btn btn-dark" data-module="{{ $key }}" data-input="input_{{ $key }}"  data-preview="preview_{{ $key }}">Select images</button>
        <ul class="place-photos gallery-place_input_{{ $key }}">
            @if(isset($model) && !empty($model[$key]))
                @php $images = $model[$key]; @endphp
                @if(count($images) > 0)
                    @foreach($images as $image)
                        <li class="list_image">
                            <div class="remove-image-galery">x</div>
                            <img src="{{ $image }}" />
                            <input type="hidden" name="{{ $key }}[]" value="{{ $image }}">
                        </li>
                    @endforeach
                @endif
            @endif
        </ul>
    @endif

@endif

@push('gallery_field')
<script>
    $( function() {
        $( ".place-photos").sortable();
        $( ".place-photos").disableSelection();
    } );

    $(document).on("click", ".remove-image-galery", function() {
        var item_image = $(this).parent();
        item_image.remove();
    });

</script>
@endpush