<div class="row options-container">
    <div class="options-children">
        @if(isset($model[$key]) && $model[$key])
            @foreach($model[$key] as $number => $item)
                <div class="col-md-12">
                    <div class="col-md-4">
                        <input type="text" name="option-key-{{ $number }}">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="option-value-{{ $item }}">
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <div class="form-group col-md-12">
        <input type="button" value="Add Item" class="btn btn-default add add-option" />
    </div>
</div>

<script>
    $('.add-option').click(function () {
        var optionsChildren = $(this)
            .closest('.options-container')
            .find('.options-children');

        var counter = optionsChildren
            .children('div').length;
        console.log(counter);

        var key = $('<input type="text">')
            .attr('name', 'option[' + counter + '][key]')
            .addClass('form-control translate_this')
            ;
        var value = $('<input type="text">')
            .attr('name', 'option[' + counter + '][value]')
            .addClass('form-control translate_this')
            ;
        var container = $('<div></div>').addClass('col-md-4');
        var container2 = $('<div></div>').addClass('col-md-4');

        var megaContainer = $('<div></div>').addClass('col-md-12');
        container.append(key);
        container2.append(value);
        megaContainer.append(container).append(container2);

        optionsChildren.append(megaContainer);
    });
</script>