<tr>
    <td>
        <input type="checkbox" name="posts[]" class="checkbox-list-posts" value="{{$item->id}}" onclick="if_is_checked_list_posts()">
    </td>
    @php $nr = 0; @endphp
    @foreach($data['rows'] as $key=>$row)  @php $nr++; @endphp
    <td>
        @if(isset($child) && $nr == 1)
            {{ '&nbsp;&nbsp;&nbsp;&nbsp;-' }}
        @endif
        @if($nr == 1 && isset($item->parent->name)) {!! '<small>'.$item->parent->name.'</small> >'!!}@endif
        @if($row == 'translatable')
            {{ \App\Http\Helpers\webus_help::translate_automat($item->$key) }}
        @elseif($row == 'image_src' && !empty($item->$key))
            <img src="{{asset($item->$key)}}" class="img-responsive" style="width:100px" />
        @elseif($row == 'status')
            @php
            if($item->$key == 1){echo 'Enabled';}else{echo 'Disabled';}
            @endphp
        @elseif($row == 'taxonomy')
            @php
            $post_terms = \App\WebusModels\TaxonomyTerms::get_post_terms_full($key,$item->id,$data['slug'])->pluck('name','id');
            @endphp
            @if($post_terms)
                @foreach($post_terms as $term)
                    {{ $term }}
                @endforeach
            @endif

        @else
            {{ $item->$key }}
        @endif
    </td>
    @endforeach

    <td class="no-sort no-click" id="bread-actions">
        <a href="{{ url($data['url_edit'].$item->id) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
        </a>
        <!-- <a href="#" title="View" class="btn btn-sm btn-warning pull-right">
             <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>
         </a>-->
    </td>
</tr>
@if(isset($item->children) && $item->children->count())
    @foreach($item->children as $child)
        @include('webus.standart.item-row', ['item' => $child, 'child' => 'yes'])
    @endforeach
@endif