@extends('layouts.webus')
@section('css')
    <style>
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding: 0px;
            line-height: 1.42857143;
            vertical-align: middle;
            border-top: 1px solid #ddd;
            padding-left: 5px;
        }
        .delete_in_form {
            margin-top: -15px;
            margin-left: 0px;
        }
    </style>
@endsection
@section('content')

    <h1 class="page-title">
        <i class="voyager-list-add"></i> {{ $data['title'] }}
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <table class="row table table-hover webus-table">

            <thead>
            <tr>
                <th>Module name</th>
                <th class="actions">Actions</th>
            </tr>

            </thead>

            <tbody>
            @foreach($data['list'] as $key=>$item)
                <tr class="module-name">
                    <td>{{ $key }}</td>
                    <td class="no-sort no-click" id="bread-actions">
                        <a href="{{ url('admin/modules/add/'.$key) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                            <i class="voyager-plus"></i> <span class="hidden-xs hidden-sm">Add</span>
                        </a>
                    </td>
                </tr>
                @if(count($item) > 0)
                    @foreach($item as $modul)
                        <tr>
                            <td><strong>{{ '[ID:'.$modul->id.']' }}</strong>&nbsp;&nbsp; - {{ $modul->name }} </td>
                            <td class="no-sort no-click" id="bread-actions">
                                @if($modul->module_status == 0){!! '<span class="warning">Disabled</span>' !!}@elseif($modul->module_status == 1){!! '<span class="success">Enabled</span>' !!}@endif
                                <a href="{{ route('edit_'.$key, ['setting_id' => $modul->id]) }}" title="Edit" class="btn btn-sm btn-warning pull-right edit">
                                    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                </a>
                                <form action="{{ route('delete_'.$key, ['setting_id' => $modul->id]) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?');">
                                    {{ method_field("POST") }}
                                    {{ csrf_field() }}
                                    <button title="Delete" class="btn btn-sm btn-danger pull-right delete_in_form delete">
                                        <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                    </button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                @endif
            @endforeach
            </tbody>
        </table>

    </div>
@endsection
