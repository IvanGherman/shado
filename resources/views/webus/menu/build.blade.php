@extends('layouts.webus')

@section('css')
    <link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/nestable.css">
@endsection
@section('content')

    <h1 class="page-title">
        <i class="voyager-list-add"></i> {{ $data['title'] }}
        <button type="button" class="btn btn-success add_item">
            <i class="voyager-plus"></i> Add New
        </button>

    </h1>

    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <p class="panel-title" style="color:#777">Drag and drop the menu Items below to re-arrange
                            them.</p>
                    </div>

                    <div class="panel-body" style="padding:30px;">

                        <div class="dd">
                            {!! $data['items'] !!}
                        </div>

                    </div>

                </div>


            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> Are you sure you want to delete this menu
                        item?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('menu.delete_item', ['menu' => $data['menu_id'], 'id' => '__id']) }}" id="delete_form"
                          method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="Yes, Delete This Menu Item">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal modal-success fade" tabindex="-1" id="add_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-plus"></i> Create a New Menu Item</h4>
                </div>
                <form action="{{ $data['form_action_add_new_item'] }}" id="add_form" method="POST">

                    {{ csrf_field() }}

                    <div class="modal-body">
                        <label for="name">Title of the Menu Item</label>
                        @if($data['translatable'])
                            <ul class="change_langs">
                                @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                                    <li class="change_lang" data_lang="{{$localeCode}}">{{$localeCode}}</li>
                                @endforeach
                            </ul>
                            @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                                <input
                                        name="translatable[name][{{$localeCode}}]"
                                        id="translatable_add_title_{{$localeCode}}"
                                        type="hidden"
                                        value="">
                            @endforeach
                        @endif
                        <input type="text" class="@if($data['translatable']){{'translate_this'}}@endif form-control" id="add_title" name="name" placeholder="Title">
                        {{--<label for="url">URL for the Menu Item</label>
                        <input type="text" class="form-control" name="url" placeholder="URL">--}}

                        <br>
                        <style>
                            .ui-front { z-index: 9999999!important;}
                        </style>
                        <label for="available_urls">URL for the Menu: </label>
                        <input id="available_urls" class="form-control" name="url">
                        <br>

                        <label for="icon_class">Font Icon class for the Menu Item (Use a <a
                                    href="{{ route('icons') }}"
                                    target="_blank">Voyager Font Class</a>)</label>
                        <input type="text" class="form-control" name="icon"
                               placeholder="Icon Class (optional)"><br>
                        <label for="class">Custom Class</label>
                        <input type="text" class="form-control" name="class"
                               placeholder="Custom css class for item menu"><br>
                        <label for="target">Open In</label>
                        <select id="edit_target" class="form-control" name="target">
                            <option value="_self">Same Tab/Window</option>
                            <option value="_blank">New Tab/Window</option>
                        </select>

                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success pull-right" value="Add New Item">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal modal-info fade" tabindex="-1" id="edit_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-edit"></i> Edit Menu Item</h4>
                </div>
                <form action="{{ $data['form_action_update_item'] }}" id="edit_form" method="POST">

                    <input type="hidden" name="_method" value="PUT">

                    {{ csrf_field() }}

                    <div class="modal-body">

                        <label for="name">Title of the Menu Item</label>
                        @if($data['translatable'])
                            <ul class="change_langs">
                                @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                                    <li class="change_lang" data_lang="{{$localeCode}}">{{$localeCode}}</li>
                                @endforeach
                            </ul>
                            @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                                <input
                                        name="translatable[name][{{$localeCode}}]"
                                        id="translatable_name_{{$localeCode}}"
                                        type="hidden"
                                        value="">
                            @endforeach
                        @endif
                        <input type="text" class="@if($data['translatable']){{'translate_this'}}@endif form-control" id="name" name="name" placeholder="Title"><br>

                        {{--<label for="url">URL for the Menu Item</label>
                        <input type="text" class="form-control" id="edit_url" name="url" placeholder="URL">--}}

                        <label for="edit_url">URL for the Menu: </label>
                        <input id="edit_url" class="form-control" name="url">

                        <br>
                        <label for="icon_class">Font Icon class for the Menu Item (Use a <a
                                    href="{{ route('icons') }}"
                                    target="_blank">Voyager Font Class</a>)</label>
                        <input type="text" class="form-control" id="edit_icon_class" name="icon"
                               placeholder="Icon Class (optional)"><br>

                        <label for="class">Class </label>
                        <input type="text" class="form-control" id="edit_class" name="class"
                               placeholder="Own class"><br>
                        <label for="target">Open In</label>

                        <select id="edit_target" class="form-control" name="target">
                            <option value="_self" selected="selected">Same Tab/Window</option>
                            <option value="_blank">New Tab/Window</option>
                        </select>

                        <input type="hidden" name="item_id" id="edit_id" value="">
                        <input type="hidden" name="parent_id" id="edit_parent_id" value="">
                    </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success pull-right delete-confirm" value="Update">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    </div>

@endsection

@section('javascript')
    <script type="text/javascript" src="/webus/admin_assets/js/jquery.nestable.js"></script>
    <script>
        $(document).ready(function () {


            $('.dd').nestable({/* config options */});

            /**
             * Delete menu item
             */
            $('.item_actions').on('click', '.delete', function (e) {
                id = $(e.currentTarget).data('id');
                $('#delete_form')[0].action = $('#delete_form')[0].action.replace("__id",id);
                $('#delete_modal').modal('show');
            });

            /**
             * Edit menu item
             */
            $('.item_actions').on('click', '.edit', function (e) {
                var id = $(e.currentTarget).data('id');

                $('#name').val($(e.currentTarget).data('title'));
                $('#edit_url').val($(e.currentTarget).data('url'));
                $('#edit_icon_class').val($(e.currentTarget).data('icon_class'));
                $('#edit_class').val($(e.currentTarget).data('class'));
                $('#edit_parent_id').val($(e.currentTarget).data('parent_id'));
                $('#edit_id').val(id);



                if ($(e.currentTarget).data('target') == '_self') {
                    $("#edit_target").val('_self').change();
                } else if ($(e.currentTarget).data('target') == '_blank') {
                    $("#edit_target option[value='_self']").removeAttr('selected');
                    $("#edit_target option[value='_blank']").attr('selected', 'selected');
                    $("#edit_target").val('_blank');
                }

                if ($(e.currentTarget).data('route') != "") {
                    $("#edit_type").val('route').change();
                    $("#url_type").hide();
                } else {
                    $("#route_type").hide();
                }

                if ($("#edit_type").val() == 'route') {
                    $("#url_type").hide();
                    $("#route_type").show();
                } else {
                    $("#routel_type").hide();
                    $("#url_type").show();
                }

                $('#edit_modal').modal('show');

                @if($data['translatable'])
                //Start translated Title Item
                @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                        $('#translatable_name_{{ $localeCode }}').val($(e.currentTarget).data('lang_{{ $localeCode }}'));
                        @endforeach

                var lang = localStorage.getItem('translatable_lang');

                if(lang == null || lang == 'undefined'){
                    var get_first_lang_from_list = $('.change_langs li:first-child').attr('data_lang');
                    localStorage.setItem('translatable_lang', get_first_lang_from_list);
                }

                var lang = localStorage.getItem('translatable_lang');
                var get_translatable_fields = $('.translate_this');

                get_translatable_fields.each(function(index,item){
                    var from_lang = $('#translatable_'+ item['id'] +'_' + lang + '').val();

                    $('#' + item['id'] + '').val(from_lang);

                    //for richtextBox
                    if($(item).hasClass('richTextBox')){
                        tinymce.get($(item).attr('id')).setContent(from_lang);
                    }

                });
                //End translated Title Item
                @endif
            });


            $('.add_item').click(function () {

                $('#add_modal').modal('show');
            });

            /**
             * Reorder items
             */
            $('.dd').on('change', function (e) {
                $.post('{{ $data['order_action'] }}', {
                    order: JSON.stringify($('.dd').nestable('serialize')),
                    _token: '{{ csrf_token() }}'
                }, function (data) {
                    toastr.success("Successfully updated menu order.");
                });

            });

            $('#edit_type').on('change', function (e) {
                if ($("#edit_type").val() == 'route') {
                    $("#url_type").hide();
                    $("#route_type").show();
                } else {
                    $("#routel_type").hide();
                    $("#url_type").show();
                }
            });

        });
    </script>

    <script>{{--   ADD NEW --}}
        $( function() {
            var availableTags = {!! $available_urls !!};
            $( "#available_urls" ).autocomplete({
                source: availableTags
            });

            $( "#edit_url" ).autocomplete({
                source: availableTags
            });

        } );
    </script>
@endsection
