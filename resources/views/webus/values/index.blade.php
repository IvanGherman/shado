@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-@if(!empty($data['icon'])){{ $data['icon'] }}@else{{ 'list-add' }}@endif"></i> {{ $data['title'] }}
        <a href="{{ $data['url_create'] }}" class="btn btn-success">
            <i class="voyager-plus"></i> Add New
        </a>
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <form id="list_form" action="{{url($data['url_delete_mass'])}}" method="post">
            {{ csrf_field() }}
            <table class="row table table-hover webus-table">

                <thead>
                <tr>
                    <th><input id="toggle_all" type="checkbox" onClick="toggle_all_checkboxes(this)" /> <label for="toggle_all"><strong>All</strong></label></th>
                    @foreach($data['rows'] as $key=>$row)
                        <th> @if($key != null){{ str_replace('_', ' ', $key) }} @else {{ $row }} @endif </th>
                    @endforeach
                    <th class="actions">Actions</th>
                </tr>

                </thead>

                <tbody>
                @foreach($data['list'] as $item)
                    @include('webus.values.item-row')
                @endforeach
                </tbody>
            </table>
        </form>
        {{ $data['list']->links() }}

    </div>
@endsection
