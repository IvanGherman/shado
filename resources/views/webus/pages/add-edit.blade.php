@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-@if(!empty($data['icon'])){{ $data['icon'] }}@else{{ 'list-add' }}@endif"></i> {{ $data['title'] }}
        @if(!empty($data['url_create']))
            <a href="{{ $data['url_create'] }}" class="btn btn-success">
                <i class="voyager-plus"></i> Add New
            </a>
        @endif
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">


                    <!-- form start -->
                    <form role="form" action="{{ $data['form_action'] }}" method="POST" enctype="multipart/form-data">


                        @if(isset($model))
                            {{ method_field("PUT") }}
                        @else
                            {{ method_field("POST") }}
                        @endif

                        {{ csrf_field() }}

                        <div class="panel-body">
                            @foreach($data['rows'] as $key => $item)
                                @php
                                    if($item['translatable']){
                                    $classes = 'translate_this ';
                                    }else{
                                    $classes = ' ';
                                    }
                                    if($item['custom_class']){
                                    $classes .= $item['custom_class'];
                                    }else{
                                    $classes .= '';
                                    }
                                @endphp
                                <div class="form-group {{ $classes }}">
                                    @include('webus.partials.fields')
                                </div>
                            @endforeach

                        </div><!-- panel-body -->



                        @if(isset($data['metabox']) && $data['metabox'])
                            <div class="panel panel-bordered panel-default remove-radius">

                                <div class="panel-heading">
                                    <h3 class="panel-title">Advanced</h3>
                                </div>

                                <div class="panel-body">
                                    @foreach($data['metabox'] as $key => $item)
                                        @php
                                            if($item['translatable']){
                                            $classes = 'translate_this ';
                                            }else{
                                            $classes = ' ';
                                            }
                                            if($item['custom_class']){
                                            $classes .= $item['custom_class'];
                                            }else{
                                            $classes .= '';
                                            }
                                        @endphp
                                        <div class="form-group">
                                            @include('webus.partials.fields')
                                        </div>
                                    @endforeach
                                </div>

                            </div>

                        @endif



                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                    @if(isset($model) && !in_array($model->slug, ['faq', 'home_page', 'contacts', 'news']))

                        <form action="{{ url($data['url_delete'].$model['id']) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?');">
                            {{ method_field("POST") }}
                            {{ csrf_field() }}
                            <button title="Delete" class="btn btn-sm btn-danger pull-left delete_in_form delete">
                                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                            </button>
                        </form>
                    @endif

                </div>
            </div>
        </div>

    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/nestable.css">
@endsection
@section('javascript')
    <script src="/webus/admin_assets/lib/js/tinymce/tinymce.min.js"></script>
    <script src="/webus/admin_assets/js/voyager_tinymce.js"></script>
    <script type="text/javascript" src="/webus/admin_assets/js/jquery.nestable.js"></script>
    @stack('scripts')
    @stack('gallery_field')
@endsection

