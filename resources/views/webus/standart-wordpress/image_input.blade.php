<input id="input_{{ $key }}" type="text" name="{{ $key }}" value="@if(isset($model) && !empty($data['thumbnail'])){{config('app.url').$data['thumbnail'] }}@endif" class="form-control" @if($item['validate']) required @endif>
<button type="button" class="input_image btn btn-dark" data-input="input_{{ $key }}" data-preview="preview_{{ $key }}">Select file</button>
<img id="preview_{{ $key }}" src="@if(isset($model) && !empty($data['thumbnail'])){{ $data['thumbnail'] }}@endif" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">

