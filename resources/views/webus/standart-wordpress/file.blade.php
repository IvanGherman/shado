@if(isset($model) && !empty($data['thumbnail']))
    <img src="@if(isset($model)){{ $data['thumbnail'] }}@endif" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
    <label for="remove_{{$key}}">Remove attachment from this post</label>
    <input type="checkbox" name="remove[{{$key}}]" id="remove_{{$key}}">
@endif
<input type="file" name="{{ $key }}" @if($item['validate']) required @endif>

