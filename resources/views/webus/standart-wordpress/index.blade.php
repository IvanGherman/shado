@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-list-add"></i> {{ $data['title'] }}
        <a href="{{ $data['url_create'] }}" class="btn btn-success">
            <i class="voyager-plus"></i> Add New
        </a>
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-12">
            <div id="dataTable_filter" class="dataTables_filter">
                <form role="form" action="" method="GET">
                    <label>Search:<input type="search" name="search" value="@php if(isset($_GET['search']) && $_GET['search']){echo $_GET['search'];} @endphp" class="form-control input-sm" placeholder="" aria-controls="dataTable"></label>
                </form>

            </div>
        </div>

        <table class="row table table-hover webus-table">

            <thead>
            <tr>
                @foreach($data['rows'] as $key=>$row)
                    <th> @if($key != null){{ str_replace('_', ' ', $key) }} @else {{ $row }} @endif </th>
                @endforeach
                <th class="actions">Actions</th>
            </tr>

            </thead>

            <tbody>
            @foreach($data['list'] as $item)
                <tr>

                    @foreach($data['rows'] as $key=>$row)
                        <td>
                            @if($row == 'translatable')
                                {{ \App\Http\Helpers\webus_help::translate_automat($item->$key) }}
                            @elseif($row == 'image_src' && !empty($item->$key))
                                <img src="/storage/{{$item->$key}}" class="img-responsive" style="width:100px" />
                            @else
                                {{ $item->$row }}
                                @if($row == 'thumbnail' && !empty(webus_help::get_thumb_by_id($item->ID, $data['old_path'])))
                                    <img src="{{webus_help::get_thumb_by_id($item->ID, $data['old_path'])}}" class="img-responsive" style="width:100px" />
                                @endif
                            @endif
                        </td>
                    @endforeach

                    <td class="no-sort no-click" id="bread-actions">
                        <form action="{{ url($data['url_delete'].$item->ID) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this item?');">
                            {{ method_field("POST") }}
                            {{ csrf_field() }}
                            <button title="Delete" class="btn btn-sm btn-danger pull-right delete">
                                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Delete</span>
                            </button>
                        </form>

                        <a href="{{ url($data['url_edit'].$item->ID) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                        </a>
                        <!-- <a href="#" title="View" class="btn btn-sm btn-warning pull-right">
                             <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>
                         </a>-->
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $data['list']->links() }}

    </div>
@endsection
