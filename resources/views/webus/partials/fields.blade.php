@if($item['html_type'] == 'info')
    @include('webus.fields.info')
@else
    @if($key == 'modules'){!! '<hr>' !!}@endif
    <label for="{{ $key }}" class="heading_field">{{ $item['title'] }} @if($item['validate'])<span class="required-field">*</span>@endif</label>

    @include('webus.partials.languages')

    @include('webus.partials.field_rules')
@endif



