<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('dashboard') }}">
                    <div class="logo-icon-container">
                        <img src="/webus/admin_assets/images/logo-icon-light.png" alt="Logo Icon">
                    </div>
                    <div class="title">{{ config('app.admin_name', 'Webus Admin') }}</div>
                </a>
            </div><!-- .navbar-header -->

            <div class="panel widget center bgimage"
                 style="background-image:url(/webus/admin_assets/images/bg.jpg);">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <img src="/webus/admin_assets/images/captain-avatar.png" class="avatar" alt="Webus avatar">
                    <h4>{{ Auth::user()->name }}</h4>
                    <p>{{ Auth::user()->email }}</p>

                    <a href="{{ route('profile') }}" class="btn btn-primary">Profile</a>
                    <div style="clear:both"></div>
                </div>
            </div>

        </div>

           {!! \App\Http\Helpers\webus_help::menu('backend', 'admin') !!}

    </nav>
</div>
