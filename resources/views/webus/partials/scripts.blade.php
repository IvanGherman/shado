<script type="text/javascript" src="/webus/admin_assets/js/sidebar.js"></script>
<script type="text/javascript" src="/webus/admin_assets/lib/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/webus/admin_assets/lib/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="/webus/admin_assets/lib/js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="/webus/admin_assets/lib/js/toastr.min.js"></script>
<script type="text/javascript" src="/webus/admin_assets/lib/js/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/select2/select2.min.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/webus/admin_assets/settings/buttonsTinyMce.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/app.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/helpers.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/jquery.repeatable.js"></script>
@yield('javascript')
<script type="text/javascript" src="/webus/admin_assets/js/media/media-from-input.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/slugify.js"></script>
<script type="text/javascript" src="/webus/admin_assets/js/translatable.js"></script>
@if(config('app.faker_text'))
    <script type="text/javascript" src="/webus/admin_assets/js/faker_text.js"></script>
@endif

{{--<script type="text/javascript" src="/webus/admin_assets/lib/js/jquery.dataTables.min.js"></script>--}}
{{--<script type="text/javascript" src="/webus/admin_assets/lib/js/dataTables.bootstrap.min.js"></script>--}}