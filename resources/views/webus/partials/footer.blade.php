<footer class="app-footer">
    <div class="site-footer-right">
        Made with <i class="voyager-heart"></i> by <a href="http://webus.md" target="_blank">Webus</a>
        - v0.11.7
    </div>
</footer>
@include('webus.partials.scripts')

