@if ($item['translatable'])
    <ul class="change_langs">
        @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
            <li class="change_lang" data_lang="{{$localeCode}}">{{$localeCode}}</li>
        @endforeach
    </ul>

    @if (isset($repeatable_group))
        @foreach (LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
            <input
                    name="{{ $name_repeatable }}[<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>][{{ $key }}_{{$localeCode}}]"
                    id="translatable_{{ $key }}_<?php if(isset($i)){echo 'new'.$i;}else{echo '{?}';} ?>_{{$localeCode}}"
                    type="hidden"
                    @if (isset($model) && !empty($model[$name_repeatable]))
                    <?php $id_group = $key.'_'.$localeCode; ?>
                    @if (isset($repeatable->$id_group))
                    value="<?php echo htmlentities($repeatable->$id_group); ?>"
                    @else
                    value="<?php echo htmlentities($repeatable->$key); ?>"
                    @endif
                    @endif
            >
        @endforeach
    @else
        @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
            <input
                    @if(isset($item['metabox']))
                        name="meta_translatable[{{ $key }}][{{$localeCode}}]"
                    @else
                        name="translatable[{{ $key }}][{{$localeCode}}]"
                    @endif
                    id="translatable_{{ $key }}_{{$localeCode}}"
                    type="hidden"
                    value="@if(isset($model) && !empty($model[$key])){{ \App\Http\Helpers\webus_help::translate_manual($localeCode, $model[$key]) }}@else<?php if(isset($item['default'])): ?>{{ webus_help::translate_manual($localeCode, $item['default'])  }}<?php endif;?>@endif">
        @endforeach
    @endif

@endif
