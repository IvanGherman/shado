<!-- Fonts -->
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400|Lato:300,400,700,900' rel='stylesheet'
      type='text/css'>

<!-- CSS Libs -->
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/bootstrap-switch.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/checkbox3.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/toastr.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/lib/css/perfect-scrollbar.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/bootstrap-toggle.min.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/js/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/js/datetimepicker/bootstrap-datetimepicker.min.css">
<!-- CSS App -->
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/style.css">
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/css/themes/flat-blue.css">
<link rel="stylesheet" type="text/css" href="/css/admin.css">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic">

<!-- Favicon -->
<link rel="shortcut icon" href="/webus/admin_assets/images/logo-icon.png" type="image/x-icon">

<!-- CSS Fonts -->
<link rel="stylesheet" href="/webus/admin_assets/fonts/voyager/styles.css">
<script type="text/javascript" src="/webus/admin_assets/lib/js/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="/webus/admin_assets/js/select2/select2.min.css">

<!-- WebusAdmin CSS -->
<link rel="stylesheet" href="/webus/admin_assets/css/webus_admin.css">
@yield('css')
<!-- Few Dynamic Styles -->
<style type="text/css">
    .flat-blue .side-menu .navbar-header, .widget .btn-primary, .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
        background:#22A7F0;
        border-color:#22A7F0;
    }
    .breadcrumb a{
        color:#22A7F0;
    }
</style>
