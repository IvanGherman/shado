<script src="/webus/admin_assets/js/media/dropzone.js"></script>
<script src="/webus/admin_assets/js/media/media.js"></script>
<script type="text/javascript">
    var media = new VoyagerMedia({
        baseUrl: "{{ route('dashboard') }}"
    });
    $(function () {
        media.init();
    });
</script>
