@extends('layouts.webus')

@section('css')
   @include('webus.media.styles')
    <style>
        .insert_to_tinymnce{display: none!important;}
    </style>
@stop



@section('content')

@include('webus.media.file-manager-content')


@section('javascript')
  @include('webus.media.scripts')
@endsection


@stop
