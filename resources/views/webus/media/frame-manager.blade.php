<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
<meta charset="utf-8">
<title>File Manager</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('webus.partials.head')
    @include('webus.media.styles')

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

        var path_absolute = '{{ route('dashboard') }}';

    </script>
    <style>
        footer{display:none!important;}
    </style>
</head>
<body>


@include('webus.media.file-manager-content')

@include('webus/partials.footer')
@include('webus.media.scripts')


</body>
</html>
