@extends('layouts.webus')
@section('content')

    <h1 class="page-title">
        <i class="voyager-@if(!empty($data['icon'])){{ $data['icon'] }}@else{{ 'list-add' }}@endif"></i> {{ $data['title'] }}
        <a href="{{ $data['url_create'] }}" class="btn btn-success">
            <i class="voyager-plus"></i> Add New
        </a>
    </h1>

    <div class="page-content with_padding">

        <div class="col-md-12">
            <div class="row">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-12">
            <div id="dataTable_filter" class="dataTables_filter">
                <form role="form" action="" method="GET">
                    <label>Search:<input type="search"
                                         name="search"
                                         value="@php if(isset($_GET['search']) && $_GET['search']){echo $_GET['search'];} @endphp"
                                         class="form-control input-sm"
                                         placeholder=""
                                         aria-controls="dataTable">
                    </label>
                    <label>
                        <input type="checkbox"
                               name="show_in_catalog"
                                @php if(isset($_GET['show_in_catalog']) && $_GET['show_in_catalog']){echo 'checked';} @endphp
                        > Show in catalog
                    </label>
                    <button type="confirm" class="btn btn-primary">Apply</button>
                </form>
                {{--<button type="button" --}}
                        {{--class="btn btn-danger btn-sm mass_show_in_catalog" --}}
                        {{--disabled --}}
                        {{--onclick="if(confirm('Are you sure you want to set *SHOW IN CATALOG* for this items?')){ showInCatalog() }">Set show in catalog</button>--}}
                <button type="button"
                        class="btn btn-danger btn-sm mass_delete"
                        disabled
                        onclick="if(confirm('Are you sure you want to delete this items?')){ $('#list_form').submit(); }">Delete</button>
            </div>
        </div>
        <form id="list_form" action="{{url($data['url_delete_mass'])}}" method="post">
            {{ csrf_field() }}
            <table class="row table table-hover webus-table">

                <thead>
                <tr>
                    <th>
                        <input id="toggle_all" type="checkbox" onClick="toggle_all_checkboxes(this)" /> 
                        <label for="toggle_all"><strong>All</strong></label>
                    </th>
                    @foreach($data['rows'] as $key=>$row)
                        <th> @if($key != null){{ str_replace('_', ' ', $key) }} @else {{ $row }} @endif </th>
                    @endforeach
                    <th class="actions">Actions</th>
                </tr>

                </thead>

                <tbody>
                @foreach($data['list'] as $item)
                    @include('webus.products.item-row')
                @endforeach
                </tbody>
            </table>
        </form>
        {{ $data['list']->links() }}

    </div>
    {{--<script>--}}
        {{--$('.webus-table input[type=checkbox]').on('click', function () {--}}
            {{--if ($('.webus-table input[type=checkbox]:checked').length > 0) {--}}
                {{--$('.mass_show_in_catalog').attr('disabled', false);--}}
            {{--} else {--}}
                {{--$('.mass_show_in_catalog').attr('disabled', true);--}}
            {{--}--}}
        {{--});--}}
        {{--function showInCatalog() {--}}
            {{--var data = $('.webus-table input[type=checkbox]:checked');--}}
            {{--var ids = [];--}}
            {{--$.each(data, function (key,value) {--}}
                {{--ids.push($(this).val());--}}
            {{--});--}}
            {{--console.log(ids);--}}
            {{--$.ajax({--}}
                {{--url: '{{ route('products.show_in_catalog') }}',--}}
                {{--method: 'POST',--}}
                {{--data: {'ids' : ids}--}}
            {{--}).done(function () {--}}
                {{--location.reload();--}}
            {{--});--}}
        {{--}--}}
    {{--</script>--}}
@endsection
