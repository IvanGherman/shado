@extends('layouts.front')

@section('content')
    <div class="redirect">
        <div class="container container--medium">
            <div class="redirect__content">
                <div class="redirect__wrong-browser-img"><img src="assets/img/Frame.svg" width="168" height="105" alt=""/></div>
                <h2 class="title--xl redirect__title">Наш сайт не поддерживает Internet Explorer</h2>
                <p class="redirect__text">Пожалуйста, воспользуйтесь другим браузером</p>
                <div class="redirect__content-wrapper">
                    <ul class="redirect__browsers-list">
                        <li class="redirect__browsers-item"><a href="https://www.google.com/chrome/">
                                <img src="assets/img/browser-chrome.svg" alt="chrome"/></a>
                        </li>
                        <li class="redirect__browsers-item">
                            <a href="https://www.mozilla.org/ru/firefox/new/">
                                <img src="assets/img/browser-firefox.svg" alt="firefox"/>
                            </a>
                        </li>
                        <li class="redirect__browsers-item">
                            <a href="https://support.apple.com/downloads/safari">
                                <img src="assets/img/browser-safari.svg" alt="safari"/>
                            </a>
                        </li>
                        <li class="redirect__browsers-item">
                            <a href="https://www.opera.com/ru/download">
                                <img src="assets/img/browser-opera.svg" alt="opera"/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection


