@extends('layouts.front')
@section('content')
    <div class="page-block start-block product-page-start">
        <div class="container animateMe">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="start-block__text">
                        <div class="start-block__text-title">{{ $title }}</div>
                        <div class="start-block__text-content">Добро пожаловать, <span class="cabinet-user-name">{{ \Illuminate\Support\Facades\Auth::user()->name }}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-block users-cabinet-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 animateMe bio-col">
                    <div class="users-cabinet-checkout clear_end">
                        @include('front.shop.account-menu')
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 bio-col">
                    <div class="checkout profile-checkout animateMe">
                        <div class="block-title-zone">
                            <div class="checkout__title">{{ $subtitle }}</div>
                        </div>
                        <div class="zone__content clear_end">
                            <div class="row">
                                @if(Session::has('flash_message'))
                                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                            @if($addresses)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>Name</td>
                                        <td>Street</td>
                                        <td>Phone</td>
                                        <td>Index</td>
                                        <td>Country</td>
                                        <td>City</td>
                                        <td>Phone</td>
                                        <td>Action</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($addresses as $address)
                                        <tr>
                                            <td>{{ $address->name }}</td>
                                            <td>{{ $address->street }}</td>
                                            <td>{{ $address->phone }}</td>
                                            <td>{{ $address->index }}</td>
                                            <td>{{ $address->country['name'] }}</td>
                                            <td>{{ $address->city }}</td>
                                            <td>{{ $address->phone }}</td>
                                            <td><a href="{{ route('account_edit_address', [$address->id]) }}" class="page-button page-button__green green__light transition">Edit</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection