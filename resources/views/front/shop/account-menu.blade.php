@foreach($menu as $item_menu)
    <div class="users-cabinet-checkout-item">
        <div class="checkout__column-name">{{ $item_menu['title'] }}</div>
        <ul class="left-side-bar-list">
            @if(count($item_menu['items']))
                @foreach($item_menu['items'] as $url => $item)
                    <li class="transition @if($url == url()->current()){{'active'}}@endif"><a href="{{ $url }}">{{ $item }}</a> </li>
                @endforeach
            @endif
        </ul>
    </div>
@endforeach