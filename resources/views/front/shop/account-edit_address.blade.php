@extends('layouts.front')
@section('content')
    <div class="page-block start-block product-page-start">
        <div class="container animateMe">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="start-block__text">
                        <div class="start-block__text-title">{{ $title }}</div>
                        <div class="start-block__text-content">Добро пожаловать, <span class="cabinet-user-name">{{ \Illuminate\Support\Facades\Auth::user()->name }}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-block users-cabinet-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 animateMe bio-col">
                    <div class="users-cabinet-checkout clear_end">
                        @include('front.shop.account-menu')
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 bio-col">
                    <div class="checkout profile-checkout animateMe">
                        <div class="block-title-zone">
                            <div class="checkout__title">{{ $subtitle }}</div>
                        </div>
                        <div class="zone__content clear_end checkout__column" style="width: 100%">
                            <div class="row">
                                @if(Session::has('flash_message'))
                                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                                {{ Form::model($record,[
                                'method' => 'POST',
                                'route' => ['account_edit_address_action', $record->id],
                                'files' => false,
                                'class'=> 'form',
                                ]) }}
                                <label for="">ФИО</label>
                                {{ Form::input('text', 'name', null) }}

                                <label for="">Страна</label>
                                {{ Form::select('country_id', $countries, null, ['class' => 'form-select']) }}

                                <label for="">Город</label>
                                {{ Form::input('text', 'city', null) }}

                                <label for="">Адрес</label>
                                {{ Form::input('text', 'street', null) }}

                                <label for="">Индес</label>
                                {{ Form::input('text', 'index', null) }}

                                <label for="">Телефон</label>
                                {{ Form::input('text', 'phone', null) }}

                                <button type="submit" class="page-button page-button__green green__light transition">Продолжить</button>
                                <button type="button" class="delete_address page-button page-button__green green__light transition">Удалить</button>

                            {{ Form::close() }}
                            {{  Form::open(['route' => ['account_delete_address_action', $record->id], 'class' => 'hidden', 'id'=> 'delete_address']) }}
                            <button type="submit">Удалить</button>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).on('click', '.delete_address', function (event) {
         if(confirm('Вы уверены, что хотите удалить этот адрес?')){
           $('form#delete_address').trigger('submit');
         }
            return false;
        })
    </script>
@endsection