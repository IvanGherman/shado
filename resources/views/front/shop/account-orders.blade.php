@extends('layouts.front')
@section('content')

    <div class="page-block start-block product-page-start">
        <div class="container animateMe">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="start-block__text">
                        <div class="start-block__text-title">{{ $title }}</div>
                        <div class="start-block__text-content">Добро пожаловать, <span class="cabinet-user-name">{{ \Illuminate\Support\Facades\Auth::user()->name }}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-block users-cabinet-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 animateMe bio-col">
                    <div class="users-cabinet-checkout clear_end">
                        @include('front.shop.account-menu')
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 bio-col">
                    <div class="checkout profile-checkout animateMe">
                        <div class="block-title-zone">
                            <div class="checkout__title">{{ $subtitle }}</div>
                        </div>
                        <div class="zone__content clear_end">
                        @if($orders->count())
                        <table class="table">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Дата</td>
                                <td>Стоимость</td>
                                <td>Способ оплаты</td>
                                <td>Товары</td>
                                <td>Адрес</td>
                                <td>Статус</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ \Carbon\Carbon::parse($order->created_at)->format('Y-m-d') }}</td>
                                <td>{{ $order->total.' '.$order->currency }}</td>
                                <td>{{ \App\Http\HelpersShop\webus_shop::get_payment_methods()[$order->method_payment] }}</td>
                                <td>
                                    @if($order->products->count())
                                    <ul>
                                       @foreach($order->products as $product)
                                           <li>{{ $product->name }}</li>
                                       @endforeach
                                    </ul>
                                    @endif
                                </td>
                                <td>
                                    <ul>
                                      <li>{{ $order->address_name }}</li>
                                      <li>{{ $order->address_country }}</li>
                                      <li>{{ $order->address_city }}</li>
                                      <li>{{ $order->address_street }}</li>
                                      <li>{{ $order->address_index }}</li>
                                    </ul>
                                </td>
                                <td>{{ \App\Http\HelpersShop\webus_shop::order_status($order->order_status) }}</td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                            {{ $orders->links('front.includes.paginate') }}
                         @else
                            <h2>Нет заказов</h2>
                         @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection