<aside>
    @if(!$coupon)
        <form id="apply_coupons" method="post">
            <div class="sidebar-wrap mb-30">
                <h3 class="sidebar-title text-center">Have a promo code?</h3>
                <div class="cart-form-wrap">
                    <input type="text" name="code" placeholder="Enter your coupon code here...">
                    <button type="submit">Apply Coupon Code</button>
                </div>
            </div>
        </form>
    @else
        <form id="remove_coupons" method="post" action="{{ route('remove_coupons') }}">
            {{ csrf_field() }}
            <div class="sidebar-wrap mb-30">
                <h3 class="sidebar-title text-center">Remove coupon</h3>
                <div class="cart-form-wrap">
                    <button type="submit">Remove</button>
                </div>
            </div>
        </form>
    @endif
</aside>