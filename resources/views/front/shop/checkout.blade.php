@extends('layouts.front')
@section('content')
    <div class="page-block start-block product-page-start">
        <div class="container animateMe">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="start-block__text">
                        <div class="start-block__text-title">{{ $title }}</div>
                        <div class="start-block__text-content">{{ trans('cart.text_count') }} <span class="card-items-volume">{{ Cart::count() }}</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-block order-block">
        @include('front.includes.preloader')
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- 1 -->
                    <div id="methot_checkout">
                        @include('front.includes.checkout.method_checkout')
                    </div>
                    <!-- 2 -->
                    <div id="profile_information">
                        @include('front.includes.checkout.profile_information')
                    </div>
                    <!-- 3 -->
                   <div id="adresses">
                       @include('front.includes.checkout.adresses')
                   </div>
                    <div class="space"></div>
                    <!-- 4 -->
                    <div id="method_payment">
                        @include('front.includes.checkout.method_payment')
                    </div>
                    <div class="space"></div>
                    <!-- 5 -->
                    <div id="confirm">
                        <div class="checkout animateMe">
                            <div class="block-title-zone">
                                <div class="checkout-number">5</div>
                                <div class="checkout__title">Подтверждение заказа</div>
                            </div>
                           <div class="confirm-place">
                               @include('front.includes.checkout.confirm')
                           </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @stack('function_profile_information')
    @stack('function_address')
    @stack('function_confirm')
@endsection