@extends('layouts.front')

@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
@section('content')
    <section class="contacts-top" style="background-image: url({{asset('assets/img/customer-img/contacts-bg.jpg')}})">
        <div class="container">
            <div class="row hidden-md">
                <div class="col-4">
                    <div class="contacts-card">
                        <div class="contacts-card__decor">
                            <svg viewBox="0 0 384 107" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="384" height="12" fill="currentColor"/>
                                <rect x="366" y="12" width="2" height="78" fill="currentColor"/>
                                <rect x="365" y="85" width="4" height="22" fill="currentColor"/>
                            </svg>
                        </div>
                        <div class="contacts-card__icon">
                            <svg class="icon" width="48" height="48">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-mail') }}"></use>
                            </svg>
                        </div>
                        <div class="contacts-card__content">
                            <a class="contacts__link" href="mailto:{{ $variables->contact_page_contact_email }}">{{ $variables->contact_page_contact_email }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="contacts-card">
                        <div class="contacts-card__decor">
                            <svg viewBox="0 0 384 107" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="384" height="12" fill="currentColor"/>
                                <rect x="366" y="12" width="2" height="78" fill="currentColor"/>
                                <rect x="365" y="85" width="4" height="22" fill="currentColor"/>
                            </svg>

                        </div>
                        <div class="contacts-card__icon">
                            <svg class="icon" width="48" height="48">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-phone-chat') }}"></use>
                            </svg>
                        </div>
                        <div class="contacts-card__content">
                            <a class="contacts__link" href="tel:{{ \App\Variables::changePhone($variables, 1) }}">{{ \App\Variables::changePhone($variables, 1) }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="contacts-card">
                        <div class="contacts-card__decor">
                            <svg viewBox="0 0 384 107" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="384" height="12" fill="currentColor"/>
                                <rect x="366" y="12" width="2" height="78" fill="currentColor"/>
                                <rect x="365" y="85" width="4" height="22" fill="currentColor"/>
                            </svg>

                        </div>
                        <div class="contacts-card__icon">
                            <svg class="icon" width="48" height="48">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-social') }}"></use>
                            </svg>
                        </div>
                        <div class="contacts-card__content">
                            <div class="socials socials--compensation">
                                <a class="btn btn-icon-only btn--link socials__btn"
                                   href="{{ $variables->facebook }}">
                                    <span></span>
                                    <svg class="icon" width="24" height="24">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-fb') }}"></use>
                                    </svg>
                                </a>
                                <a class="btn btn-icon-only btn--link socials__btn"
                                   href="{{ $variables->instagram }}">
                                    <span></span>
                                    <svg class="icon" width="24" height="24">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-insta') }}"></use>
                                    </svg>
                                </a>
                                <a class="btn btn-icon-only btn--link socials__btn"
                                   href="{{ $variables->whatsapp }}"><span></span>
                                    <svg class="icon" width="24" height="24">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-what') }}"></use>
                                    </svg>
                                </a>
                                <a class="btn btn-icon-only btn--link socials__btn"
                                   href="{{ $variables->viber }}"><span></span>
                                    <svg class="icon" width="24" height="24">
                                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-viber') }}"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($contacts->count() > 0)
    <section class="addresses">
        <div class="container">
            <h1 class="section-title--alt address__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->departments_addresses) !!}</h1>
            <div class="row row--center row--gap-2">
                @foreach($contacts as $contact)
                    <div class="col-4 col-lg-6 col-sm-12">
                    <div class="address">
                        <div class="address__section">
                            <div class="address__label">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $contact->title) !!}</div>
                            <div class="address__location">
                                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $contact->address) !!}</span>
                            </div>
                        </div>
                        <div class="address__section">
                            <div class="address__label">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->phone) !!}</div>
                            <div class="address__location">
                                <a href="tel:{{ $contact->phone }}">{{ $contact->phone }}</a>
                            </div>
                        </div>
                        @if ($contact->schedule)
                            @php
                                $periods = $contact->getSchedulePeriods();
                            @endphp
                        <div class="address__section">
                            <div class="address__label">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->schedule) !!}</div>
                            <div class="address__time">
                                @foreach($periods as $period)
                                    @php
                                        $cls = '';
                                        if ($period['counter'] === 1) {
                                            $cls = 'single';
                                        } elseif ($period['counter'] === 2) {
                                            $cls = 'double';
                                        }
                                    @endphp
                                    @if ($period['fromTo']['from'] && $period['fromTo']['to'])
                                    <div class="address__time-main">
                                        <div class="address__time-particles {{ $cls }}">
                                            @for($i=1;$i<=$period['counter'];$i++)
                                                @if ($period['counter'] === $i || $i === 1)
                                                    <span class="bg-primary deco"></span>
                                                @else
                                                    <span class="bg-primary"></span>
                                                @endif
                                            @endfor
                                        </div>
                                        <div class="address__time-label">{{ $period['fromTo']['from'] }} - {{ $period['fromTo']['to'] }}</div>
                                    </div>
                                    @else
                                        <div class="address__time-w">
                                            <div class="address__time-particles {{ $cls }}">
                                                @for($i=0;$i<$period['counter'];$i++)
                                                    <span class="deco bg-grey-1"></span>
                                                @endfor
                                            </div>
                                            <div class="address__time-label">@if ($cls === 'single') {!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->free_day) !!} @else {!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->free_days) !!} @endif</div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif
    <section class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col col-6 col-sm-12">
                    <div class="contact-us__form">
                        <form class="contact_form form" novalidate="novalidate" data-scrolling-container=".modal__content--scrollable">
                            <div class="form__title ff-secondary">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->contact_us) !!}</div>
                            <div class="form__group">
                                <label class="form__wrapper">
                                    <input type="text" required="" placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->name) !!}" name="person_name"/>
                                </label>
                                <span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                            </div>
                            <div class="form__group">
                                <label class="form__wrapper">
                                    <input type="email" required="" placeholder="Email" name="email"/>
                                </label><span class="error__text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->not_valid) !!}</span>
                            </div>
                            <div class="form__group">
                                <label class="form__wrapper">
                                    <textarea placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->additional_info) !!}" data-min="2" name="message"></textarea>
                                </label>
                            </div>
                            <div class="align--center">
                                <button class="btn btn--primary btn--custom form__btn">
                                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->send) !!}</span>
                                </button>
                            </div>
                            <div class="form__info">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->confirm_with) !!} <a href="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $variables->privacy_policy_link) !!}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->privacy_policy) !!} </a>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->company) !!}</div>
                            <div class="form__success">
                                <img class="form__success-img" src="{{ asset('assets/img/icon-success.svg') }}"/>
                                <span class="form__success-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->form_sent) !!}</span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col col-6 col-sm-12">
                    <div class="contact-us__map" id="map_">

                    </div>
                </div>
                <style type="text/css">
                    /* Set a size for our map container, the Google Map will take up 100% of this container */
                    #map_ {
                        width: 100%;
                        height: 471px;
                        margin-left: 0px;
                    }
                </style>
                <script>
                    var mapStyles = [
                        {
                            "featureType": "all",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "weight": "2.00"
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#9c9c9c"
                                }
                            ]
                        },
                        {
                            "featureType": "all",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape.man_made",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 45
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#eeeeee"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#7b7b7b"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#46bcec"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#c8d7d4"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#070707"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        }
                    ];
                    var map;
                    function initMap() {
                        map = new google.maps.Map(document.getElementById('map_'), {
                            center: {lat: {{ $variables->map_default_lat }} , lng: {{ $variables->map_default_ltg }} },
                            zoom: {{ $variables->map_zoom }},
                            styles: mapStyles
                        });

                        @foreach($contacts as $contact)
                            @if ($contact->lat && $contact->lng)
                                var myLatlng = new google.maps.LatLng({{ $contact->lat }},{{ $contact->lng }});

                                var marker = new google.maps.Marker({
                                    position: myLatlng,
                                    title:"{!! \App\Http\Helpers\webus_help::translate_manual($locale, $contact->title) !!}"
                                });

                                marker.setMap(map);
                            @endif
                        @endforeach
                    }
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZIaHR-kf8UCHZWsqJh3SGSS7t3vNzLWo&callback=initMap"
                        async defer></script>
            </div>
        </div>
    </section>
    {!! \App\Http\Helpers\webus_help::get_modules($modules) !!}
@endsection

