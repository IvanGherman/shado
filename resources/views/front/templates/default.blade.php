@extends('layouts.front')

@section('content')
    @php
        $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
        $variables = json_decode($variables);

        $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
        $translations = json_decode($translations);

        $locale = Config::get('app.locale');
    @endphp
    @if ($data !== null)
        {!! \App\Http\Shortcodes\shortcodes::do_shortcode(\App\Http\Helpers\webus_help::translate_manual($locale, $data->body)) !!}
    @endif
    {!! \App\Http\Helpers\webus_help::get_modules($modules) !!}
@endsection