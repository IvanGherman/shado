@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
@foreach ($products as $product)
    @php
        $images = json_decode($product->images);
        $newImages = [];
        if (!empty($images)) {
            foreach ($images as $image) {
                $newImages[] = \App\Product::getAssetOrDefault($image->image);
            }
        }
    @endphp
    <li class="products-main__item col-3 col-xl-4 col-lg-6" data-id="{{ $product->getId() }}" id="product-{{ $product->getId() }}">
        <div class="preview-card dflex f-d-col preview-card--product hidden-xl">
            <div class="preview-card__img-slider-container bg-white br-8 dflex f-d-col">
                <div class="preview-card__img-thumb-slider leafer js-leafer-thumbs">
                    <div class="leafer__wrap leafer--vertical">
                        @foreach ($newImages as $newImage)
                            <div class="leafer__slide thumb"
                                 data-select="[&quot;{{ $newImage }}&quot;]">
                                <img class="preview-card__img" src="{{ $newImage }}"/></div>
                            <div class="leafer__slide thumb"></div>
                        @endforeach
                    </div>
                </div>
                <div class="leafer__btns">
                    <button class="btn btn-icon-only btn--nopadding btn--link preview-card__img-slider-btn-prev leafer__btn leafer__btn--prev">
                        <svg class="icon stroke-primary" width="20" height="20">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                    <button class="btn btn-icon-only btn--nopadding btn--link preview-card__img-slider-btn-next leafer__btn leafer__btn--next">
                        <svg class="icon stroke-primary" width="20" height="20">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="preview-card__img-container">
                <a class="preview-card__slider-container" href="{{ route('product', $product->getId()) }}">
                    <img class="preview-card__img img--main" src="{{ asset($product->getFirstImage()) }}"/>
                    <img class="on-top img--first invisible" alt=""/>
                    <img class="on-top img--second invisible" alt=""/>
                </a>
                <button class="btn btn--reverse btn--with-icon btn--white preview-card__modal-btn hidden-xl"
                        data-modal="#product-preview-modal"
                        data-slide-change="G{{ $product->getId() }}"
                >
                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->preview) !!}</span>
                    <svg class="icon icon--mright stroke-primary" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-eye') }}"></use>
                    </svg>
                </button>
                @if ($product->promotion)
                    <div class="preview-card__product-label">
                        <div class="preview-card__product-label-container br-24 bg-primary">
                            <svg class="icon" width="12" height="12">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-percent') }}"></use>
                            </svg>
                            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->discount) !!}</span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="preview-card__text-container">
                <div class="preview-card__img-secondary">
                    <img class="icon" src="{{ asset($product->getModelImage()) }}" alt="" width="32" height="32"/>
                </div>
                <a class="product-item__title align--left"
                   href="{{ url('product', $product->getId()) }}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->title) !!}</a>

                <p class="product-item__text align--left">{{ $product->getImploded('categories', $locale) }}</p>

                @include('front.partial.price', ['item' => $product])
            </div>
        </div>
        <div class="preview-card dflex f-d-col preview-card--product hidden-xl-up">
            <div class="preview-card__img-container">
                <a class="preview-card__slider-container" href="{{ url('product', $product->getId()) }}">
                    <img class="preview-card__img" src="{{ asset($product->getFirstImage()) }}"/>
                </a>
                @if ($product->promotion)
                    <div class="preview-card__product-label">
                        <div class="preview-card__product-label-container br-24 bg-primary">
                            <svg class="icon" width="12" height="12">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-percent') }}"></use>
                            </svg>
                            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->discount) !!}</span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="preview-card__text-container">
                <div class="preview-card__img-secondary">
                    <img class="icon" src="{{ asset($product->getModelImage()) }}" alt="" width="32" height="32"/>
                </div>
                <a class="product-item__title align--left" href="{{ url('product', $product->getId()) }}">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->title) !!}</a>

                <p class="product-item__text align--left">{{ $product->getImploded('categories', $locale) }}</p>
                @include('front.partial.price', ['item' => $product])
            </div>
        </div>
    </li>
@endforeach