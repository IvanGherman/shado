<ul class="product-info__details-list details-list">
        @if (!empty($product->production_duration))
            <li class="details-list__item">
                <div class="details-list__item-left">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->product_duration) !!}:</div>
                <div class="details-list__item-right">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $product->production_duration) !!}</div>
            </li>
        @endif
        @if ($product->textures->count() > 0)
            <li class="details-list__item">
                <div class="details-list__item-left">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->type) !!}:</div>
                <div class="details-list__item-right">{{ $product->getTextures($locale) }}</div>
            </li>
        @endif
        @php
            $transparency = !empty($product->transparency) ? \App\Http\Helpers\webus_help::translate_manual($locale, $product->transparency->title) : '';
        @endphp
        @if (!empty($transparency))
            <li class="details-list__item">
                <div class="details-list__item-left">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->transparency) !!}:</div>
                <div class="details-list__item-right">{!! $transparency !!}</div>
            </li>
        @endif
        @if ($product->countries->count() > 0)
            <li class="details-list__item">
                <div class="details-list__item-left">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->country_producer) !!}:</div>

                <div class="details-list__item-right">{{ $product->getCountries($locale) }}</div>
            </li>
        @endif
        @foreach ($product->getValues() as $value)
            <li class="details-list__item">
                <div class="details-list__item-left">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $value->attribute->title) !!}:</div>
                <div class="details-list__item-right">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $value->attributeValue->title) !!}</div>
            </li>
        @endforeach
    </ul>