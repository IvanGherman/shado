@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<div class="filters__fixed-wrap">
    <div class="filters js-filters product__filters">
        <form class="filters__form no-reset js-filter-container" novalidate="novalidate">
            <div class="filters__close js-filters__btn-close hidden-xl-up">
                <svg class="icon" width="40" height="40">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-filters') }}"></use>
                </svg><span class="filters__close-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->filters) !!}</span>
                <button class="filters__btn-close js-filters__btn-close" type="button">
                    <svg class="icon stroke-black" width="20" height="20">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                    </svg>
                </button>
            </div>
            <div class="accordion">
                <div class="filters__searchfield">
                    <div class="form__group">
                        <label class="form__wrapper form__wrapper--has-icon">
                            <input type="text"
                                   placeholder="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->search_by_code) !!}"
                                   name="keyword"
                            />
                            <span class="form__input-icon">
                              <svg class="icon stroke-current" width="24" height="24">
                                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-search') }}"></use>
                              </svg>
                            </span>
                        </label>
                    </div>
                </div>
                <div class="filters__reset-btn-container">
                    <button class="btn btn--primary btn--sm btn--custom filters__reset-btn hidden-xl js-form-reset-btn js-filters-reset-btn" type="button">
                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->reset_filters) !!}</span>
                    </button>
                </div>
                @if (!empty($categories))
                    <div class="accordion__item active show">
                        <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->categories) !!}
                            <div class="accordion__icon"></div>
                        </div>
                        <div class="accordion__content">
                            <div class="some-content">
                                <!-- for backend .js-filters-list - add this to ul.filters__list if list height is more than 245 px-->
                                <ul class="filters__list dflex f-d-col js-filters-list">
                                    @foreach ($categories as $category)
                                        <li class="filters__item">
                                            <div class="form__group no-margin">
                                                <label class="form__wrapper">
                                                    <input class="form__checkbox" type="checkbox" name="categories[{{ $category->id }}]" id="categories{{ $category->id }}"/>
                                                    <span class="icon">
                                                      <svg width="22" height="22" viewbox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M27 5.7V24.5C27 26.7091 25.2091 28.5 23 28.5H6C3.79086 28.5 2 26.7091 2 24.5V7.5C2 5.29086 3.79086 3.5 6 3.5H24.8C26.015 3.5 27 4.48497 27 5.7V5.7Z" stroke-linecap="round" stroke-linejoin="round"></path>
                                                        <path d="M7.7998 13.9L14.5998 20.7L29.9998 5.3" stroke-linecap="round" stroke-linejoin="round"></path>
                                                      </svg>
                                                    </span>
                                                    <div class="filters__item-text">
                                                        <span class="filters__item-text-main">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $category->title) !!}</span>
                                                        @php $found = false; @endphp
                                                        @foreach($counters['categories'] as $item)
                                                            @if ($item->id == $category->id)
                                                                <span class="filterable" id="categories-amount-{{ $category->id }}">{{ $item->amount }}</span>
                                                                @php $found = true; @endphp
                                                            @endif
                                                        @endforeach
                                                        @if (!$found)
                                                            <span class="filterable" id="categories-amount-{{ $category->id }}"></span>
                                                        @endif
                                                    </div>
                                                </label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="filters__single-item">
                    <div class="form__group no-margin">
                        <label class="form__wrapper">
                            <input class="form__checkbox" type="checkbox" name="promotion"/><span class="icon">
                              <svg width="22" height="22" viewbox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M27 5.7V24.5C27 26.7091 25.2091 28.5 23 28.5H6C3.79086 28.5 2 26.7091 2 24.5V7.5C2 5.29086 3.79086 3.5 6 3.5H24.8C26.015 3.5 27 4.48497 27 5.7V5.7Z" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path d="M7.7998 13.9L14.5998 20.7L29.9998 5.3" stroke-linecap="round" stroke-linejoin="round"></path>
                              </svg>
                            </span>
                            <span class="filters__single-item-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->promotion) !!}</span>
                        </label>
                    </div>
                </div>
                @if (!empty($colors))
                    <div class="accordion__item active show">
                        <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->color) !!}
                            <div class="accordion__icon"></div>
                        </div>
                        <div class="accordion__content">
                            <div class="some-content">
                                <ul class="filters__list filters__list--circle dflex fwrap js-filters-list">
                                    @foreach ($colors as $color)
                                        <li class="filters__item">
                                            <div class="form__group checkbox--circle js-checkbox-color">
                                                <label class="form__wrapper jcc br-50p shadow-main" style="background-color: {{ $color->color }}">
                                                    <input class="form__checkbox" type="checkbox" name="colors[{{ $color->id }}]" id="colors{{ $color->id }}"/>
                                                    <span class="icon filters__color-icon">
                                                      <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 32 32">
                                                        <path fill="none" d="M7.8 13.9l6.8 6.8L30 5.3"></path>
                                                      </svg>
                                                    </span>
                                                    @php $found = false; @endphp
                                                    @foreach($counters['colors'] as $item)
                                                        @if ($item->id == $color->id)
                                                            @php $found = true; @endphp
                                                            <span class="filters__color-counter filterable" id="colors-amount-{{ $color->id }}">{{ $item->amount }}</span>
                                                        @endif
                                                    @endforeach
                                                    @if (!$found)
                                                        <span class="filters__color-counter filterable" id="colors-amount-{{ $color->id }}">0</span>
                                                    @endif
                                                </label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                @if (!empty($textures))
                    <div class="accordion__item active show">
                        <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->texture_type) !!}
                            <div class="accordion__icon"></div>
                        </div>
                        <div class="accordion__content">
                            <div class="some-content no-padding">
                                <ul class="filters__list filters__list--boxes filters__list--by-three dflex fwrap overflow js-filters-list">
                                    @foreach ($textures as $texture)
                                        <li class="filters__item">
                                            <div class="form__group checkbox--box-img f-d-col aic">
                                                <label class="form__wrapper">
                                                    <input class="form__checkbox visually-hidden" type="checkbox" name="textures[{{ $texture->id }}]" id="textures{{ $texture->id }}"/>
                                                    <div class="checkbox--box-img-wrapper" style="background-image: url({{ asset($texture->icon) }})"></div>
                                                    @php $found = false; @endphp
                                                    @foreach($counters['textures'] as $item)
                                                        @if ($item->id == $texture->id)
                                                            @php $found = true; @endphp
                                                            <div class="checkbox--box-img-text filterable" id="textures-amount-{{ $texture->id }}">{{ $item->amount }}</div>
                                                        @endif
                                                    @endforeach
                                                    @if (!$found)
                                                        <div class="checkbox--box-img-text filterable" id="textures-amount-{{ $texture->id }}">0</div>
                                                    @endif
                                                </label>
                                                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $texture->title) !!}</span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="filters__title title--md">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->attributes) !!}</div>
                @if (!empty($transparencies))
                    <div class="accordion__item @if (!empty($filters_predefined['transparencies'])) active show @endif">
                        <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->transparencies) !!}
                            <div class="accordion__icon"></div>
                        </div>
                        <div class="accordion__content">
                            <div class="some-content">
                                <!-- for backend  - add this to ul.filters__list if list height is more than 245 px-->
                                <ul class="filters__list dflex f-d-col js-filters-list">
                                    @foreach ($transparencies as $transparency)
                                        <li class="filters__item">
                                            <span class="form__group no-margin">
                                                <label class="form__wrapper">
                                                    <input class="form__checkbox" type="checkbox" name="transparencies[{{ $transparency->id }}]" id="transparencies{{ $transparency->id }}"/>
                                                    <span class="icon">
                                                      <svg width="22" height="22" viewbox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M27 5.7V24.5C27 26.7091 25.2091 28.5 23 28.5H6C3.79086 28.5 2 26.7091 2 24.5V7.5C2 5.29086 3.79086 3.5 6 3.5H24.8C26.015 3.5 27 4.48497 27 5.7V5.7Z" stroke-linecap="round" stroke-linejoin="round"></path>
                                                        <path d="M7.7998 13.9L14.5998 20.7L29.9998 5.3" stroke-linecap="round" stroke-linejoin="round"></path>
                                                      </svg>
                                                    </span>
                                                    <span class="filters__item-text">
                                                        <span class="filters__item-text-main">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $transparency->title) !!}</span>

                                                        @php $found = false; @endphp
                                                        @foreach($counters['transparencies'] as $item)
                                                            @if ($item->id == $transparency->id)
                                                                @php $found = true; @endphp
                                                                <span class="filterable" id="transparencies-amount-{{ $transparency->id }}">{{ $item->amount }}</span>
                                                            @endif
                                                        @endforeach
                                                        @if (!$found)
                                                            <span class="filterable" id="transparencies-amount-{{ $transparency->id }}">0</span>
                                                        @endif
                                                    </span>
                                                </label>
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                @if (!empty($countries))
                    <div class="accordion__item @if (!empty($filters_predefined['countries'])) active show @endif">
                        <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->countries_producer) !!}
                            <div class="accordion__icon"></div>
                        </div>
                        <div class="accordion__content">
                            <div class="some-content">
                                <!-- for backend  - add this to ul.filters__list if list height is more than 245 px-->
                                <ul class="filters__list dflex f-d-col js-filters-list">
                                    @foreach ($countries as $country)
                                        <li class="filters__item">
                                            <div class="form__group no-margin">
                                                <label class="form__wrapper">
                                                    <input class="form__checkbox" type="checkbox" name="countries[{{ $country->id }}]" id="countries{{ $country->id }}"/>
                                                    <span class="icon">
                                                          <svg width="22" height="22" viewbox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M27 5.7V24.5C27 26.7091 25.2091 28.5 23 28.5H6C3.79086 28.5 2 26.7091 2 24.5V7.5C2 5.29086 3.79086 3.5 6 3.5H24.8C26.015 3.5 27 4.48497 27 5.7V5.7Z" stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path d="M7.7998 13.9L14.5998 20.7L29.9998 5.3" stroke-linecap="round" stroke-linejoin="round"></path>
                                                          </svg></span>
                                                    <div class="filters__item-text">
                                                        <span class="filters__item-text-main">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $country->title) !!}</span>

                                                        @php $amount = 0; @endphp
                                                        @foreach($counters['countries'] as $item)
                                                            @if ($item->id == $country->id)
                                                                @php $amount = $item->amount; @endphp
                                                            @endif
                                                        @endforeach
                                                        <span class="filterable" id="countries-amount-{{ $country->id }}">{{ $amount }}</span>
                                                    </div>
                                                </label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                @if (!empty($tree_types))
                    <div class="accordion__item @if (!empty($filters_predefined['tree_types'])) active show @endif">
                        <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->tree_type) !!}
                            <div class="accordion__icon"></div>
                        </div>
                        <div class="accordion__content">
                            <div class="some-content">
                                <!-- for backend  - add this to ul.filters__list if list height is more than 245 px-->
                                <ul class="filters__list dflex f-d-col js-filters-list">
                                    @foreach ($tree_types as $tree)
                                        <li class="filters__item">
                                            <div class="form__group no-margin">
                                                <label class="form__wrapper">
                                                    <input class="form__checkbox" type="checkbox" name="tree_types[{{ $tree->id }}]"/>
                                                    <span class="icon">
                                                      <svg width="22" height="22" viewbox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M27 5.7V24.5C27 26.7091 25.2091 28.5 23 28.5H6C3.79086 28.5 2 26.7091 2 24.5V7.5C2 5.29086 3.79086 3.5 6 3.5H24.8C26.015 3.5 27 4.48497 27 5.7V5.7Z" stroke-linecap="round" stroke-linejoin="round"></path>
                                                        <path d="M7.7998 13.9L14.5998 20.7L29.9998 5.3" stroke-linecap="round" stroke-linejoin="round"></path>
                                                      </svg>
                                                    </span>
                                                    <div class="filters__item-text">
                                                        <span class="filters__item-text-main">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $tree->title) !!}</span>

                                                        @php $amount = 0; @endphp
                                                        @foreach($counters['treeType'] as $item)
                                                            @if ($item->id == $tree->id)
                                                                @php $amount = $item->amount; @endphp
                                                            @endif
                                                        @endforeach
                                                        <span class="filterable" id="tree-types-amount-{{ $tree->id }}">{{ $amount }}</span>
                                                    </div>
                                                </label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($attribute_filters->count() > 0)
                    @foreach($attribute_filters as $attribute_filter)
                        <div class="accordion__item @if (!empty($filters_predefined['attribute'][$attribute_filter->id])) active show @endif">
                            <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $attribute_filter->title) !!}
                                <div class="accordion__icon"></div>
                            </div>
                            <div class="accordion__content">
                                <div class="some-content">
                                    <!-- for backend  - add this to ul.filters__list if list height is more than 245 px-->
                                    <ul class="filters__list dflex f-d-col js-filters-list">
                                        @foreach ($attribute_filter->values as $value)
                                            <li class="filters__item">
                                                <div class="form__group no-margin">
                                                    <label class="form__wrapper">
                                                        <input class="form__checkbox" type="checkbox" name="attribute[{{ $attribute_filter->id }}][{{ $value->id }}]" id="attribute-{{$attribute_filter->id}}-{{ $value->id }}"/><span class="icon">
                                                              <svg width="22" height="22" viewbox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M27 5.7V24.5C27 26.7091 25.2091 28.5 23 28.5H6C3.79086 28.5 2 26.7091 2 24.5V7.5C2 5.29086 3.79086 3.5 6 3.5H24.8C26.015 3.5 27 4.48497 27 5.7V5.7Z" stroke-linecap="round" stroke-linejoin="round"></path>
                                                                <path d="M7.7998 13.9L14.5998 20.7L29.9998 5.3" stroke-linecap="round" stroke-linejoin="round"></path>
                                                              </svg>
                                                                </span>
                                                        <div class="filters__item-text">
                                                            <span class="filters__item-text-main">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $value->title) !!}</span>

                                                            @php $amount = 0; @endphp
                                                            @foreach($counters['attributes'] as $item)
                                                                @if ($value->id == $item->attribute_value_id)
                                                                    @php $amount = $item->amount; @endphp
                                                                @endif
                                                            @endforeach
                                                            <span class="filterable" id="attribute-value-{{ $value->id }}-amount">{{ $amount }}</span>
                                                        </div>
                                                    </label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="accordion__item active show some-content-tiny">
                    <div class="accordion__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->price) !!}
                        <div class="accordion__icon"></div>
                    </div>
                    <div class="accordion__content">
                        <div class="some-content">
                            <div class="range" id="price" data-type="double" data-step="1" data-min="0" data-max="3000" data-start="0" data-end="7900">
                                <div class="form__group no-margin">
                                    <div class="form__wrapper">
                                        <span class="form__range">
                                            <span class="range__vals">
                                              <div class="range__min">
                                                <input class="range__min-val" type="text"/>
                                              </div>
                                              <div class="range__max">
                                                <input class="range__max-val" type="text"/>
                                              </div>
                                            </span>
                                            <span class="range__slider"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="filters__reset-btn-container">
            <button class="btn btn--primary btn--custom filters__reset-btn hidden-xl-up js-filters__btn-close" type="button" id="get-products">
                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->apply) !!}</span>
            </button>
            <button class="btn btn--link filters__reset-btn hidden-xl-up js-form-reset-btn js-filters-reset-btn js-noUi-reset-btn" type="button">
                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->reset) !!}</span>
            </button>
            <button class="btn btn--primary btn--sm btn--custom filters__reset-btn hidden-xl js-form-reset-btn js-filters-reset-btn" type="button">
                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->reset_filters) !!}</span>
            </button>
        </div>
        <div class="filters__thumb"></div>
    </div>
    <div class="filters__bg js-filter-bg"></div>
</div>