@extends('layouts.front')
@section('content')
    @php
        $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
        $variables = json_decode($variables);

        $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
        $translations = json_decode($translations);

        $locale = Config::get('app.locale');
    @endphp

    <section class="faq">
        <div class="container container--medium">
            <h2 class="title--xxl ff-secondary section-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->questions_answers) !!}</h2>
            <div class="accordion">
                @foreach($data as $item)
                <div class="accordion__item">
                    <div class="accordion__title">
                        <div class="faq__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->question) !!}</div>
                        <div class="accordion__icon"></div>
                    </div>
                    <div class="accordion__content">
                        <div class="some-content">
                            <div class="accordion__text">
                                <div class="static">
                                    {!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->answer) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    {!! \App\Http\Helpers\webus_help::get_modules($modules) !!}
@endsection