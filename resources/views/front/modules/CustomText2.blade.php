@if($module != null)
    @php
        $locale = Config::get('app.locale');
    @endphp

    <div class="static">
        <div class="container container--short">
            {!! \App\Http\Shortcodes\shortcodes::do_shortcode(\App\Http\Helpers\webus_help::translate_manual($locale, $module->body)) !!}
        </div>
    </div>
@endif