@php
    $locale = Config::get('app.locale');
@endphp
@if ($comforts->count() > 0)
    <section class="benefits hidden-lg">
    <div class="container">
        <div class="row">
            <div class="col-4 col-xl-12 dflex f-d-col jcc ais ai-xl-c">
                <h2 class="title--xlg benefits__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $module->title) !!}</h2>
                @if (!empty($module->button_text) && !empty($module->button_url))
                    <a class="btn btn--primary btn--custom benefits__link hidden-xl"
                       href="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $module->button_url) !!}">
                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $module->button_text) !!}</span>
                    </a>
                @endif
            </div>
            <div class="col-8 col-xl-12">
                <div class="benefits__items">
                    <div class="row jc-sm-c">
                        @foreach($comforts as $comfort)
                            <div class="col-6 col-sm-12">
                                <div class="benefits__item">
                                    <img class="icon benefits__item-icon" src="{{ asset($comfort->icon) }}" alt="" width="100" height="90"/>
                                    <div class="benefits__item-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $comfort->title) !!}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="m-0-a">
                <a class="btn btn--primary btn--custom benefits__link hidden-xl-up" href="{!! \App\Http\Helpers\webus_help::translate_manual($locale, $module->button_url) !!}">
                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $module->button_text) !!}</span>
                </a>
            </div>
        </div>
    </div>
</section>
@endif