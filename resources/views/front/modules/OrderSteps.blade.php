@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
@if ($steps->count() > 0)
<section class="info align--center hidden-lg">
    <div class="container">
        <div class="info__content">
            <h2 class="title--xxl ff-secondary section-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $module->title) !!}</h2>
            <div class="info__items">
                <div class="row">
                    @foreach($steps as $key => $step)
                    @php $cls = ($key + 1 !== $steps->count()) ? 'info__item--deco' : ''; @endphp
                    <div class="col-3 col-md-12">
                        <div class="info__item dflex f-d-col f-centered sr-up-multiple {{$cls}}">
                            <img class="icon info__item-icon" src="{{ asset($step->image) }}" alt="" width="54" height="54"/>
                            <h3 class="title--sm ff-secondary info__item-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $step->title) !!}</h3>
                            <div class="info__item-text">
                                {!! \App\Http\Helpers\webus_help::translate_manual($locale, $step->short_description) !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif