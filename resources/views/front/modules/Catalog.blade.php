@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<section class="products">
    <!-- filters-->
    <div class="products__filters">
        <div class="products-filters">
            <div class="products-filters__container">
                <div class="swiper-container js-filters-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="products-filters__item">
                                <label class="products-filters__wrapper">
                                    <input class="visually-hidden" type="radio" checked="checked" name="product-filter" value="all"/>
                                    <div class="products-filters__icon">
                                        <img src="{{ asset('assets/img/customer-img/blinds-all.svg') }}" alt=""/>
                                    </div>
                                    <div class="products-filters__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->all) !!}</div>
                                </label>
                            </div>
                        </div>
                        @foreach($categories as $category)
                        <div class="swiper-slide">
                            <div class="products-filters__item">
                                <label class="products-filters__wrapper">
                                    <input class="visually-hidden" type="radio" name="product-filter" value="{{ $category->id }}"/>
                                    <div class="products-filters__icon">
                                        {{--<svg class="icon fill-current" width="40" height="40">--}}
                                            {{--<use xlink:href="{{ \App\Product::getAssetOrDefault($category->image) }}"></use>--}}
                                        {{--</svg>--}}
                                        <img src="{{ \App\Product::getAssetOrDefault($category->image) }}">
                                    </div>
                                    <div class="products-filters__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $category->title) !!}</div>
                                </label>
                            </div>
                        </div>
                        @endforeach
                        @foreach($customMenu as $key => $item)
                            <div class="swiper-slide @if ($key == 0)separator @endif">
                                <div class="products-filters__item">
                                    <a class="products-filters__wrapper" href="{{ url($item->url) }}">
                                        <div class="products-filters__icon color-primary">
                                            <img src="{{ asset($item->image) }}">
                                        </div>
                                        <div class="products-filters__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->title) !!}</div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
            <div class="products-filters__btns">
                <button class="btn btn-icon-only btn--link btn--prev products-filters__btn-prev"><span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
                <button class="btn btn-icon-only btn--link btn--next products-filters__btn-next"><span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>
    <div class="products__items">
        <div class="container container--large">
            <ul class="products-main__list row" id="catalog-container">
                <!-- for backend - hidden-xl - show only on desktop - higher than 1200, hidden-xl-up show only on mobile - lower than 1200-->
                @include('front.pages.products_partial')
            </ul>
        </div>
        <div class="align--center">
            <a class="btn btn--primary btn--custom products__link" href="{{ route('product_list') }}">
                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->go_catalog) !!}</span>
            </a>
        </div>
    </div>
    <div class="modal product-preview-modal hidden-xl" id="product-preview-modal">
        <div class="modal__wrapper">
            <div class="modal__close">
                <svg class="icon stroke-black" width="20" height="20">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                </svg>
            </div>
            <div class="modal__content">
                <div class="product-preview">
                    <div class="swiper-container product-preview__slider js-product-preview-slider">
                        <div class="swiper-wrapper" id="jproduct-modals">
                            @include('front.pages.products_modals')
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <button class="btn btn-icon-only btn--link product-preview__slider-btn-prev btn--prev">
                        <svg class="icon stroke-current" width="52" height="52">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                    <button class="btn btn-icon-only btn--link product-preview__slider-btn-next btn--next">
                        <svg class="icon stroke-current" width="52" height="52">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        var url = '{{ route('product_list') }}';

        function getFilteredProducts() {
            var formData = $('input[name ="product-filter"]:checked').val();

            var categoriesQuery = '&categories%5B' + formData + '%5D=on';
            if (formData == 'all') {
                categoriesQuery = '';
            }

            var newUrl = url + '?limit=12&show_in_catalog=1&sort_by=order_in_catalog&order=ASC' + categoriesQuery;
            $('.products-main__list').addClass('is-loading');
            $.ajax({
                url: newUrl
            }).done(function (data) {
                if (!data.hasOwnProperty('error')) {
                    $('#catalog-container').html(data.html);
                    $('#jproduct-modals').html(data.modals);

                    App.initPreviewModalSlider();

                    App.initProduct();
                    $('.products-main__list').removeClass('is-loading');
                } else {
                    alert(data.error);
                }
            });
        }
        $('input[name ="product-filter"]').on('change', function () {
            url = '{{ route('product_list') }}';
            getFilteredProducts();
        });
    });

</script>