@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');

    $images = json_decode($module->images);
    $newImages = [];

    foreach ($images as $image) {
        $newImages[] = \App\Product::getAssetOrDefault($image->image);
    }
@endphp
<section class="about">
    <div class="about__slider">
        <div class="swiper-container about-gallery">
            <div class="swiper-wrapper">
                @foreach($newImages as $image)
                    <div class="swiper-slide">
                        <img src="{{ $image }}" alt=""/>
                    </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
        <div class="about-gallery__btns">
            <button class="btn btn-icon-only btn--link btn--prev about-gallery__btn-prev"><span></span>
                <svg class="icon stroke-current" width="24" height="24">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                </svg>
            </button>
            <button class="btn btn-icon-only btn--link btn--next about-gallery__btn-next"><span></span>
                <svg class="icon stroke-current" width="24" height="24">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                </svg>
            </button>
        </div>
        <div class="modal">
            <div class="modal__wrapper">
                <div class="modal__close">
                    <svg class="icon stroke-black" width="20" height="20">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                    </svg>
                </div>
                <div class="modal__content">
                    <div class="swiper-container gallery-swiper">
                        <div class="swiper-wrapper">
                            @foreach($newImages as $image)
                                <div class="swiper-slide">
                                    <img src="{{ $image }}" alt=""/>
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about__text">
        <h2 class="title--xxl ff-secondary section-title about__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $module->title) !!}</h2>
        <div class="about__text-wrapper">
            <div class="static">
                {!! \App\Http\Helpers\webus_help::translate_manual($locale, $module->description) !!}
            </div>
        </div>
    </div>
</section>