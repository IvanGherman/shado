@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<section class="partnership">
    <div class="partnership__wrapper toggle-hidden hidden-sm">
        <h2 class="title--xxl ff-secondary section-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->our_clients) !!}</h2>
        @if ($visible)
        <div class="toggle-hidden__content">
            <div class="row">
                @foreach($visible as $client)
                    <div class="col col-2">
                        <div class="partnership__item">
                            <img src="{{ asset($client['logo']) }}"/>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @endif
        @if ($hidden)
            <div class="toggle-hidden__content--hidden">
                <div class="row">
                    @foreach($hidden as $item)
                        <div class="col col-2">
                            <div class="partnership__item">
                                <img src="{{ asset($item['logo']) }}"/>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="align--center mt-30">
                <button class="btn btn--primary btn--custom toggle-hidden__btn">
                    <span class="toggle-hidden__btn-text visible">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->show_more) !!}</span>
                    <span class="toggle-hidden__btn-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->hide) !!}   </span>
                </button>
            </div>
        @endif
    </div>
    <div class="partnership__wrapper toggle-hidden hidden-sm-up">
        <div class="accordion">
            <div class="accordion__item">
                <div class="accordion__title accordion__title--section-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->our_clients) !!}
                    <div class="accordion__icon"></div>
                </div>
                <div class="accordion__content">
                    <div class="some-content">
                        @if ($visibleMobile)
                            <div class="toggle-hidden__content">
                                <div class="row">
                                    @foreach($visibleMobile as $item)
                                        <div class="col col-sm-6">
                                            <div class="partnership__item">
                                                <img src="{{ asset($item['logo']) }}"/>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if ($hiddenMobile)
                            <div class="toggle-hidden__content--hidden">
                                <div class="row">
                                    @foreach($hiddenMobile as $item)
                                        <div class="col col-sm-6">
                                            <div class="partnership__item">
                                                <img src="{{ asset($item['logo']) }}"/>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="align--center mt-sm-10">
                                <button class="btn btn--primary btn--custom toggle-hidden__btn">
                                    <span class="toggle-hidden__btn-text visible">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->show_more) !!}</span>
                                    <span class="toggle-hidden__btn-text">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->hide) !!}   </span>
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>