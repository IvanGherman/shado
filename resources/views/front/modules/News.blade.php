@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<section class="news-preview">
    <div class="news-preview__wrapper align--center hidden-sm">
        <h2 class="title--xxl ff-secondary section-title news-preview__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->news) !!}</h2>
        <div class="container">
            <div class="row row--gap-1">
                @foreach($items as $news)
                    <div class="col-4 col-md-6 col-sm-12">
                        <a class="news-card" href="{{ route('news_view', ['slug' => $news->slug]) }}">
                            @if ($news->image)
                                <div class="news-card__img">
                                    <img src="{{ asset($news->image) }}" alt="{{ $news->title }}"/>
                                </div>
                            @endif
                            <div class="news-card__text">
                                <h3 class="title--sm news-card__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $news->title) !!}</h3>
                                @php
                                    $desc = \App\Http\Helpers\webus_help::translate_manual($locale, $news->short_description);
                                    $description = strlen($desc) > 200
                                        ? substr($desc, 0, 200) . '...'
                                        : $desc
                                    ;
                                @endphp
                                <p class="news-card__text-content">{!! $description !!}</p>
                                <div class="news-card__date">{{ $news->getDate($locale) }}</div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="align--center">
                <a class="btn btn--primary btn--custom" href="{{ route('news_view_all') }}">
                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->all_news) !!}</span>
                </a>
            </div>
        </div>
    </div>
    <div class="news-preview__wrapper hidden-sm-up">
        <div class="accordion">
            <div class="accordion__item">
                <div class="accordion__title accordion__title--section-title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->news) !!}
                    <div class="accordion__icon"></div>
                </div>
                <div class="accordion__content">
                    <div class="some-content">
                        @if ($items and $items->count() > 0)
                        @php $news = $items->first(); @endphp
                            <a class="news-card" href="{{ route('news_view', ['slug' => $news->slug]) }}">
                                <div class="news-card__img">
                                    <img src="{{ asset($news->image) }}" alt="{{ $news->title }}"/>
                                </div>
                                <div class="news-card__text">
                                    <h3 class="title--sm news-card__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $news->title) !!}</h3>
                                    <p class="news-card__text-content">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $news->short_description) !!}</p>
                                    <div class="news-card__date">{{ $news->created_at }}</div>
                                </div>
                            </a>
                        @endif
                        <div class="align--center">
                            <a class="btn btn--primary btn--custom" href="{{ route('news_view_all') }}">
                                <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->all_news) !!}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>