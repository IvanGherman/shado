@if($module != null)
    @php
        $locale = Config::get('app.locale');
    @endphp
    <section class="promo-text hidden-sm">
        <span class="promo-text__wrapper">{!! \App\Http\Shortcodes\shortcodes::do_shortcode(\App\Http\Helpers\webus_help::translate_manual($locale, $module->body)) !!}</span>
    </section>
@endif