<div class="static__gallery gallery">
    <div class="swiper-container js-static-gallery">
        <div class="swiper-wrapper">
            @foreach($images as $key => $image)
                <div class="swiper-slide">
                    <div class="static__gallery-img-wrapper gallery__item" data-index="{{ $key }}">
                        <img class="static__gallery-img" src="{{ asset($image) }}"/>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
    <div class="static__gallery-btns">
        <button class="static__gallery-btn static__gallery-btn-prev">
            <svg class="icon stroke-current" width="24" height="24">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
            </svg>
        </button>
        <button class="static__gallery-btn static__gallery-btn-next">
            <svg class="icon stroke-current" width="24" height="24">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
            </svg>
        </button>
    </div>
    <div class="modal modal--no-bg">
        <div class="modal__wrapper">
            <div class="modal__close">
                <svg class="icon stroke-black" width="20" height="20">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                </svg>
            </div>
            <div class="modal__content">
                <div class="swiper-container gallery-swiper">
                    <div class="swiper-wrapper">
                        @foreach($images as $key => $image)
                        <div class="swiper-slide">
                            <img src="{{ \App\Product::getAssetOrDefault($image) }}" alt=""/>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
                <div class="static__modal-gallery-btns">
                    <button class="static__gallery-btn static__modal-gallery-btn-prev gallery-swiper__btn-prev btn--prev">
                        <svg class="icon stroke-current" width="24" height="24">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                    <button class="static__gallery-btn static__modal-gallery-btn-next gallery-swiper__btn-next btn--next">
                        <svg class="icon stroke-current" width="24" height="24">
                            <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
