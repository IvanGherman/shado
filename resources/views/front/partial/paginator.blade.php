@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="pagination__item">
                <a class="btn btn-icon-only btn--link btn--xs btn--reverse-icon innactive" href="#">
                    <span></span>
                    <svg class="icon" width="32" height="32">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow-thin-right') }}"></use>
                    </svg>
                </a>
            </li>
        @else
            <li class="pagination__item">
                <a class="btn btn-icon-only btn--link btn--xs btn--reverse-icon" href="{{ $paginator->previousPageUrl() }}">
                    <span></span>
                    <svg class="icon" width="32" height="32">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow-thin-right') }}"></use>
                    </svg>
                </a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="pagination__item pagination__item--empty"> <span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="pagination__item">
                            <a class="btn btn--link pagination__btn active" href="{{ $url }}">
                                <span>{{ $page }}</span>
                            </a>
                        </li>
                    @else
                        <li class="pagination__item">
                            <a class="btn btn--link pagination__btn" href="{{ $url }}">
                                <span>{{ $page }}</span>
                            </a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pagination__item">
                <a class="btn btn-icon-only btn--link btn--xs" href="{{ $paginator->nextPageUrl() }}">
                    <span></span>
                    <svg class="icon" width="32" height="32">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow-thin-right') }}"></use>
                    </svg>
                </a>
            </li>
        @else
            <li class="pagination__item">
                <a class="btn btn-icon-only btn--link btn--xs innactive" href="#">
                    <span></span>
                    <svg class="icon" width="32" height="32">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow-thin-right') }}"></use>
                    </svg>
                </a>
            </li>
        @endif
    </ul>
@endif
