@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="pagination__item">
                <button class="btn btn-icon-only btn--nopadding btn--link btn--prev btn--disabled"><span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
            </li>
        @else
            <li class="pagination__item">
                <button class="btn btn-icon-only btn--nopadding btn--link btn--prev paginator" data-href="{{ $paginator->previousPageUrl() }}"><span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="pagination__item pagination__item--empty"> <span>...</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="pagination__item">
                            <a class="btn btn--link btn--xs pagination__btn active"><span>{{ $page }}</span>
                            </a>
                        </li>
                    @else
                        <li class="pagination__item">
                            <a class="btn btn--link btn--xs pagination__btn paginator" data-href="{{ $url }}"><span>{{ $page }}</span>
                            </a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pagination__item">
                <button class="btn btn-icon-only btn--nopadding btn--link btn--next paginator" data-href="{{ $paginator->nextPageUrl() }}">
                    <span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
            </li>
        @else
            <li class="pagination__item">
                <button class="btn btn-icon-only btn--nopadding btn--link btn--next btn--disabled" data-href="{{ $paginator->nextPageUrl() }}">
                    <span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
            </li>
        @endif
    </ul>
@endif
