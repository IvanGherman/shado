@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<div class="footer">
    <div class="footer__content">
        <div class="footer__logo-container">
            <div class="footer__logo">
                <img src="{{ asset('assets/img/shado-logo.svg') }}" alt=""/></div>
        </div>
        <div class="contacts__header hidden-sm">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->contact_us) !!}       </div>
        <div class="contacts__header hidden-sm-up">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->we_in_social) !!}    </div>
        <div class="contacts hidden-sm">
            <a class="contacts__link" href="tel:{{ \App\Variables::changePhone($variables, 1) }}">{{ \App\Variables::changePhone($variables, 1) }}</a>
            @if (!empty(\App\Variables::changePhone($variables, 2)))
            <a class="contacts__link" href="tel:{{ \App\Variables::changePhone($variables, 2) }}">{{ \App\Variables::changePhone($variables, 2) }}</a>
            @endif
        </div>
        <div class="socials">
            <a class="btn btn-icon-only btn--link socials__btn" href="{{ $variables->facebook }}"><span></span>
                <svg class="icon" width="24" height="24">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-fb') }}"></use>
                </svg>
            </a>
            <a class="btn btn-icon-only btn--link socials__btn" href="{{ $variables->instagram }}"><span></span>
                <svg class="icon" width="24" height="24">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-insta') }}"></use>
                </svg>
            </a>
            <a class="btn btn-icon-only btn--link socials__btn" href="{{ $variables->whatsapp }}"><span></span>
                <svg class="icon" width="24" height="24">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-what') }}"></use>
                </svg>
            </a>
            <a class="btn btn-icon-only btn--link socials__btn" href="{{ $variables->viber }}"><span></span>
                <svg class="icon" width="24" height="24">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-viber') }}"></use>
                </svg>
            </a>
        </div>
        <button class="btn btn-ghost-primary-dark btn--no-lsp hidden-sm"
                data-modal="#request-modal">
            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->request_master) !!}</span>
        </button>
    </div>
    <a class="copyright" href="https://webus.md">
        <div class="copyright__text">Developed by:</div>
        <div class="copyright__logo">
            <img src="{{ asset('assets/img/logo-webus.svg') }}" alt=""/>
        </div>
    </a>
    <div class="footer__fixed-btns hidden-sm-up is-hidden">
        <a class="btn btn-icon-only btn--link btn__round-icon" href="tel:{{ \App\Variables::changePhone($variables, 1) }}"><span></span>
            <svg class="icon" width="32" height="32">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-phone') }}"></use>
            </svg>
        </a>
        <button class="btn btn--link f-grow" data-modal="#request-modal">
            <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->request_master) !!}</span>
        </button>
        <button class="btn btn-icon-only btn--link btn__round-icon js-scroll-to-top"><span></span>
            <svg class="icon" width="18" height="18">
                <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow-up') }}"></use>
            </svg>
        </button>
    </div>
</div>