@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
<div class="header__mega-menu bg-white shadow hidden-xl">
    <div class="header__mega-menu-wrapper">
        <div class="products-filters">
            <div class="products-filters__container">
                <div class="swiper-container js-filters-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="products-filters__item">
                                <a class="products-filters__wrapper" href="{{ route('product_list') }}">
                                    <div class="products-filters__icon">
                                        <img src="{{ asset('assets/img/customer-img/blinds-all.svg') }}" alt=""/>
                                    </div>
                                    <div class="products-filters__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->all) !!}</div>
                                </a>
                            </div>
                        </div>
                        @foreach($categories as $category)
                            <div class="swiper-slide">
                                <div class="products-filters__item">
                                    <a class="products-filters__wrapper" href="{{ route('product_list', ['categories' => [$category['id'] => 'on']]) }}">
                                        <div class="products-filters__icon">
                                            <img src="{{ \App\Product::getAssetOrDefault($category['image']) }}">
                                        </div>
                                        <div class="products-filters__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $category['title']) !!}</div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        @foreach($customMenu as $key => $item)
                            <div class="swiper-slide @if ($key == 0) separator @endif">
                                <div class="products-filters__item">
                                    <a class="products-filters__wrapper" href="{{ url($item->url) }}">
                                        <div class="products-filters__icon color-primary">
                                            <img src="{{ asset($item->image) }}">
                                        </div>
                                        <div class="products-filters__title">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $item->title) !!}</div></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
            <div class="products-filters__btns">
                <button class="btn btn-icon-only btn--link btn--prev products-filters__btn-prev"><span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
                <button class="btn btn-icon-only btn--link btn--next products-filters__btn-next"><span></span>
                    <svg class="icon stroke-current" width="24" height="24">
                        <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-arrow') }}"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>
</div>