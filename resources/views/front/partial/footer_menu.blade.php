@php
    $locale = Config::get('app.locale');
@endphp
<h5 class="item__title color--white">{{ $menu->name }}</h5>
<ul class="footer__item-list">
    @foreach($items->sortBy('item_order') as $item)
        <li class="footer__item-wrap">
            <a class="footer__item-link"
               href="@if(!empty($item['url'])){{ LaravelLocalization::localizeUrl($item['url']) }}@endif"
               target="{{ $item['target'] }}"
            >{{ \App\Http\Helpers\webus_help::translate_manual($locale, $item['name']) }}</a>
        </li>
    @endforeach
</ul>