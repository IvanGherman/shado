<div class="container container--short">
    <div class="video-container__wrapper">
        <button class="btn btn-icon-only btn--nopadding btn--primary video-container"
                data-id="https://www.youtube.com/embed/{{ $id }}"
                data-modal="#modal-static-{{ $id }}">
            <img src="{!! url($preview) !!}" data-modal="#modal-static-{{ $id }}">
        </button>
        <span class="video-container__label">{{ $title }}</span>
    </div>

    <div class="modal modal-video" id="modal-static-{{ $id }}">
        <div class="modal__wrapper">
            <div class="modal__close">
                <svg class="icon stroke-black" width="20" height="20">
                    <use xlink:href="{{ asset('assets/img/sprite.min.svg#ic-close') }}"></use>
                </svg>
            </div>
            <div class="modal__content">
                <div class="iframe iframe--video">
                    <iframe data-src="https://www.youtube.com/embed/{{ $id }}"
                            allow="autoplay; encrypted-media" allowfullscreen="">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</div>


