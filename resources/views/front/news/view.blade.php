@extends('layouts.front')
@php
    $locale = Config::get('app.locale');
@endphp
@section('content')
<section class="article">
    <div class="static">
        <div class="static-intro">
            @if ($news->big_image)
            <div class="static-intro__main">
                <img src="{{ asset($news->big_image) }}" alt=""/>
                <div class="static-intro__text">
                    <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $news->title) !!}</span>
                </div>
            </div>
            @endif
            <div class="static-intro__label">{{ $news->getDate($locale) }}</div>
        </div>
        <div class="container container--short">
            {!! $news->getDescription() !!}
        </div>
    </div>
</section>
@endsection
