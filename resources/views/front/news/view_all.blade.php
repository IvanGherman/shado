@extends('layouts.front')
@php
    $variables = \App\Http\Helpers\webus_help::get_variables()->getContent();
    $variables = json_decode($variables);

    $translations = \App\Http\Helpers\webus_help::get_translations()->getContent();
    $translations = json_decode($translations);

    $locale = Config::get('app.locale');
@endphp
@section('content')
    <section>
        <div class="container">
            <h2 class="section-title--alt">{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->our_news) !!}</h2>
            <div class="row row--gap-1" id="js-news-container">
                @include('front.news.partial_news')
            </div>
            @if ($amount < $total)
                <div class="align--center" id="more-news">
                    <button class="btn btn--primary btn--custom js-load-news">
                        <span>{!! \App\Http\Helpers\webus_help::translate_manual($locale, $translations->previous_news) !!}</span>
                    </button>
                </div>
            @endif
        </div>
    </section>
    <script>
        var offset = +'{{ $limit }}';
        var limit = +'{{ $limit }}';
        var amount = +'{{ $amount }}';
        var total = +'{{ $total }}';

        $('.js-load-news').on('click', function () {
            $.ajax({
                url: '{{ route('news_view_all') }}?offset=' + offset
            }).done(function (data) {
                if (!data.hasOwnProperty('error')) {
                    amount += data.amount;
                    $('#js-news-container').append(data.html);

                    if (amount >= total) {
                        $('#more-news').hide();
                    }
                    offset = offset + limit;
                } else {
                    alert(data.error);
                }
            });
        });
    </script>
@endsection
