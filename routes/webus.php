<?php
Route::group(['prefix' => 'admin'], function() use($custom_routes,$shop_routes)
{
    //Login to panel
    Route::get('login', ['uses' => 'Webus\WebusAuthController@index', 'as' => 'admin_login']);
    Route::post('login', ['uses' => 'Webus\WebusAuthController@postLogin', 'as' => 'admin_postlogin']);


    Route::group(['middleware' => 'webus_admin'], function () use($custom_routes,$shop_routes) { //start middleware admin role

        //Dashboard
        Route::get('/', ['uses' => 'Webus\WebusDashboardController@index', 'as' => 'dashboard']);
        Route::post('logout', ['uses' => 'Webus\WebusDashboardController@logout', 'as' => 'logout']);
        Route::get('profile', ['uses' => 'Webus\WebusDashboardController@profile', 'as' => 'profile']);
        Route::post('profile/update_profile', ['uses' => 'Webus\WebusDashboardController@update_profile', 'as' => 'update_profile']);

        //Settings
        Route::get('settings', ['uses' => 'Webus\WebusAdminSiteSettingsController@index', 'as' => 'settings']);
        Route::post('settings/add', ['uses' => 'Webus\WebusAdminSiteSettingsController@add', 'as' => 'add_settings']);
        Route::post('settings/save_options', ['uses' => 'Webus\WebusAdminSiteSettingsController@save_options', 'as' => 'save_options_settings']);
        Route::delete('settings/delete_option/{setting_id}', ['uses' => 'Webus\WebusAdminSiteSettingsController@delete', 'as' => 'delete_option_settings']);

        //Icons
        Route::get('icons', ['uses' => 'Webus\WebusDashboardController@icons', 'as' => 'icons']);

        //Modules
        Route::get('modules', ['uses' => 'Webus\WebusModulesController@index', 'as' => 'modules']);
        foreach (scandir(app_path('Http/Controllers/Modules')) as $file)
        {
            if($file != '.' && $file != '..'){
                $name_file = 'Modules\\'.pathinfo($file)['filename'];
                Route::get('modules/add/'.pathinfo($file)['filename'], ['uses' => $name_file.'@create', 'as' => 'create_'.pathinfo($file)['filename']]);
                Route::post('modules/save/'.pathinfo($file)['filename'], ['uses' => $name_file.'@save', 'as'=> 'save_'.pathinfo($file)['filename']]);
                Route::get('modules/edit/'.pathinfo($file)['filename'].'/{setting_id}', ['uses' => $name_file.'@edit', 'as' => 'edit_'.pathinfo($file)['filename']]);
                Route::put('modules/update/'.pathinfo($file)['filename'].'/{setting_id}', ['uses' => $name_file.'@update', 'as'=> 'update_'.pathinfo($file)['filename']]);
                Route::post('modules/delete/'.pathinfo($file)['filename'].'/{setting_id}', ['uses' => $name_file.'@delete', 'as'=> 'delete_'.pathinfo($file)['filename']]);

            }
        }

        //Menu
        Route::group([
            'as'     => 'menu.',
            'prefix' => 'menu',
        ], function () {
            Route::get('/', ['uses' => 'Webus\WebusMenuController@index', 'as' => 'menu']);
            Route::get('create', ['uses' => 'Webus\WebusMenuController@create', 'as' => 'create']);
            Route::post('store', ['uses' => 'Webus\WebusMenuController@store', 'as' => 'store']);
            Route::get('edit/{id}', ['uses' => 'Webus\WebusMenuController@edit', 'as' => 'edit']);
            Route::put('update/{id}', ['uses' => 'Webus\WebusMenuController@update', 'as' => 'update']);
            Route::post('delete/{id}', ['uses' => 'Webus\WebusMenuController@delete', 'as' => 'delete']);

            //build menu(custom function, not standart)
            Route::get('build/{id}', ['uses' => 'Webus\WebusMenuController@build', 'as' => 'build']);
            Route::post('add_new_item/{id}', ['uses' => 'Webus\WebusMenuController@add_new_item', 'as' => 'add_new_item']);
            Route::delete('delete_item/{id}', ['uses' => 'Webus\WebusMenuController@delete_item', 'as' => 'delete_item']);
            Route::put('update_item/{id}', ['uses' => 'Webus\WebusMenuController@update_item', 'as' => 'update_item']);
            Route::post('order/{id}', ['uses' => 'Webus\WebusMenuController@order', 'as' => 'order']);

        });

        // Media
        Route::group([
            'as'     => 'media.',
            'prefix' => 'media',
        ], function () {
            Route::get('/', ['uses' => 'Webus\WebusMediaController@index', 'as' => 'index']);
            Route::get('frame-manager', ['uses' => 'Webus\WebusMediaController@index_frame', 'as' => 'frame-manager']);
            Route::post('files', ['uses' => 'Webus\WebusMediaController@files', 'as' => 'files']);
            Route::post('new_folder', ['uses' => 'Webus\WebusMediaController@new_folder', 'as' => 'new_folder']);
            Route::post('delete_file_folder', ['uses' => 'Webus\WebusMediaController@delete_file_folder', 'as' => 'delete_file_folder']);
            Route::post('directories', ['uses' => 'Webus\WebusMediaController@get_all_dirs', 'as' => 'get_all_dirs']);
            Route::post('move_file', ['uses' => 'Webus\WebusMediaController@move_file', 'as' => 'move_file']);
            Route::post('rename_file', ['uses' => 'Webus\WebusMediaController@rename_file', 'as' => 'rename_file']);
            Route::post('upload', ['uses' => 'Webus\WebusMediaController@upload', 'as' => 'upload']);
            Route::post('remove', ['uses' => 'Webus\WebusMediaController@remove', 'as' => 'remove']);
        });
        //End Admin Media

        if($shop_routes){
            //Shop Orders
            Route::get('orders', ['uses' => 'Webus\Shop\AdminShopOrdersController@index', 'as' => 'admin_orders']);
            Route::get('orders/view/{id}', ['uses' => 'Webus\Shop\AdminShopOrdersController@edit', 'as' => 'admin_order_view']);
            Route::post('orders/update/{id}', ['uses' => 'Webus\Shop\AdminShopOrdersController@update', 'as' => 'admin_order_update']);
            Route::post('orders/delete/{id}', ['uses' => 'Webus\Shop\AdminShopOrdersController@delete', 'as' => 'admin_order_delete']);
            //End shop Orders
        }

        $users = ['users' => 'Webus\WebusUserController'];
        $custom_routes = $custom_routes+$users;
        //Custom Routes
        foreach ($custom_routes as $route => $controller){
            if ($route === 'values') {
                Route::group([
                    'as'     => $route.'.',
                    'prefix' => 'attributes/{attributeId}/values',
                ], function () use($route, $controller) {
                    Route::get('/', ['uses' => $controller.'@indexAction', 'as' => $route]);
                    Route::get('create', ['uses' => $controller.'@createItem', 'as' => 'create']);
                    Route::post('store', ['uses' => $controller.'@storeItem', 'as' => 'store']);
                    Route::get('edit/{id}', ['uses' => $controller.'@editItem', 'as' => 'edit']);
                    Route::put('update/{id}', ['uses' => $controller.'@updateItem', 'as' => 'update']);
                    Route::post('delete/{id}', ['uses' => $controller.'@deleteItem', 'as' => 'delete']);
                    Route::post('delete_mass', ['uses' => $controller.'@delete_mass_item', 'as' => 'delete_mass']);
                });
            } else {
                Route::group([
                    'as'     => $route.'.',
                    'prefix' => $route,
                ], function () use($route, $controller) {
                    Route::get('/', ['uses' => $controller.'@index', 'as' => $route]);
                    Route::get('create', ['uses' => $controller.'@create', 'as' => 'create']);
                    Route::post('store', ['uses' => $controller.'@store', 'as' => 'store']);
                    Route::get('edit/{id}', ['uses' => $controller.'@edit', 'as' => 'edit']);
                    Route::put('update/{id}', ['uses' => $controller.'@update', 'as' => 'update']);
                    Route::post('delete/{id}', ['uses' => $controller.'@delete', 'as' => 'delete']);
                    Route::post('delete_mass', ['uses' => $controller.'@delete_mass', 'as' => 'delete_mass']);
                });
            }

            if (in_array($route, ['attributes', 'categories', 'products'])) {
                Route::group([
                    'as'     => $route.'.',
                    'prefix' => $route,
                ], function () use($route, $controller) {
                    Route::get('import', ['uses' => $controller.'@import', 'as' => 'import']);
                    Route::post('import/do', ['uses' => $controller.'@importDo', 'as' => 'import_do']);
                });
            }

            if (in_array($route, ['products'])) {
                Route::group([
                    'as'     => $route.'.',
                    'prefix' => $route,
                ], function () use($route, $controller) {
                    Route::post('show-in-catalog', ['uses' => $controller.'@showInCatalog', 'as' => 'show_in_catalog']);
                });
            }
        }
        //End custom routes

    }); //end middleware admin role

});