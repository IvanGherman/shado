<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now  create something great!
| Created by WebUs, from components used by other systems created on the Laravel 5
*/
App::setLocale('ro');

$custom_routes = [
    'pages' => 'Admin\PagesController',
    'news' => 'Admin\NewsController',
    'variables' => 'Admin\VariablesController',
    'galleries' => 'Admin\GalleriesController',
    'translations' => 'Admin\TranslationsController',
    'categories' => 'Admin\CategoriesController',
    'textures' => 'Admin\TextureController',
    'colors' => 'Admin\ColorsController',
    'transparencies' => 'Admin\TransparenciesController',
    'countries' => 'Admin\CountriesController',
    'tree-types' => 'Admin\TreeTypesController',
    'attributes' => 'Admin\AttributesController',
    'products' => 'Admin\ProductsController',
    'values' => 'Admin\ValuesController',
    'client' => 'Admin\ClientController',
    'reviews' => 'Admin\ReviewsController',
    'comforts' => 'Admin\ComfortController',
    'faq' => 'Admin\FAQController',
    'contacts' => 'Admin\ContactsController',
    'selected-products' => 'Admin\SelectedProductsController',
    'custom-menu' => 'Admin\CustomMenuController',
    'order-steps' => 'Admin\OrderStepsController',
];
$shop_routes = false; //true or false

require ('webus.php');

/***************************************************Front-End**************************************************************/


Route::any('/register',function(){
    return redirect('/');
});

Auth::routes();


Route::post('/request-master', ['uses' => 'Front\HomeController@requestMaster', 'as' => 'request_master']);
Route::post('/call-request', ['uses' => 'Front\HomeController@callRequest', 'as' => 'request_master']);
Route::post('/contact', ['uses' => 'Front\HomeController@contact', 'as' => 'contact']);

Route::get('/clear-cache', function() {
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/link-create', function () {
    Artisan::call('storage:link');
	return "Link created";
});

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
    ], function()
{
    Route::get('/', ['uses' => 'Front\HomeController@index', 'as' => 'home']);
    Route::get('/no-ie', ['uses' => 'Front\HomeController@noExplorer', 'as' => 'no_ie']);
    Route::get('/news', ['uses' => 'Front\NewsController@viewAll', 'as' => 'news_view_all']);
    Route::get('/news/{slug}', ['uses' => 'Front\NewsController@view', 'as' => 'news_view']);
    Route::get('/products', ['uses' => 'Front\ProductController@viewAll', 'as' => 'product_list']);
    Route::get('/product/{id}', ['uses' => 'Front\ProductController@view', 'as' => 'product']);
    Route::post('/review', ['uses' => 'Front\HomeController@review', 'as' => 'review']);
    Route::get('/faq', ['uses' => 'Front\HomeController@faq', 'as' => 'faq_front']);
    Route::get('/contacts', ['uses' => 'Front\ContactsController@viewAll', 'as' => 'contacts']);
    Route::get('{slug}', 'Front\PagesController@index');

});
