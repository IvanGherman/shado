<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommonProduct extends Model
{
    protected $table = 'common_products';
}
