<?php

namespace App\WebusModels;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    protected $table = 'modules';
    public $timestamps = false;

}
