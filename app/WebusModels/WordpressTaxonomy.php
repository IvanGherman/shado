<?php

namespace App\WebusModels;

use App\Http\Helpers\webus_help;
use Illuminate\Database\Eloquent\Model;
use DB;
class WordpressTaxonomy extends Model
{
//    protected $table = 'wp_term_taxonomy';
//
//    public $timestamps = false;


    static function insert_term($data, $taxonomy){
        $insert_term = DB::table('wp_terms')->insertGetId([
            'name' => $data['name'],
            'slug' => $data['slug']
        ]);

        if(isset($data['parent'])){$parent = $data['parent'];}else{$parent = '0';}
        $insert_term_tax = DB::table('wp_term_taxonomy')->insert([
            'term_id' => $insert_term,
            'taxonomy' => $taxonomy,
            'parent' => $parent,
        ]);

        return $insert_term;
    }

    static function update_term($data, $term_id, $taxonomy){
        if(isset($data['name']) || $data['slug']){
            DB::table('wp_terms')
                ->where('term_id', $term_id)
                ->update(['name' => $data['name'], 'slug' => $data['slug']]);
        }

        if(isset($data['parent'])){
            DB::table('wp_term_taxonomy')
                ->where('term_id', $term_id)
                ->where('taxonomy', $taxonomy)
                ->update(['parent' => $data['parent']]);
        }

        return true;

    }

    static function get_terms($taxonomy){
        $query = DB::table('wp_terms')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_id', '=', 'wp_terms.term_id')
            ->where('wp_term_taxonomy.taxonomy', $taxonomy)
            ->orderBy('wp_terms.name', 'asc')
            ->paginate(25);
        return $query;
    }

    static function get_terms_array($taxonomy){
        $query = DB::table('wp_terms')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_id', '=', 'wp_terms.term_id')
            ->where('wp_term_taxonomy.taxonomy', $taxonomy)
            ->orderBy('wp_terms.name', 'asc')
            ->get();

        $terms = array();
        $terms[0] = 'None';
        if($query){
            foreach ($query as $item){
                $terms[$item->term_id] = webus_help::translate_automat($item->name);
            }
        }

        return $terms;

    }



    static function get_term_by_id_taxonomy($taxonomy, $term_id){
        $query = DB::table('wp_terms')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_id', '=', 'wp_terms.term_id')
            ->where('wp_term_taxonomy.taxonomy', $taxonomy)
            ->where('wp_terms.term_id', $term_id)
            ->first();
        return $query;
    }


    static function get_selected_relation($object_id, $taxonomy){
        $query = DB::table('wp_term_relationships')
            ->join('wp_term_taxonomy', 'wp_term_relationships.term_taxonomy_id', '=', 'wp_term_taxonomy.term_taxonomy_id')
            ->select(DB::raw('wp_term_taxonomy.term_id as term_id'))
            ->where('object_id', $object_id)
            ->where('wp_term_taxonomy.taxonomy', $taxonomy)
            //->groupBy('wp_term_relationships.term_taxonomy_id')
            ->get();

        $relations = array();

        foreach ($query as $item){
            $relations[$item->term_id] = $item->term_id;
        }

        return $relations;
    }


    static function delete_term($term_id, $taxonomy){
        DB::table('wp_terms')->where('term_id', $term_id)->delete();
        DB::table('wp_term_taxonomy')->where('term_id', $term_id)->where('taxonomy', $taxonomy)->delete();
        return true;
    }


    static function atach_terms_to_post($post_id, $data, $taxonomy){
        if($data){

            DB::table('wp_term_relationships')->where('object_id', $post_id)->delete();

            foreach ($data as $term_id){

                $atach_taxonomy = DB::table('wp_term_relationships')->insert([
                    'object_id' => $post_id,
                    'term_taxonomy_id' => $term_id
                ]);

            }
        }

    }

}
