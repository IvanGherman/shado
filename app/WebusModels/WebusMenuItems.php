<?php

namespace App\WebusModels;

use Illuminate\Database\Eloquent\Model;

class WebusMenuItems extends Model
{
    protected $table = 'menu_items';
    protected $fillable = [
        'item_order',
        'menu_id',
        'parent_id',
        'name',
        'url',
        'icon',
        'class',
        'target',
        'create_at',
        'updated_at'
    ];

    public function children()
    {
        return $this->hasMany('App\WebusModels\WebusMenuItems', 'parent_id')->with('children');
    }

}
