<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';

    public function getSchedulePeriods()
    {
        $schedule = unserialize($this->schedule);

        $periods = [];

        $period = null;
        foreach ($schedule as $day => $fromTo) {
            if (!empty($period)) {
                if ($period['fromTo']['from'] === $fromTo['from'] && $period['fromTo']['to'] === $fromTo['to']) {
                    $period['counter']++;
                } else {
                    $periods[] = $period;
                    $period = null;
                }
            }

            if (empty($period)) {
                $period = [
                    'fromTo' => $fromTo,
                    'counter' => 1,
                ];
            }
        }

        $periods[] = $period;

        return $periods;
    }
}
