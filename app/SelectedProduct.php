<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelectedProduct extends Model
{
    const DEFAULT_IMAGE = '/assets/img/no_image.svg';

    protected $table = 'selected_products';

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

    public function getImage()
    {
        if (!empty($this->image) && file_exists(public_path().$this->image)) {
            return $this->image;
        }

        return self::DEFAULT_IMAGE;
    }
}
