<?php

namespace App\Http\Services\ImportMapper;

use App\Attribute;

class AttributeMapper extends ImportMapper
{
    const SHEET = 'Доп. Атрибуты';
    protected $model = 'App\Attribute';

    public $map = [
        'A' => [
            'title' => 'id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'C' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
        'D' => [
            'title' => 'position',
            'validate' => false,
        ],
        'E' => [
            'title' => 'show_in_filter',
            'validate' => self::REQUIRED,
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }
}