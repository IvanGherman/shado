<?php

namespace App\Http\Services\ImportMapper;

use App\Transparency;

class TreeTypeMapper extends ImportMapper
{
    const SHEET = 'Вид дерева';

    protected $model = 'App\TreeType';
    protected $imageFolder = self::PREFIX_IMAGE_FOLDER.'/tree_types/';

    public $map = [
        'A' => [
            'title' => 'id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'C' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }
}