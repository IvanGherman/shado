<?php

namespace App\Http\Services\ImportMapper;

use App\AttributeValue;

class AttributeValueMapper extends ImportMapper
{
    const SHEET = 'Показатели атрибута';
    protected $model = 'App\AttributeValue';

    public $map = [
        'A' => [
            'title' => 'attribute_id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'C' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
        'D' => [
            'title' => 'key',
            'validate' => self::REQUIRED,
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }
}