<?php

namespace App\Http\Services\ImportMapper;

use App\Http\Services\ErrorItem;
use App\Http\Services\ImportFeedbackItem;

abstract class ImportMapper
{
    /** NOTE: IF ALL REQUIRED FIELDS ARE MISSING THE ROW IS SKIPPING */
    const REQUIRED = 'required';
    const WRONG_SEPARATOR = 'wrong_separator';
    const DEFAULT_SEPARATOR = ',';
    const PREFIX_IMAGE_FOLDER = '/storage';

    public $map = [];

    protected $data = [];

    protected $errors = [];
    protected $skipped = [];
    protected $skippedBecauseOfDBErrors = [];
    protected $skippedExistent = [];
    protected $saved = 0;

    protected $replace = false;
    protected $model = null;
    protected $imageFolder = '/import/';

    public function getData()
    {
        $this->skipEmptyRows();
        return $this->data;
    }

    protected static $errorMessages = [
        self::REQUIRED => 'Field cannot be empty',
        self::WRONG_SEPARATOR => 'Field contain wrong separator ("." instead of ",").',
    ];

    public function store(int $key, string $column, ?string $value)
    {
        $row = $key + 1;

        if (empty($this->map)) {
            throw new \Exception('Map is empty', 409);
        }

        if (!array_key_exists($column, $this->map)) {
            return;
        }

        if ($this->map[$column]['validate']) {
            if (!$this->isValid($value, $column, $row)) {
                return;
            }
        }

        $lang = $this->map[$column]['lang'] ?? null;
        if (!empty($lang)) {
            $this->data[$row][$this->map[$column]['title']][$lang] = $value;
        } else {
            $this->data[$row][$this->map[$column]['title']] = $value;
        }
    }

    public function isValid(?string $value, string $column, int $row)
    {
        $rules = $this->map[$column]['validate'];
        if (!$rules) {
            return true;
        }

        if (!is_array($rules)) {
            $rules = [$rules];
        }

        foreach ($rules as $rule) {
            if ($rule === self::REQUIRED && empty($value)) {
                if (empty($this->errors[$row])) {
                    $this->errors[$row] = [];
                }
                $this->errors[$row][] = new ErrorItem($row, $column, self::$errorMessages[self::REQUIRED]);
            } elseif ($rule === self::WRONG_SEPARATOR) {
                if (strpos($value, ".") !== false) {
                    if (empty($this->errors[$row])) {
                        $this->errors[$row] = [];
                    }
                    $this->errors[$row][] = new ErrorItem($row, $column, self::$errorMessages[self::WRONG_SEPARATOR]);
                }
            }
        }

        if (!empty($this->errors[$row])) {
            return false;
        }

        return true;
    }

    public function skipEmptyRows()
    {
        $skipRows = [];
        $columns = 0;

        foreach ($this->map as $item) {
            if (!empty($item['validate'])) {
                $columns++;
            }
        }

        foreach ($this->errors as $key => $errorsPerRow) {
            $errorsLength = count($errorsPerRow);

            if ($errorsLength === $columns) {
                $skipRows[] = $key;
                $this->skipped[] = new ImportFeedbackItem($key, $errorsPerRow);
            }
        }

        foreach ($skipRows as $row) {
            unset($this->errors[$row]);
            unset($this->data[$row]);
        }
    }

    public function getErrors()
    {
        $this->skipEmptyRows();
        return $this->errors;
    }

    abstract public function getSheet();

    public function saveModel()
    {
        if (!$this->model) {
            throw new \Exception('Model is not defined', 409);
        }

        $modelName = $this->model;

        $data = $this->getData();

        foreach ($data as $row => $item) {
            $model = new $modelName;

            if (!empty($this->errors[$row])) {
                continue;
            }

            $saveAfter = [];
            foreach ($item as $key => $value) {
                $valueToSave = $value;

                if ($this->isMultipleField($key)) {
                    $valueToSave = [];
                    if (!empty($value)) {
                        $valueToSave = explode(self::DEFAULT_SEPARATOR, $value);
                    }
                }

                switch (true) {
                    case $this->isCollectionField($key):
                        if (!empty($value)) {
                            $saveAfter[] = [
                                'key' => $key,
                                'value' => $value,
                            ];
                        }
                        break;
                    case ($this->isTranslatable($key) && is_array($value)):
                        $valueToSave = '';
                        foreach ($value as $lang => $langValue) {
                            $valueToSave .= '[:'.$lang.']'.$langValue;
                        }
                        $model->$key = $valueToSave;
                        break;
                    case $this->isImageField($key):
                        if ($this->isMultipleField($key)) {
                            $counter = 1;
                            $multipleItems = [];
                            foreach ($valueToSave as $item) {
                                $multipleItems['new'.$counter] = [
                                    'image' => $this->imageFolder . $item,
                                ];
                                $counter++;
                            }

                            if (!empty($multipleItems)) {
                                $model->$key = json_encode($multipleItems);
                            }
                        } elseif (!empty($valueToSave)) {
                            $model->$key = $this->imageFolder . $valueToSave;
                        }
                        break;
                    default:
                        if ($this->isMultipleField($key)) {
                            $model->$key = json_encode($valueToSave);
                        } else {
                            $model->$key = $valueToSave;
                        }
                        break;
                }
            }

            $model->name = $model->title;

            $existing = $modelName::where('id', '=', $model->id)->get()->first();

            if ($existing) {
                $this->skippedExistent[] = $model->id;
                unset($model);
                continue;
            }

            try {
                $model->save();

                foreach ($saveAfter as $toSave) {
                    $this->saveCollection($model, $toSave['key'], $toSave['value'], $row);
                }

                $this->saved++;
            } catch (\Exception $e) {
                $this->skippedBecauseOfDBErrors[] = [
                    'message' => $e->getMessage(),
                    'row' => $row,
                ];
            }
        }
    }

    public function isMultipleField(string $key)
    {
        foreach ($this->map as $item) {
            if ($key === $item['title'] && !empty($item['multiple']) && $item['multiple']) {
                return true;
            }
        }

        return false;
    }

    public function isTranslatable(string $key)
    {
        foreach ($this->map as $item) {
            if ($key === $item['title'] && !empty($item['lang'])) {
                return true;
            }
        }

        return false;
    }

    public function isImageField(string $key)
    {
        foreach ($this->map as $item) {
            if ($key === $item['title'] && !empty($item['is_image']) && $item['is_image']) {
                return true;
            }
        }

        return false;
    }

    public function isCollectionField(string $key)
    {
        foreach ($this->map as $item) {
            if ($key === $item['title'] && !empty($item['collection']) && $item['collection']) {
                return true;
            }
        }

        return false;
    }

    public function saveCollection($model, $key, $data, $row)
    {
        foreach ($this->map as $column => $item) {
            if ($key === $item['title'] && !empty($item['collection'])) {
                $collectionSettings = $item['collection'];
                if (!empty($collectionSettings['pattern']) && $collectionSettings['pattern'] === 'EAV') {
                    $this->saveEAVCollection($model, $key, $data, $collectionSettings['separator'], $row);
                } else {
                    $entityClass = $item['collection']['related'];
                    $collection = explode($item['collection']['separator'], $data);

                    if (!empty($collection) && is_array($collection)) {
                        $items = $entityClass::find($collection);
                        if ($items->count() > 0) {
                            $model->$key()->sync($items);
                        }
                    }
                }
            }
        }
    }

    public function saveEAVCollection($model, $key, $data, $delimiter, $row)
    {

    }

    public function getSaved()
    {
        return $this->saved;
    }

    public function getDBErrors()
    {
        return $this->skippedBecauseOfDBErrors;
    }

    public function getDBErrorsDetailed()
    {
        $detailed = [];
        foreach ($this->skippedBecauseOfDBErrors as $error) {
            $detailed[] = sprintf('Error occured when saving data in row %s with message: "%s"', $error['row'], $error['message']);
        }

        return $detailed;
    }

    public function getErrorsDetailed()
    {
        $detailed = [];

        foreach ($this->errors as $errors) {
            /** @var ErrorItem $item */
            foreach ($errors as $item) {
                $detailed[] = $item->toString();
            }
        }

        return $detailed;
    }
}