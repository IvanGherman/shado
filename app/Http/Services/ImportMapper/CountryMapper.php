<?php

namespace App\Http\Services\ImportMapper;

use App\Country;

class CountryMapper extends ImportMapper
{
    const SHEET = 'Страны производителя';
    protected $model = 'App\Country';

    public $map = [
        'A' => [
            'title' => 'id',
            'validate' => self::REQUIRED,
        ],
        'B' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ro',
        ],
        'C' => [
            'title' => 'title',
            'validate' => self::REQUIRED,
            'lang' => 'ru',
        ],
    ];

    public function getSheet()
    {
        return self::SHEET;
    }
}