<?php

namespace App\Http\Services;

use App\Http\Services\ImportMapper\ImportMapper;

class ImportManager
{
    /** @var ImportMapper  */
    protected $mapper;

    protected $limit = 1000;
    protected $offset = 2;
    protected $total = 0;

    public function __construct(ImportMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    public function setOffset(int $offset)
    {
        $this->offset = $offset + 2;
    }

    protected function createReader($format = 'Xlsx')
    {
        $reader = null;
        if ($format === 'Xlsx') {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $reader->setReadDataOnly(true);
            $reader->setLoadSheetsOnly($this->mapper->getSheet());
        }

        if ($format === 'text/csv') {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            $reader->setInputEncoding('UTF-8');
            $reader->setDelimiter(';');
            $reader->setEnclosure('');
            $reader->setSheetIndex(0);
        }

        if ($reader) {
            return $reader;
        }

        throw new \Exception('Wrong format', 409);
    }

    public function manage($file, string $format = 'Xlsx')
    {
        $reader = $this->createReader($format);

        $spreadsheet = $reader->load($file);
        $worksheetIterator = $spreadsheet->getWorksheetIterator();
        $currentWorksheet = $worksheetIterator->current();

        $this->total = $currentWorksheet->getHighestRow();

        $limit = $this->limit + $this->offset - 1;
        if ($this->total < $limit) {
            $limit = $this->total;
        }

        foreach ($currentWorksheet->getRowIterator($this->offset, $limit) as $rowNumber => $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);

            foreach ($cellIterator as $key => $cell) {
                $cellValue = $cell->getValue();
                $this->mapper->store($rowNumber - 1, $key, $cellValue);
            }
        }
    }

    public function getTotal()
    {
        return $this->total;
    }
}