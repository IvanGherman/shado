<?php

namespace App\Http\Services;

class ImportFeedbackItem
{
    protected $errors = null;
    protected $row = null;

    public function __construct(int $row, array $errors)
    {
        $this->row = $row;
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getRow()
    {
        return $this->row;
    }

    public function getErrorsPerRow(int $row)
    {
        return $this->errors[$row];
    }
}