<?php

namespace App\Http\Services;

class ErrorItem
{
    protected $column = null;
    protected $row = null;
    protected $message = null;

    public function __construct(int $row, string $column, string $message)
    {
        $this->row = $row;
        $this->column = $column;
        $this->message = $message;
    }

    public function getColumn()
    {
        return $this->column;
    }

    public function getRow()
    {
        return $this->row;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function toString()
    {
        return sprintf('Error occured in row %s in column %s with message: %s',
            $this->getRow(), $this->getColumn(), $this->getMessage()
        );
    }
}