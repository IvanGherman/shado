<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\DB;

class ProductCounter
{
    protected $criteria = [];

    public function __construct(array $criteria = [])
    {
        $this->criteria = $criteria;
    }

    public static function exclude($criteria, $field)
    {
        if (!empty($criteria[$field])) {
            unset($criteria[$field]);
        }

        return $criteria;
    }

    public static function excludeAttribute($criteria, $field = null)
    {
        if (!empty($criteria['attribute']) && is_array($criteria['attribute'])) {
            foreach ($criteria['attribute'] as $id => $attributes) {
                if ($field === $id) {
                    unset($criteria['attribute'][$id]);
                }
            }
        }

        return $criteria;
    }

    public function getCountersFor($field = null)
    {
        $criteria = $this->criteria;

        $criteria = self::exclude($criteria, $field);

        $counterQuery = DB::table('products')->select('products.*');

        if (isset($criteria['promotion']) && in_array($criteria['promotion'], [0,1])) {
            $counterQuery->where('promotion', '=', $criteria['promotion']);
        }

        if (!empty($criteria['categories'])) {
            $counterQuery->join('product_categories', 'products.id', '=', 'product_categories.product_id');
            $counterQuery->whereIn('product_categories.category_id', $criteria['categories']);
        }

        if (!empty($criteria['colors'])) {
            $counterQuery->join('product_colors', 'products.id', '=', 'product_colors.product_id');
            $counterQuery->whereIn('product_colors.color_id', $criteria['colors']);
        }

        if (!empty($criteria['textures'])) {
            $counterQuery->join('product_textures', 'products.id', '=', 'product_textures.product_id');
            $counterQuery->whereIn('product_textures.texture_type_id', $criteria['textures']);
        }

        if (!empty($criteria['transparencies'])) {
            $counterQuery->whereIn('products.transparency_id', $criteria['transparencies']);
        }

        if (!empty($criteria['countries'])) {
            $counterQuery->join('product_countries', 'products.id', '=', 'product_countries.product_id');
            $counterQuery->whereIn('product_countries.country_id', $criteria['countries']);
        }

        if (!empty($criteria['tree_types'])) {
            $counterQuery->whereIn('products.tree_type_id', $criteria['tree_types']);
        }

        if (!empty($criteria['price_from'])) {
            $counterQuery->where(function ($counterQuery) use ($criteria) {
                $counterQuery->where('price', '>', $criteria['price_from'])
                    ->orWhere('promo_price', '>', $criteria['price_from']);
            });
        }

        if (!empty($criteria['price_to'])) {
            $counterQuery->where(function ($counterQuery) use ($criteria) {
                $counterQuery->where('price', '<', $criteria['price_to'])
                    ->orWhere('promo_price', '<', $criteria['price_to']);
            });

        }

        if (!empty($criteria['keyword'])) {
            $counterQuery->where('code', 'LIKE', $criteria['keyword']);
        }

        if (!empty($criteria['attribute']) && is_array($criteria['attribute'])) {
            foreach ($criteria['attribute'] as $id => $attributes) {
                $counterQuery->join('product_attribute_values as a'.$id, 'products.id', '=', 'a'.$id.'.product_id');
                $counterQuery->whereIn('a' .$id. '.attribute_value_id', $attributes);
            }
        }

        return $counterQuery;
    }

    public function getCountersForAttribute($idField = null)
    {
        $criteria = $this->criteria;

        $criteria = self::excludeAttribute($criteria, $idField);

        $counterQuery = DB::table('products')->select('products.*');

        if (isset($criteria['promotion']) && in_array($criteria['promotion'], [0,1])) {
            $counterQuery->where('promotion', '=', $criteria['promotion']);
        }

        if (!empty($criteria['categories'])) {
            $counterQuery->join('product_categories', 'products.id', '=', 'product_categories.product_id');
            $counterQuery->whereIn('product_categories.category_id', $criteria['categories']);
        }

        if (!empty($criteria['colors'])) {
            $counterQuery->join('product_colors', 'products.id', '=', 'product_colors.product_id');
            $counterQuery->whereIn('product_colors.color_id', $criteria['colors']);
        }

        if (!empty($criteria['textures'])) {
            $counterQuery->join('product_textures', 'products.id', '=', 'product_textures.product_id');
            $counterQuery->whereIn('product_textures.texture_type_id', $criteria['textures']);
        }

        if (!empty($criteria['transparencies'])) {
            $counterQuery->whereIn('products.transparency_id', $criteria['transparencies']);
        }

        if (!empty($criteria['countries'])) {
            $counterQuery->join('product_countries', 'products.id', '=', 'product_countries.product_id');
            $counterQuery->whereIn('product_countries.country_id', $criteria['countries']);
        }

        if (!empty($criteria['tree_types'])) {
            $counterQuery->whereIn('products.tree_type_id', $criteria['tree_types']);
        }

        if (!empty($criteria['price_from'])) {
            $counterQuery->where(function ($counterQuery) use ($criteria) {
                $counterQuery->where('price', '>', $criteria['price_from'])
                    ->orWhere('promo_price', '>', $criteria['price_from']);
            });
        }

        if (!empty($criteria['price_to'])) {
            $counterQuery->where(function ($counterQuery) use ($criteria) {
                $counterQuery->where('price', '<', $criteria['price_to'])
                    ->orWhere('promo_price', '<', $criteria['price_to']);
            });

        }

        if (!empty($criteria['keyword'])) {
            $counterQuery->where('code', 'LIKE', $criteria['keyword']);
        }

        if (!empty($criteria['attribute']) && is_array($criteria['attribute'])) {
            foreach ($criteria['attribute'] as $id => $attributes) {
                $counterQuery->join('product_attribute_values as a'.$id, 'products.id', '=', 'a'.$id.'.product_id');
                $counterQuery->whereIn('a' .$id. '.attribute_value_id', $attributes);
            }
        }

        return $counterQuery;
    }
}