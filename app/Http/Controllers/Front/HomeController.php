<?php

namespace App\Http\Controllers\Front;

use App\FAQ;
use App\Http\Requests\CallRequestForm;
use App\Http\Requests\ContactForm;
use App\Http\Requests\RequestMasterForm;
use App\Mail\CallRequestMail;
use App\Mail\ContactEmail;
use App\Mail\RequestMasterEmail;
use App\Product;
use App\Variables;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Http\Helpers\webus_help;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Jenssegers\Agent\Agent;

class HomeController extends Controller
{
    const HOMEPAGE = 'home_page';
    const HOMEPAGE_MOBILE = 'home_page_mobile';

    public function index()
    {
        $record = Pages::where('post_status', 1)->where('slug', self::HOMEPAGE)->get()->first();
        if($record == null){abort('404');}

        //set extra function for custom template
        $extra = array();
        $template_extra = app_path('Extra/'.$record->template.'.php');
        if(file_exists($template_extra)){
            require ($template_extra);
        }

        $og = new OpenGraph();
        $title = (!empty($record->meta_title) ? $record->meta_title : $record->name);

        return view('front.templates.'.$record->template, [
            'meta' => [
                'title' => $title,
                'page_name' => ProductController::metaTagPrefixAndTranslate($record->name),
                'description' => $record->meta_description,
                'keywords' => $record->meta_keywords
            ],
            'og' => $og->title($title)
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description($record->meta_description),
            'data' => $record,
            'post_type' => 'pages',
            'modules' => webus_help::get_metabox($record->id, 'pages', 'modules'),
            'extra' => $extra
        ]);
    }

    /**
     * @param ContactForm $contactForm
     * @return JsonResponse
     * @throws \Exception
     */
    public function contact(ContactForm $contactForm)
    {
        $post = $contactForm->request->all();

        $email = Variables::where('key', 'contact_email')->get();

        if (!$email || count($email) === 0) {
            throw new \Exception('Email is not set', 404);
        }
        $email = $email[0]['value'];

        try {
            Mail::to($email)->send(new ContactEmail($post));
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()]);
        }

        return new JsonResponse(['status' => 'OK']);
    }

    /**
     * @param ContactForm $contactForm
     * @return JsonResponse
     * @throws \Exception
     */
    public function review(ContactForm $contactForm)
    {
        $post = $contactForm->request->all();

        $email = Variables::where('key', 'review_email')->get();

        if (!$email || count($email) === 0) {
            throw new \Exception('Email is not set', 404);
        }
        $email = $email[0]['value'];

        try {
            Mail::to($email)->send(new ContactEmail($post));
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()]);
        }

        return new JsonResponse(['status' => 'OK']);
    }

    /**
     * @param RequestMasterForm $masterForm
     * @return JsonResponse
     * @throws \Exception
     */
    public function requestMaster(RequestMasterForm $masterForm)
    {
        $post = $masterForm->request->all();

        $email = Variables::where('key', 'request_master_email')->get();

        if (!$email || count($email) === 0) {
            throw new \Exception('Email is not set', 404);
        }
        $email = $email[0]['value'];

        try {
            $post['email'] = $email;
            Mail::to($email)->send(new RequestMasterEmail($post));
        } catch (\Exception $e) {
            $code = $e->getCode() > 0 ? $e->getCode() : 409;
            return new JsonResponse(['error' => $e->getMessage().$e->getFile().$e->getLine()], $code);
        }

        return new JsonResponse(['status' => 'OK']);
    }

    public function callRequest(CallRequestForm $callRequestForm)
    {
        $post = $callRequestForm->request->all();

        $email = Variables::where('key', 'call_request_email')->get();

        if (!$email || count($email) === 0) {
            throw new \Exception('Email is not set', 404);
        }
        $email = $email[0]['value'];

        if (empty($post['product_id'])) {
            return new JsonResponse(['error' => 'product_id is required']);
        }

        $product = Product::find($post['product_id']);
        if (!$product) {
            return new JsonResponse(['error' => 'Product not found']);
        }

        $locale = Config::get('app.locale');
        $post['title'] = \App\Http\Helpers\webus_help::translate_manual($locale, $product->title);

        try {
            $post['email'] = $email;
            Mail::to($email)->send(new CallRequestMail($post));
        } catch (\Exception $e) {
            $code = $e->getCode() > 0 ? $e->getCode() : 409;
            return new JsonResponse(['error' => $e->getMessage()], $code);
        }

        return new JsonResponse(['status' => 'OK']);
    }

    public function noExplorer()
    {
        $og = new OpenGraph();
        return view('front.errors.no_ie', [
            'meta' => ProductController::getEmptyMeta(),
            'og' => $og->title('')
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description('faq'),
        ]);
    }

    public function faq()
    {
        $faq = FAQ::where('post_status', 1)->get();

        $og = new OpenGraph();
        $title = 'faq';

        $meta = ProductController::prepareMeta('faq');

        $record = Pages::where('post_status', 1)->where('slug', 'faq')->get()->first();
        $modules = [];
        if ($record != null) {
            $modules = webus_help::get_metabox($record->id, 'pages', 'modules');
        }

        return view('front.pages.faq', [
            'meta' => $meta,
            'og' => $og->title($title)
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description('faq'),
            'data' => $faq,
            'post_type' => 'pages',
            'modules' => $modules,
        ]);
    }
}
