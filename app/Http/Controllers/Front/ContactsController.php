<?php

namespace App\Http\Controllers\Front;

use App\Contacts;
use App\Http\Helpers\webus_help;
use App\Pages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ChrisKonnertz\OpenGraph\OpenGraph;

class ContactsController extends Controller
{
    public function viewAll(Request $request)
    {
        $query = Contacts::where('post_status', 1);

        $contacts = $query->get();

        $meta = ProductController::prepareMeta('contacts');

        $page = Pages::where('slug', '=', 'contacts')->get()->first();
        $modules = [];
        if ($page) {
            $modules = webus_help::get_metabox($page->id, 'pages', 'modules');
        }

        $og = new OpenGraph();

        return view('front.contacts.view_all', [
            'meta' => $meta,
            'og' => $og->title($meta['title'])
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description($meta['description']),
            'contacts' => $contacts,
            'post_type' => 'pages',
            'modules' => $modules,
        ]);
    }
}
