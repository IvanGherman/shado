<?php

namespace App\Http\Controllers\Front;

use App\Category;

use App\CustomMenu;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    static function front(){
        $categories = Category::where('post_status', 1)->get()->toArray();
        $customMenu = CustomMenu::where('post_status', 1)->orderBy('position', 'ASC')->get();

        return view('front.partial.categories', [
            'categories' => $categories,
            'customMenu' => $customMenu,
        ]);
    }
}
