<?php

namespace App\Http\Controllers\Front;

use App\Http\Helpers\webus_help;
use App\News;
use App\Variables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NewsController extends Controller
{
    const LIMIT = 9;
    const MOBILE_LIMIT = 4;

    public function viewAll(Request $request)
    {
        $detect = new \Mobile_Detect();

        $offset = 0;

        $limit = self::LIMIT;
        if ($detect->isMobile()) {
            $limit = self::MOBILE_LIMIT;
        }

        $data = $request->query->all();

        if (!empty($data['offset'])) {
            $offset = $data['offset'];
        }

        $query = News::where('post_status', 1)
            ->orderBy('created_at', 'DESC')
            ->skip($offset)
            ->take($limit)
        ;

        $news = $query->get();

        $total = $query->count();

        if ($request->isXmlHttpRequest()) {
            return [
                'html' => view('front.news.partial_news', [
                    'data' => $news,
                ])->render(),
                'amount' => $news->count(),
            ];
        }

        $variables = Variables::all();

        $variableMap = [];
        foreach ($variables as $variable) {
            $variableMap[$variable->key] = $variable->value;
        }

        $og = new OpenGraph();

        $meta = ProductController::prepareMeta('news');

        return view('front.news.view_all', [
            'meta' => $meta,
            'og' => $og->title($meta['title'])
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description($meta['description']),
            'data' => $news,
            'post_type' => 'pages',
            'variables' => $variableMap,
            'total' => $total,
            'limit' => $limit,
            'amount' => $news->count(),
        ]);
    }

    public function view(Request $request, string $slug)
    {
        $news = News::where('post_status', 1)
            ->where('slug', $slug)
            ->first()
        ;

        if (!$news) {
            throw new NotFoundHttpException('News not found');
        }

        $variables = Variables::all();

        $variableMap = [];
        foreach ($variables as $variable) {
            $variableMap[$variable->key] = $variable->value;
        }

        $og = new OpenGraph();

        $meta = [
            'description' => ProductController::metaTagPrefixAndTranslate($news->meta_description),
            'keywords' => ProductController::metaTagPrefixAndTranslate($news->meta_keywords),
            'title' => ProductController::metaTagPrefixAndTranslate($news->title),
            'page_name' => ProductController::metaTagPrefixAndTranslate($news->title),
        ];

        return view('front.news.view', [
            'meta' => $meta,
            'og' => $og->title($meta['title'])
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description($meta['description']),
            'news' => $news,
            'post_type' => 'pages',
            'variables' => $variableMap,
            'modules' => webus_help::get_metabox($news->id, 'news', 'modules'),
        ]);
    }
}
