<?php

namespace App\Http\Controllers\Front;

use App\Attribute;
use App\Category;
use App\Color;
use App\Country;
use App\Http\Helpers\webus_help;
use App\Http\Services\ProductCounter;
use App\News;
use App\Pages;
use App\Product;
use App\Texture;
use App\Transparency;
use App\TreeType;
use App\Variables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductController extends Controller
{
    public function viewAll(Request $request)
    {
        $attributeFilters = Attribute::where('show_in_filter', 1)->get();

        /** @var RELATIONS WORK BUT DID NOT FIND SUBQUERY $query */
        $query = Product::where('post_status', '=', 1);
        /** @var SUBQUERY WORKS BUT NOT RELATIONS :( $query */
        /** @var NOT BEST WAY -> CREATE BOTH + BOTH CONDITIONS ADD $query */
        $counterQuery = DB::table('products')->select('products.*');

        $data = $request->query->all();
        $criteria = [];

        if (!empty($data['page'])) {
            unset($data['page']);
        }

        $sort = 'position';
        $order = 'ASC';
        if (!empty($data['sort_by']) && in_array($data['sort_by'], ['order_in_catalog'])) {
            $sort = $data['sort_by'];
            unset($data['sort_by']);
        }

        if (!empty($data['order']) && in_array($data['order'], ['ASC', 'DESC'])) {
            $order = $data['order'];
            unset($data['order']);
        }

        $allowedLimits = [20,40,60];
        $limit = array_first($allowedLimits);
        if (!empty($data['limit'])) {
            $limit = (int) $data['limit'];
            unset($data['limit']);
        }

        if (!empty($data['show_in_catalog'])) {
            $criteria['show_in_catalog'] = $data['show_in_catalog'];
            unset($data['show_in_catalog']);
        }

        if (!empty($data['promotion'])) {
            $criteria['promotion'] = 1;
            unset($data['promotion']);
        }

        if (!empty($data['keyword'])) {
            $criteria['keyword'] = $data['keyword'];
            unset($data['keyword']);
        }

        if (!empty($data['price_from'])) {
            $criteria['price_from'] = (int) $data['price_from'];
        }

        if (!empty($data['price_to'])) {
            $criteria['price_to'] = (int) $data['price_to'];
        }

        foreach ($data as $title => $items) {
            if ($title === 'attribute') {
                $subItems = [];
                foreach ($items as $key => $item) {
                    $subItems[$key] = array_keys($item);
                }
                $criteria[$title] = $subItems;
            } elseif (in_array($title, ['categories', 'textures', 'countries', 'colors', 'transparencies', 'tree_types'])) {
                $criteria[$title] = array_keys($items);
            }
        }

        if (isset($criteria['promotion']) && in_array($criteria['promotion'], [0,1])) {
            $query->where('promotion', '=', $criteria['promotion']);
        }

        if (!empty($criteria['categories'])) {
            $query->join('product_categories', 'products.id', '=', 'product_categories.product_id');
            $query->whereIn('product_categories.category_id', $criteria['categories']);
        }

        if (!empty($criteria['colors'])) {
            $query->join('product_colors', 'products.id', '=', 'product_colors.product_id');
            $query->whereIn('product_colors.color_id', $criteria['colors']);
        }

        if (!empty($criteria['textures'])) {
            $query->join('product_textures', 'products.id', '=', 'product_textures.product_id');
            $query->whereIn('product_textures.texture_type_id', $criteria['textures']);
        }

        if (!empty($criteria['transparencies'])) {
            $query->whereIn('products.transparency_id', $criteria['transparencies']);
        }

        if (!empty($criteria['countries'])) {
            $query->join('product_countries', 'products.id', '=', 'product_countries.product_id');
            $query->whereIn('product_countries.country_id', $criteria['countries']);
        }

        if (!empty($criteria['tree_types'])) {
            $query->whereIn('products.tree_type_id', $criteria['tree_types']);
        }

        if (!empty($criteria['price_from'])) {
            $query->where(function ($query) use ($criteria) {
                $query->where('price', '>', $criteria['price_from']);
                $query->orWhere('promo_price', '>', $criteria['price_from']);
            });
        }

        if (!empty($criteria['price_to'])) {
            $query->where(function ($query) use ($criteria) {
                $query->where('price', '<', $criteria['price_to']);
                $query->orWhere('promo_price', '<', $criteria['price_to']);
            });
        }

        if (!empty($criteria['keyword'])) {
            $query->where('code', 'LIKE', $criteria['keyword']);
        }

        if (!empty($criteria['show_in_catalog'])) {
            $query->where('show_in_catalog', '=', $criteria['show_in_catalog']);
        }

        if (!empty($criteria['attribute']) && is_array($criteria['attribute'])) {
            foreach ($criteria['attribute'] as $id => $attributes) {
                $query->join('product_attribute_values as a'.$id, 'products.id', '=', 'a'.$id.'.product_id');
                $query->whereIn('a' .$id. '.attribute_value_id', $attributes);
            }
        }

        $productCounter = new ProductCounter($criteria);
        $counterQuery = $productCounter->getCountersFor('tree_types');

        /** COUNTERS */
        $treeTypeCounter = DB::table( DB::raw("({$counterQuery->toSql()}) AS p") )
            ->mergeBindings($counterQuery)
            ->selectRaw('COUNT(tree_type_id) AS amount, tree_type_id as id')
            ->groupBy('tree_type_id')
            ->get();

        $counterQuery = $productCounter->getCountersFor('categories');

        $categoriesCounter = DB::table( DB::raw("({$counterQuery->toSql()}) AS p") )
            ->mergeBindings($counterQuery)
            ->join('product_categories', 'p.id', '=', 'product_categories.product_id')
            ->selectRaw('COUNT(product_categories.category_id) AS amount, product_categories.category_id as id')
            ->groupBy('product_categories.category_id')
            ->get();

        $counterQuery = $productCounter->getCountersFor('colors');

        $colorsCounter = DB::table( DB::raw("({$counterQuery->toSql()}) AS p") )
            ->mergeBindings($counterQuery)
            ->join('product_colors', 'p.id', '=', 'product_colors.product_id')
            ->selectRaw('COUNT(product_colors.color_id) AS amount, product_colors.color_id as id')
            ->groupBy('product_colors.color_id')
            ->get();

        $counterQuery = $productCounter->getCountersFor('textures');

        $texturesCounter = DB::table( DB::raw("({$counterQuery->toSql()}) AS p") )
            ->mergeBindings($counterQuery)
            ->join('product_textures', 'p.id', '=', 'product_textures.product_id')
            ->selectRaw('COUNT(product_textures.texture_type_id) AS amount, product_textures.texture_type_id as id')
            ->groupBy('product_textures.texture_type_id')
            ->get();

        $counterQuery = $productCounter->getCountersFor('transparencies');

        $transparencyCounter = DB::table( DB::raw("({$counterQuery->toSql()}) AS p") )
            ->mergeBindings($counterQuery)
            ->selectRaw('COUNT(id) AS amount, transparency_id as id')
            ->groupBy('transparency_id')
            ->get();

        $counterQuery = $productCounter->getCountersFor('countries');

        $countriesCounter = DB::table( DB::raw("({$counterQuery->toSql()}) AS p") )
            ->mergeBindings($counterQuery)
            ->join('product_countries', 'p.id', '=', 'product_countries.product_id')
            ->selectRaw('COUNT(product_countries.country_id) AS amount, product_countries.country_id AS id')
            ->groupBy('product_countries.country_id')
            ->get();

        $counterQuery = $productCounter->getCountersForAttribute();
        $attributesCounterDefault = DB::table( DB::raw("({$counterQuery->toSql()}) AS p") )
            ->mergeBindings($counterQuery)
            ->join('product_attribute_values', 'p.id', '=', 'product_attribute_values.product_id')
            ->selectRaw('COUNT(product_attribute_values.attribute_value_id) AS amount, product_attribute_values.attribute_id, product_attribute_values.attribute_value_id')
            ->groupBy('product_attribute_values.attribute_value_id', 'product_attribute_values.attribute_id')
            ->get()
            ->toArray()
        ;

        $attributesCounter = [];
        foreach ($attributesCounterDefault as $counter) {
            $attributesCounter[$counter->attribute_value_id] = $counter;
        }

        if (!empty($criteria['attribute'])) {
            foreach ($criteria['attribute'] as $id => $values) {
                $counterQuery = $productCounter->getCountersForAttribute($id);

                $localCounter = DB::table( DB::raw("({$counterQuery->toSql()}) AS p") )
                    ->mergeBindings($counterQuery)
                    ->join('product_attribute_values', 'p.id', '=', 'product_attribute_values.product_id')
                    ->selectRaw('COUNT(product_attribute_values.attribute_value_id) AS amount, product_attribute_values.attribute_id, product_attribute_values.attribute_value_id')
                    ->groupBy('product_attribute_values.attribute_value_id', 'product_attribute_values.attribute_id')
                    ->get();

                foreach ($localCounter as $counter) {
                    if ($counter->attribute_id == $id) {
                        $attributesCounter[$counter->attribute_value_id] = $counter;
                    }
                }
            }
        }

        if (!empty($attributesCounter)) {
            $attributesCounter = array_values($attributesCounter);
        }

        $query->orderBy($sort, $order);

        $ids = $query->select('products.id')->get();

        $newIds = [];
        foreach ($ids as $id) {
            $newIds[$id->id] = $id->id;
        }

        $newQuery = Product::where('post_status', '=', 1)->whereIn('id', array_values($newIds));
        $newQuery->groupBy('products.id');
        $newQuery->orderBy($sort, $order);

        $products = $newQuery->paginate($limit);

        $counters = [
            'treeType' => $treeTypeCounter->toArray(),
            'categories' => $categoriesCounter->toArray(),
            'colors' => $colorsCounter->toArray(),
            'textures' => $texturesCounter->toArray(),
            'transparencies' => $transparencyCounter->toArray(),
            'countries' => $countriesCounter->toArray(),
            'attributes' => $attributesCounter,
        ];

        if ($request->isXmlHttpRequest()) {
            return [
                'counters' => $counters,
                'html' => view('front.pages.products_partial', [
                    'products' => $products,
                ])->render(),
                'modals' => view('front.pages.products_modals', [
                    'products' => $products,
                ])->render(),
                'pagination' => view('front.pages.products_pagination', [
                    'products' => $products,
                ])->render(),
            ];
        }

        $og = new OpenGraph();

        $categories = Category::all();
        $colors = Color::all();
        $textures = Texture::all();
        $transparencies = Transparency::all();
        $countries = Country::all();
        $treeTypes = TreeType::all();

        $category = null;
        $meta = self::getEmptyMeta();
        if (!empty($criteria['categories']) && count($criteria['categories']) === 1) {
            $category = Category::find($criteria['categories'][0]);

            $meta = [
                'title' => self::metaTagPrefixAndTranslate($category->meta_title),
                'description' => self::metaTagPrefixAndTranslate($category->description),
                'keywords' => self::metaTagPrefixAndTranslate($category->keywords),
            ];

            $title = self::metaTagPrefixAndTranslate($category->title);
            $meta['page_name'] = $title;
        } else {
            $meta['page_name'] = 'Shado | Catalog';
        }

        $detect = new \Mobile_Detect();

        return view('front.pages.products', [
            'meta' => $meta,
            'og' => $og->title($meta['title'])
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description($meta['description']),
            'post_type' => 'pages',
            'categoryEntity' => $category,
            'tree_types' => $treeTypes,
            'countries' => $countries,
            'transparencies' => $transparencies,
            'textures' => $textures,
            'categories' => $categories,
            'colors' => $colors,
            'limit' => $limit,
            'allowedLimits' => $allowedLimits,
            'products' => $products,
            'counters' => $counters,
            'attribute_filters' => $attributeFilters,
            'filters_predefined' => $criteria,
            'is_mobile' => $detect->isMobile(),
        ]);
    }

    public static function metaTagPrefixAndTranslate($title)
    {
        $locale = Config::get('app.locale');
        return 'Shado | '.\App\Http\Helpers\webus_help::translate_manual($locale, $title);
    }

    public static function translate($title)
    {
        $locale = Config::get('app.locale');
        return \App\Http\Helpers\webus_help::translate_manual($locale, $title);
    }

    public function view(Request $request, int $id)
    {
        $product = Product::where('post_status', 1)
            ->where('id', $id)
            ->first()
        ;

        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        $byModel = Product::where('model', $product->model)->get();

        $common = json_decode($product->common_p);
        if (!empty($common)) {
            $common = Product::find($common);
        } else {
            $common = false;
        }


        $popular = json_decode($product->popular_p);
        if (!empty($popular)) {
            $popular = Product::find($popular);
        } else {
            $popular = false;
        }

        $variables = Variables::all();

        $variableMap = [];
        foreach ($variables as $variable) {
            $variableMap[$variable->key] = $variable->value;
        }

        $og = new OpenGraph();

        $page = Pages::where('slug', '=', 'product')->get()->first();
        $modules = webus_help::get_metabox($page->id, 'pages', 'modules');

        $meta = [
            'title' => self::metaTagPrefixAndTranslate($product->meta_title),
            'description' => self::metaTagPrefixAndTranslate($product->meta_decription),
            'keywords' => self::metaTagPrefixAndTranslate($product->meta_keywords),
        ];
        $meta['page_name'] = self::metaTagPrefixAndTranslate($product->title);

        return view('front.pages.product', [
            'meta' => $meta,
            'og' => $og->title($meta['title'])
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description($meta['description']),
            'product' => $product,
            'by_model' => $byModel,
            'common' => $common,
            'popular' => $popular,
            'post_type' => 'pages',
            'variables' => $variableMap,
            'modules' => $modules,
        ]);
    }

    public static function prepareMeta($slug)
    {
        $meta = self::getEmptyMeta();
        $page = Pages::where('slug', '=', $slug)->get()->first();
        if ($page) {
            $meta = [
                'page_name' => self::metaTagPrefixAndTranslate($page->name),
                'title' => self::metaTagPrefixAndTranslate($page->meta_title),
                'keywords' => self::metaTagPrefixAndTranslate($page->meta_keywords),
                'description' => self::metaTagPrefixAndTranslate($page->meta_description)
            ];
        }

        return $meta;
    }

    public static function getEmptyMeta()
    {
        return [
            'title' => '',
            'keywords' => '',
            'description' => '',
        ];
    }
}
