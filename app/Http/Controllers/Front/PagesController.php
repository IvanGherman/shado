<?php

namespace App\Http\Controllers\Front;

use App\Variables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pages;
use App\Http\Helpers\webus_help;
use ChrisKonnertz\OpenGraph\OpenGraph;
class PagesController extends Controller
{
    //route for this function : Route::get('{slug}', 'FrontPageController@index');
    public function index(Request $request, $slug)
    {
        $record = Pages::where('post_status', 1)->where('slug', $slug)->where('template', '<>', 'home')->first();
        if($record == null){abort('404');}

        //set extra function for custom template
        $extra = array();
        $template_extra = app_path('Extra/'.$record->template.'.php');
        if(file_exists($template_extra)){
            require ($template_extra);
        }

        $og = new OpenGraph();
        $title = (!empty($record->meta_title) ? $record->meta_title : $record->name);
        $title = ProductController::metaTagPrefixAndTranslate($title);

        $variables = Variables::all();

        $variableMap = [];
        foreach ($variables as $variable) {
            $variableMap[$variable->key] = $variable->value;
        }

        return view('front.templates.'.$record->template, [
            'meta' => [
                'page_name' => ProductController::metaTagPrefixAndTranslate($record->name),
                'title' => $title,
                'description' => ProductController::metaTagPrefixAndTranslate($record->meta_description),
                'keywords' => ProductController::metaTagPrefixAndTranslate($record->meta_keywords),
            ],
            'og' => $og->title($title)
                ->type('site')
                ->image('http://example.org/apple.jpg')
                ->description($record->meta_description),
            'data' => $record,
            'post_type' => 'pages',
            'modules' => webus_help::get_metabox($record->id, 'pages', 'modules'),
            'extra' => $extra,
            'variables' => $variableMap,
        ]);
    }
}
