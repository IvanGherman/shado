<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Http\Controllers\Webus\WebusAdminController;

class SelectedProductsController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\SelectedProduct'; //model
        $this->slug = 'selected-products'; //id controller
        $this->title = 'Selected products'; //title controller
        $this->icon = ''; //icon page
        $this->active_modules = false;
        $this->template_post = 'positions'; //template post type (default or positions)
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = [
            'name' => 'translatable',
        ]; //in list template

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'product_id' => [
                'title' => 'Product',
                'html_type' => 'select',
                'options' => self::makeMap(Product::all()),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'content',
            ],

            'color' => [
                'title' => 'Color',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'image' => [
                'title' => 'Image',
                'html_type' => 'image_input',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],


        );

        //meta_boxes
        $this->meta_box = false;
    }
}
