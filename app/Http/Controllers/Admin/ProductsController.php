<?php

namespace App\Http\Controllers\Admin;

use App\Attribute;
use App\AttributeValue;
use App\Category;
use App\Color;
use App\CommonProduct;
use App\Country;
use App\Http\Services\ErrorItem;
use App\Http\Services\ImportManager;
use App\Http\Services\ImportMapper\ProductMapper;
use App\PopularProduct;
use App\Product;
use App\ProductAttributeValue;
use App\Texture;
use App\Transparency;
use App\TreeType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusAdminController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductsController extends WebusAdminController
{
    protected $relatedEntities = [
        'countries',
        'categories',
        'colors',
        'textures',
        'common_products',
        'popular_products',
    ];

    protected static $attributes = [];

    protected $products = [];

    public function __construct()
    {
        $this->model_name = 'App\Product'; //model
        $this->slug = 'products'; //id controller
        $this->title = 'Products'; //title controller
        $this->icon = ''; //icon page
        $this->active_modules = false;
        $this->template_post = 'positions'; //template post type (default or positions)
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = [
            'name' => 'translatable',
            'show_in_catalog' => 'translatable',
        ]; //in list template

        $trees = TreeType::all();
        $treeMap = self::makeMap($trees);

        $categories = Category::all();
        $this->products = $products = Product::all();

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'common_p' => [
                'title' => 'Common products',
                'html_type' => 'select',
                'options' => self::makeMap($products),
                'multiple' => true,
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'content',
//                'is_entity' => true,
            ],

            'popular_p' => [
                'title' => 'Popular products',
                'html_type' => 'select',
                'options' => self::makeMap($products),
                'multiple' => true,
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'content',
//                'is_entity' => true,
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'description' => [
                'title' => 'Description',
                'html_type' => 'body',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'model' => [
                'title' => 'Model',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'code' => [
                'title' => 'Code',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'production_duration' => [
                'title' => 'Production duration',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'tree_type_id' => [
                'title' => 'Tree type',
                'html_type' => 'select',
                'options' => $treeMap,
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'categories' => [
                'title' => 'Categories',
                'html_type' => 'select',
                'options' => self::makeMap($categories),
                'multiple' => true,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'content',
                'is_entity' => true,
            ],

            'textures' => [
                'title' => 'Textures',
                'html_type' => 'select',
                'options' => self::makeMap(Texture::all()),
                'multiple' => true,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'content',
                'is_entity' => true,
            ],

            'colors' => [
                'title' => 'Colors',
                'html_type' => 'select',
                'options' => self::makeMap(Color::all()),
                'multiple' => true,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'content',
                'is_entity' => true,
            ],

            'countries' => [
                'title' => 'Countries',
                'html_type' => 'select',
                'options' => self::makeMap(Country::all()),
                'multiple' => true,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'content',
                'is_entity' => true,
            ],

            'transparency_id' => [
                'title' => 'Transparency',
                'html_type' => 'select',
                'options' => self::makeMap(Transparency::all()),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'content'
            ],

            'position' => [
                'title' => 'Position',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'order_in_catalog' => [
                'title' => 'Position in catalog',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'price' => [
                'title' => 'Price',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'promo_price' => [
                'title' => 'Promo price',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'model_image' => [
                'title' => 'Model image',
                'html_type' => 'image_input',
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'images' => [
                'title' => 'Images',
                'html_type' => 'repeatable',
                'start_with' => 0,
                'max' => 50,
                'options' => array(
                    'image' => array(
                        'title' => 'Image',
                        'html_type' => 'image_input',
                        'validate' => false,
                        'translatable' => false,
                        'custom_class' => 'col-md-12'
                    )
                ),
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'metabox' => true,
                'position' => 'content'
            ],

            'promotion' => [
                'title' => 'Promotion',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'show_in_catalog' => [
                'title' => 'Show in catalog module',
                'html_type' => 'select',
                'options' => array(
                    1 => 'Enabled',
                    0 => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'meta_title' => [
                'title' => 'Meta title',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'meta_keywords' => [
                'title' => 'Meta keywords',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'meta_description' => [
                'title' => 'Meta description',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],


        );

        self::$attributes = $attributes = Attribute::all();

        foreach ($attributes as $attribute) {
            $this->form_fields['attribute_'.$attribute->id] = [
                'title' => $attribute->name,
                'html_type' => 'select',
                'options' => self::makeMap($attribute->values),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'content',
                'is_entity' => true,
                'attribute' => $attribute->id,
            ];
        }
        //meta_boxes
        $this->meta_box = false;
    }

    public function index(Request $request)
    {
        $model_name = $this->model_name;
        $per_page = !empty($this->per_page) ? $this->per_page : 25;

        $search_field = 'name';
        if (isset($this->search)) {
            $search_field = $this->search;
        }

        $search_key = false;
        if (isset($request->search)) {
            $search_key = $request->search;
        }

        $order_by = 'created_at,desc';
        if (isset($this->orderby) && $this->orderby != null){
            $order_by = $this->orderby;
        }

        $list = $model_name::orderBy(explode(',',$order_by)[0], explode(',',$order_by)[1]);

        if (!empty($search_key)) {
            $list = $list->where($search_field, 'like', '%'.$search_key.'%');
        }

        if ($request->query->has('show_in_catalog')) {
            $showInCatalog = $request->query->get('show_in_catalog');
            $showInCatalog = $showInCatalog == 'on' ? 1 : 0;

            if ($showInCatalog) {
                $list->where('show_in_catalog', '=', $showInCatalog);
            }
        }

        $list = $list->paginate($per_page);

        $icon_page = '';
        if (isset($this->icon)) {
            $icon_page = $this->icon;
        }

        $data = [
            'title' => $this->title,
            'slug' => $this->slug,
            'url_create' => route($this->slug.'.create'),
            'url_edit' => 'admin/'.$this->slug.'/edit/',
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_delete_mass' => 'admin/'.$this->slug.'/delete_mass/',
            'url_build' => 'admin/'.$this->slug.'/build/',
            'rows' => $this->list_field,
            'list' => $list,
            'icon' => $icon_page,
        ];

        $view = 'webus.standart.index';

        if (view()->exists('webus.'.$this->slug.'.index')) {
            $view = 'webus.'.$this->slug.'.index';
        }

        return view($view, ['data' => $data]);
    }

    public function store(Request $request)
    {
        $attributesToSave = [];

        $validate_array = [];
        foreach ($this->form_fields as $key => $field){
            if ($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        $this->validate($request, $validate_array);

        $model_name = $this->model_name;
        $post = new $model_name;
        $data_taxonomies = [];
        /** EXAMPLE OF TRANSACTION */
        DB::beginTransaction();
        try {
            $laterFields = [];
            foreach ($this->form_fields as $key => $field){
                if ($field['html_type'] != 'info'){
                    //translatable
                    if ($field['translatable']){
                        $convert_item_to_translatable = '';
                        foreach ($request['translatable'][$key] as $key_translatable => $item_translatable){
                            if (!empty($item_translatable)){
                                $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                            }
                        }
                        $post->$key = stripslashes($convert_item_to_translatable);
                    } elseif ($field['html_type'] == 'file') {
                        if (!empty($request->file($key))) {
                            $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');
                            $post->$key = preg_replace('/^public\//', '', $path);
                        }
                    } elseif ($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input') {
                        $post->$key = parse_url($request->$key, PHP_URL_PATH);
                    } elseif ($field['html_type'] == 'password') {
                        $post->$key = bcrypt($request->$key);
                    } elseif ($field['html_type'] == 'select_taxonomy') {
                        $data_taxonomies[$key] = $request->$key;
                    } elseif ($field['html_type'] == 'select' && strpos($key, 'attribute_') !== false) {
                        $attributesToSave[$key] = $request->$key;
                    } elseif ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                        if (in_array($key, $this->relatedEntities)) {
                            $laterFields[$key] = $request->$key;
                        } else {
                            $post->$key = null;
                            if (!empty($request->$key)) {
                                $post->$key = json_encode($request->$key);
                            }
                        }
                    } elseif ($key == 'created_at') {
                        if (!empty($request->created_at)) {
                            $post->$key = Carbon::parse($request->created_at)->format('Y-m-d H:i:s');
                        } else {
                            $post->$key = Carbon::now();
                        }
                    } elseif ($key === 'images') {
                        $post->$key = json_encode($request->$key);
                    } else {
                        $post->$key = $request->$key;
                    }
                }
            }

            $post->save();

            foreach ($laterFields as $key => $item) {
                if ($key === 'countries') {
                    $countries = Country::find($request->$key);
                    $post->countries()->attach($countries);
                } elseif ($key === 'colors') {
                    $colors = Color::find($request->$key);
                    $post->colors()->attach($colors);
                } elseif ($key === 'textures') {
                    $textures = Texture::find($request->$key);
                    $post->textures()->attach($textures);
                } elseif ($key === 'categories') {
                    $categories = Category::find($request->$key);
                    $post->categories()->attach($categories);
                } elseif ($key === 'common_products') {
                    $products = Product::find($request->$key);
                    $post->common_products()->attach($products);
                } elseif ($key === 'popular_products') {
                    $products = Product::find($request->$key);
                    $post->popular_products()->attach($products);
                }
            }

            $this->saveAttributeValues($attributesToSave, $post);

            DB::commit();
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\Session::flash('flash_message',$e->getMessage(). ' '.$e->getFile(). ' '.$e->getLine());
            DB::rollback();

            return redirect(route($this->slug.'.products'));
        }

        //attach terms taxonomy
        if (isset($data_taxonomies) && $data_taxonomies != null) {
            $model_taxonomy = $this->model_taxonomy;
            foreach ($data_taxonomies as $name_taxonomy=>$terms){
                $model_taxonomy::atach_terms_to_post($name_taxonomy, $terms, $post->id, $this->slug);
            }
        }

        //MetaBox
        $metabox = $this->meta_box;

        //Enabled Modules Controlle
        if (isset($this->active_modules) && $this->active_modules){
            $metabox['modules'] = self::set_modules();
        }

        if (isset($metabox) && $metabox){
            $meta_requests = $request->metabox;
            if ($meta_requests){
                $deleted_all_metabox = true;
                webus_help::delete_metabox($post->id, $this->slug);

                foreach ($meta_requests as $key => $meta) {
                    if (isset($request['meta_translatable'][$key])) {
                        $convert_item_to_translatable = '';
                        foreach ($request['meta_translatable'][$key] as $key_translatable=>$item_translatable){
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }

                        $meta_value = stripslashes($convert_item_to_translatable);
                        webus_help::insert_metabox($post->id, $this->slug, $key, $meta_value);
                    } else {
                        webus_help::insert_metabox($post->id, $this->slug, $key, $meta);
                    }
                }
            }

            //only for repeatable
            if (!isset($deleted_all_metabox)){
                webus_help::delete_metabox($post->id, $this->slug);
            }

            foreach ($metabox as $meta_key => $meta_fields) {
                if ($meta_fields['html_type'] == 'repeatable') {
                    if (isset($request->$meta_key)) {

                        $meta_value = [];
                        foreach ($request->$meta_key as $key => $value){
                            $meta_value[] = $value;
                        }

                        webus_help::insert_metabox($post->id, $this->slug, $meta_key, $meta_value);
                    }
                }
            }

        }

        \Illuminate\Support\Facades\Session::flash('flash_message','Successfully added.');

        return redirect(route($this->slug.'.edit', ['id' => $post->id]));
    }

    protected function saveAttributeValues($attributesToSave, $post)
    {
        $values = AttributeValue::all();
        ProductAttributeValue::where('product_id', '=', $post->id)->delete();
        foreach ($attributesToSave as $key => $item) {
            $attributeId = str_replace('attribute_', '', $key);
            foreach ($values as $value) {
                if ($value->attribute_id == $attributeId && $item == $value->id) {
                    $attrValue = new ProductAttributeValue();
                    $attrValue->attribute_id = $attributeId;
                    $attrValue->product_id = $post->id;
                    $attrValue->attribute_value_id = $value->id;
                    $attrValue->save();
                }
            }
        }
    }

    public function edit(Request $request, $id)
    {
        $model_name = $this->model_name;
        $model = $model_name::findOrFail($id);

        foreach ($this->form_fields as $key => $field){
            //get taxonomy default values
            if ($field['html_type'] == 'select_taxonomy'){
                $model_taxonomy = $this->model_taxonomy;
                if (isset($field['multiple']) && $field['multiple'] == false) {
                    $model[$key] = key($model_taxonomy::get_post_terms($key, $id, $this->slug, true));
                } elseif (isset($field['multiple']) && $field['multiple'] == true) {
                    $model['selected_'.$key] = $model_taxonomy::get_post_terms($key, $id, $this->slug, true);
                }
            }

            if (in_array($key, $this->relatedEntities)) {
//                $items = $model->$key->toArray();
            } else {
                //get select & multiple from field database
                if ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                    if ($model->$key) {
                        $model['selected_'.$key] = json_decode($model->$key);

                        if (in_array($key, ['common_p', 'popular_p'])) {
                            $items = json_decode($model->$key);
                            $newItems = [];
                            foreach ($items as $item) {
                                $newItems[$item] = $item;
                            }
                            $model['selected_'.$key] = $newItems;
                        }
                    } else {
                        $model['selected_'.$key] = [];
                    }
                }
            }
        }

        //metaboxes
        $metabox = false;

        if (isset($this->meta_box) && $this->meta_box) {
            $metabox = $this->meta_box;
            $current_route_id = $id;
            $current_template = (isset($model->template) ? $model->template : '');
            $excludeFields = [];
            foreach ($this->meta_box as $key => $field){
                //prepare exclude
                if (!empty($field['show_on']) && !in_array($current_route_id, $field['show_on']) && !in_array($current_template, $field['show_on'])){
                    $excludeFields[] = $key;
                }

                if (isset($field['multiple']) && $field['multiple'] == true) { //for select multiple
                    $model['selected_'.$key] = webus_help::get_metabox($id, $this->slug, $key);
                } else {
                    $model[$key] = webus_help::get_metabox($id, $this->slug, $key);
                }
            }

            if (!empty($excludeFields)) {
                foreach ($excludeFields as $exludeField){
                    unset($metabox[$exludeField]);
                }
            }
        }
        //Enabled Modules Controller
        if (isset($this->active_modules) && $this->active_modules){
            $metabox['modules'] = self::set_modules();
            $model['modules'] = webus_help::get_metabox($id, $this->slug, 'modules');
        }

        $icon_page = '';
        if (isset($this->icon)){
            $icon_page = $this->icon;
        }

        $data = [
            'title' => 'Edit '.str_singular($this->title),
            'url_create' => route($this->slug.'.create'),
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_build' => 'admin/'.$this->slug.'/menu/',
            'form_action' => route($this->slug.'.update', ['id' => $id]),
            'rows' => $this->form_fields,
            'metabox' => $metabox,
            'icon' => $icon_page
        ];

        $view = 'webus.standart.add-edit';

        if (isset($this->template_post) && $this->template_post == 'positions') {
            $view = 'webus.standart.add-edit-positions';
        }

        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }

        return view($view, ['data' => $data, 'model' => $model]);
    }


    public function update(Request $request, $id)
    {
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);
        $attributesToSave = [];

        $validate_array = [];
        foreach ($this->form_fields as $key => $field){
            if ($field['validate']) {
                if ($key == 'slug' && $post->slug !== $request->slug) {
                    $validate_array[$key] = $field['validate'];
                }
            }
        }
        $this->validate($request, $validate_array);

        DB::beginTransaction();
        try {
            $laterFields = [];
            foreach ($this->form_fields as $key => $field) {
                if ($field['html_type'] != 'info') {
                    //translatable
                    if ($field['translatable']) {

                        $convert_item_to_translatable = '';
                        foreach ($request['translatable'][$key] as $key_translatable => $item_translatable) {
                            if (!empty($item_translatable)) {
                                $convert_item_to_translatable .= '[:' . $key_translatable . ']' . $item_translatable;
                            }
                        }

                        $post->$key = stripslashes($convert_item_to_translatable);

                    } elseif ($field['html_type'] == 'file') {

                        if (!empty($request->file($key))) {
                            $path = $request->file($key)->store($this->slug . '/' . date('Y-m'), 'public');
                            $post->$key = preg_replace('/^public\//', '', $path);
                        }

                        //if is checked for remove from db this file url
                        if ($request['remove'][$key] == 'on') {
                            $post->$key = '';
                        }
                    } elseif ($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input') {
                        $post->$key = parse_url($request->$key, PHP_URL_PATH);
                    } elseif ($field['html_type'] == 'password') {
                        if ($request->$key) {
                            $post->$key = bcrypt($request->$key);
                        }
                    } elseif ($field['html_type'] == 'select_taxonomy') {
                        $data_taxonomies[$key] = $request->$key;
                    } elseif ($field['html_type'] == 'select' && strpos($key, 'attribute_') !== false) {
                        $attributesToSave[$key] = $request->$key;
                    } elseif ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                        if (in_array($key, $this->relatedEntities)) {
                            $laterFields[$key] = $request->$key;
                        } else {
                            $post->$key = null;
                            if (!empty($request->$key)) {
                                $post->$key = json_encode($request->$key);
                            }
                        }
                    } elseif ($key == 'created_at') {
                        $post->$key = Carbon::parse($request->created_at)->format('Y-m-d H:i');
                    } elseif ($key === 'images') {
                        $post->$key = json_encode($request->$key);
                    } else {
                        $post->$key = $request->$key;
                    }
                }

            }

            $post->save();

            foreach ($laterFields as $key => $item) {
                if ($key === 'countries') {
                    $countries = Country::find($request->$key);
                    $post->countries()->sync($countries);
                } elseif ($key === 'colors') {
                    $colors = Color::find($request->$key);
                    $post->colors()->sync($colors);
                } elseif ($key === 'textures') {
                    $textures = Texture::find($request->$key);
                    $post->textures()->sync($textures);
                } elseif ($key === 'categories') {
                    $categories = Category::find($request->$key);
                    $post->categories()->sync($categories);
                } elseif ($key === 'common_products') {
                    $products = Product::find($request->$key);
                    $post->common_products()->sync($products);
                } elseif ($key === 'popular_products') {
                    $products = Product::find($request->$key);
                    $post->popular_products()->sync($products);
                }
            }

            $this->saveAttributeValues($attributesToSave, $post);

            DB::commit();
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\Session::flash('flash_message',$e->getMessage(). ' '.$e->getFile(). ' '.$e->getLine());
            DB::rollback();

            return redirect(route($this->slug.'.products'));
        }

        //atach terms taxonomy
        if(isset($data_taxonomies) && $data_taxonomies != null){
            $model_taxonomy = $this->model_taxonomy;
            foreach ($data_taxonomies as $name_taxonomy=>$terms){
                $model_taxonomy::atach_terms_to_post($name_taxonomy, $terms, $post->id, $this->slug);
            }
        }

        //MetaBox
        $metabox = $this->meta_box;
        //Enabled Modules Controller
        if (isset($this->active_modules) && $this->active_modules) {
            $metabox['modules'] = self::set_modules();
        }

        if (isset($metabox) && $metabox) {
            $meta_requests = $request->metabox;
            if($meta_requests){

                $deleted_all_metabox = true;
                webus_help::delete_metabox($id, $this->slug);

                foreach ($meta_requests as $key => $meta){
                    if (isset($request['meta_translatable'][$key])) {
                        $convert_item_to_translatable = '';
                        foreach ($request['meta_translatable'][$key] as $key_translatable=>$item_translatable){
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }

                        $meta_value = stripslashes($convert_item_to_translatable);
                        webus_help::insert_metabox($id, $this->slug, $key, $meta_value);
                    } else {
                        webus_help::insert_metabox($id, $this->slug, $key, $meta);
                    }
                }
            }

            //only for repeatable
            if (!isset($deleted_all_metabox)){
                webus_help::delete_metabox($id, $this->slug);
            }

            foreach ($metabox as $meta_key=>$meta_fields){
                if ($meta_fields['html_type'] == 'repeatable') {
                    if (isset($request->$meta_key)) {
                        $meta_value = [];
                        foreach ($request->$meta_key as $key=>$value){
                            $meta_value[] = $value;
                        }

                        webus_help::insert_metabox($id, $this->slug, $meta_key, $meta_value);
                    }
                }
            }
        }

        Session::flash('flash_message','Successfully saved.');
        return redirect(route($this->slug.'.edit', ['id' => $id]));
    }

    public function import(Request $request)
    {
        $icon_page = '';
        if (isset($this->icon)){
            $icon_page = $this->icon;
        }

        $data = [
            'title' => 'Import products',
            'url_create' => '',
            'form_action' => route($this->slug.'.import_do'),
            'icon' => $icon_page
        ];

        return view('admin.products.import', ['data' => $data]);
    }

    public function importDo(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new \Exception('Method not allowed', 405);
        }

        $file = $request->file('products');
        $clear = $request->get('clear') ? true : false;
        $noOptions = $request->get('noOptions') ?? true;

        $limit = null;
        if ($request->has('limit')) {
            $limit = (int) $request->get('limit');
        }

        $offset = null;
        if ($request->has('offset')) {
            $offset = (int) $request->get('offset');
        }

        try {
            if (!$file) {
                throw new \Exception('File not found', 404);
            }

            if ($clear && !$noOptions) {
                DB::statement('DELETE FROM products');
            }

            $mapper = new ProductMapper();
            $importManager = new ImportManager($mapper);

            if ($limit) {
                $importManager->setLimit($limit);
            }

            if ($offset !== null) {
                $importManager->setOffset($offset);
            }

            $importManager->manage($file);
            $errors = $mapper->getErrors();

            $mapper->saveModel();
        } catch (\Exception $e) {
            return new JsonResponse([
                'error' => $e->getMessage().' '.$e->getFile().' '.$e->getLine(),
            ]);
        }

        return new JsonResponse([
            'errors' => count($errors) + count($mapper->getDBErrors()),
            'errors_detailed' => array_merge($mapper->getErrorsDetailed(), $mapper->getDBErrorsDetailed()),
            'saved' => $mapper->getSaved(),
            'total' => (int) $importManager->getTotal() - 1,
        ]);
    }

    protected function handleErrors(array $errors, string $sheet)
    {
        $messages = [];
        foreach ($errors as $errorRow) {
            /** @var ErrorItem $error */
            foreach ($errorRow as $error) {
                $messages[] = sprintf('Error "%s" appeared in row %s column %s in sheet "%s"',
                    $error->getMessage(),
                    $error->getRow(),
                    $error->getColumn(),
                    $sheet
                );
            }
        }
        Session::flash('flash_message_errors',$messages);
    }

    public function showInCatalog(Request $request)
    {
        var_dump('here');exit;
        var_dump($request->request->all());exit;
        $ids = $request->request->get('ids');
        var_dump($ids);exit;
    }
}
