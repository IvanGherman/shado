<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusAdminController;

class ContactsController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\Contacts'; //model
        $this->slug = 'contacts'; //id controller
        $this->title = 'Contacts'; //title controller
        $this->icon = ''; //icon page
        $this->active_modules = false;
        $this->template_post = 'positions'; //template post type (default or positions)
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = [
            'name' => 'translatable',
        ]; //in list template

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'address' => [
                'title' => 'Address',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'phone' => [
                'title' => 'Phone',
                'html_type' => 'text',
                'validate' => '',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'lat' => [
                'title' => 'Latitude',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'lng' => [
                'title' => 'Longitude',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'schedule' => [
                'title' => 'Schedule',
                'html_type' => 'schedule',
                'validate' => '',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'content',
                'days' => $this->days,
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],


        );

        //meta_boxes
        $this->meta_box = false;
    }
}
