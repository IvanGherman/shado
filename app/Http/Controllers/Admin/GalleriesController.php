<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\webus_help;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Webus\WebusAdminController;
use Illuminate\Support\Facades\Session;

class GalleriesController extends WebusAdminController
{
    public function __construct()
    {

        $this->model_name = 'App\Galleries'; //model
        $this->slug = 'galleries'; //id controller
        $this->title = 'Galleries'; //title controller
        $this->icon = ''; //icon page
        $this->template_post = 'positions'; //template post type (default or positions)
        $this->active_modules = false; //enable modules controll on this page

//      $this->model_taxonomy = 'App\WebusModels\TaxonomyTerms'; //use taxonomy table
//      $taxonomy = $this->model_taxonomy;

        $this->per_page = '25';
        $this->search = 'name'; //field for search
        $this->orderby = 'created_at,desc';
        //Identical to migrated file
        $this->list_field = ['name' => true, 'identificator' => true]; //in list template

        $this->form_fields = array( //in form template
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'

            ],

            'identificator' => [
                'title' => 'Identificator',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'

            ],

            'gallery' => [
                'title' => 'Gallery',
                'html_type' => 'gallery',
                'validate' => 'required',
                'custom_class' => 'gallery-container',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'content',
                'metabox' => true,
            ],

        );

        //meta_boxes
        $this->meta_box = false;

    }

    public function store(Request $request)
    {
        $validate_array = [];
        foreach ($this->form_fields as $key => $field){
            if ($field['validate']){
                $validate_array[$key] = $field['validate'];
            }
        }

        $all = $request->all();
        if (!empty($all['metabox'])) {
            foreach ($all['metabox'] as $key => $item) {
                $all[$key] = $item;
            }
        }

        $this->validateArray($request, $all, $validate_array);

        $model_name = $this->model_name;
        $post = new $model_name;
        $data_taxonomies = [];

        foreach ($this->form_fields as $key => $field){
            if ($field['html_type'] != 'info'){
                //translatable
                if ($field['translatable']){
                    $convert_item_to_translatable = '';
                    foreach ($request['translatable'][$key] as $key_translatable => $item_translatable){
                        if (!empty($item_translatable)){
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }
                    }
                    $post->$key = stripslashes($convert_item_to_translatable);
                } elseif ($field['html_type'] == 'file') {
                    if (!empty($request->file($key))) {
                        $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');
                        $post->$key = preg_replace('/^public\//', '', $path);
                    }
                } elseif ($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input') {
                    $post->$key = parse_url($request->$key, PHP_URL_PATH);
                } elseif ($field['html_type'] == 'password') {
                    $post->$key = bcrypt($request->$key);
                } elseif ($field['html_type'] == 'select_taxonomy') {
                    $data_taxonomies[$key] = $request->$key;
                } elseif ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                    $post->$key = null;
                    if (!empty($request->$key)) {
                        $post->$key = json_encode($request->$key);
                    }
                } elseif ($key == 'created_at') {
                    if (!empty($request->created_at)) {
                        $post->$key = Carbon::parse($request->created_at)->format('Y-m-d H:i:s');
                    } else {
                        $post->$key = Carbon::now();
                    }
                } elseif ($key === 'gallery') {
                    $post->$key = serialize($request->metabox[$key]);
                } else {
//                if($request->$key)
                    $post->$key = $request->$key;
                }
            }
        }

        $post->save();

        //attach terms taxonomy
        if (isset($data_taxonomies) && $data_taxonomies != null) {
            $model_taxonomy = $this->model_taxonomy;
            foreach ($data_taxonomies as $name_taxonomy=>$terms){
                $model_taxonomy::atach_terms_to_post($name_taxonomy, $terms, $post->id, $this->slug);
            }
        }

        //MetaBox
        $metabox = $this->meta_box;

        //Enabled Modules Controlle
        if (isset($this->active_modules) && $this->active_modules){
            $metabox['modules'] = self::set_modules();
        }

        if (isset($metabox) && $metabox){
            $meta_requests = $request->metabox;
            if ($meta_requests){
                $deleted_all_metabox = true;
                webus_help::delete_metabox($post->id, $this->slug);

                foreach ($meta_requests as $key => $meta) {
                    if (isset($request['meta_translatable'][$key])) {
                        $convert_item_to_translatable = '';
                        foreach ($request['meta_translatable'][$key] as $key_translatable=>$item_translatable){
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }

                        $meta_value = stripslashes($convert_item_to_translatable);
                        webus_help::insert_metabox($post->id, $this->slug, $key, $meta_value);
                    } else {
                        webus_help::insert_metabox($post->id, $this->slug, $key, $meta);
                    }
                }
            }

            //only for repeatable
            if (!isset($deleted_all_metabox)){
                webus_help::delete_metabox($post->id, $this->slug);
            }

            foreach ($metabox as $meta_key => $meta_fields) {
                if ($meta_fields['html_type'] == 'repeatable') {
                    if (isset($request->$meta_key)) {

                        $meta_value = [];
                        foreach ($request->$meta_key as $key => $value){
                            $meta_value[] = $value;
                        }

                        webus_help::insert_metabox($post->id, $this->slug, $meta_key, $meta_value);
                    }
                }
            }

        }

        Session::flash('flash_message','Successfully added.');

        return redirect(route($this->slug.'.edit', ['id' => $post->id]));
    }

    public function edit(Request $request, $id)
    {
        $model_name = $this->model_name;
        $model = $model_name::findOrFail($id);

        $model['gallery'] = unserialize($model['gallery']);

        foreach ($this->form_fields as $key => $field){
            //get taxonomy default values
            if ($field['html_type'] == 'select_taxonomy'){
                $model_taxonomy = $this->model_taxonomy;
                if (isset($field['multiple']) && $field['multiple'] == false) {
                    $model[$key] = key($model_taxonomy::get_post_terms($key, $id, $this->slug, true));
                } elseif (isset($field['multiple']) && $field['multiple'] == true) {
                    $model['selected_'.$key] = $model_taxonomy::get_post_terms($key, $id, $this->slug, true);
                }
            }
            //get select & multiple from field database
            if ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                if ($model->$key) {
                    $model['selected_'.$key] = json_decode($model->$key);
                } else {
                    $model['selected_'.$key] = [];
                }
            }
        }

        //metaboxes
        $metabox = false;

        if (isset($this->meta_box) && $this->meta_box) {
            $metabox = $this->meta_box;
            $current_route_id = $id;
            $current_template = (isset($model->template) ? $model->template : '');
            $excludeFields = [];
            foreach ($this->meta_box as $key => $field){
                //prepare exclude
                if (!empty($field['show_on']) && !in_array($current_route_id, $field['show_on']) && !in_array($current_template, $field['show_on'])){
                    $excludeFields[] = $key;
                }

                if (isset($field['multiple']) && $field['multiple'] == true) { //for select multiple
                    $model['selected_'.$key] = webus_help::get_metabox($id, $this->slug, $key);
                } else {
                    $model[$key] = webus_help::get_metabox($id, $this->slug, $key);
                }
            }

            if (!empty($excludeFields)) {
                foreach ($excludeFields as $exludeField){
                    unset($metabox[$exludeField]);
                }
            }
        }
        //Enabled Modules Controller
        if (isset($this->active_modules) && $this->active_modules){
            $metabox['modules'] = self::set_modules();
            $model['modules'] = webus_help::get_metabox($id, $this->slug, 'modules');
        }

        $icon_page = '';
        if (isset($this->icon)){
            $icon_page = $this->icon;
        }

        $data = [
            'title' => 'Edit '.str_singular($this->title),
            'url_create' => route($this->slug.'.create'),
            'url_delete' => 'admin/'.$this->slug.'/delete/',
            'url_build' => 'admin/'.$this->slug.'/menu/',
            'form_action' => route($this->slug.'.update', ['id' => $id]),
            'rows' => $this->form_fields,
            'metabox' => $metabox,
            'icon' => $icon_page
        ];

        $view = 'webus.standart.add-edit';

        if (isset($this->template_post) && $this->template_post == 'positions') {
            $view = 'webus.standart.add-edit-positions';
        }

        if (view()->exists('webus.'.$this->slug.'.add-edit')) {
            $view = 'webus.'.$this->slug.'.add-edit';
        }

        return view($view, ['data' => $data, 'model' => $model]);
    }

    public function update(Request $request, $id)
    {
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);

        $validate_array = [];
        foreach ($this->form_fields as $key => $field){
            if ($field['validate']) {
                if ($key == 'slug' && $post->slug !== $request->slug) {
                    $validate_array[$key] = $field['validate'];
                }
            }
        }

        $all = $request->all();
        if (!empty($all['metabox'])) {
            foreach ($all['metabox'] as $key => $item) {
                $all[$key] = $item;
            }
        }

        $this->validateArray($request, $all, $validate_array);

        foreach ($this->form_fields as $key => $field){
            if ($field['html_type'] != 'info'){
                //translatable
                if ($field['translatable']) {

                    $convert_item_to_translatable = '';
                    foreach ($request['translatable'][$key] as $key_translatable=>$item_translatable){
                        if (!empty($item_translatable)) {
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }
                    }

                    $post->$key = stripslashes($convert_item_to_translatable);

                } elseif($field['html_type'] == 'file') {

                    if (!empty($request->file($key))) {
                        $path = $request->file($key)->store($this->slug.'/'.date('Y-m'), 'public');
                        $post->$key = preg_replace('/^public\//', '', $path);
                    }

                    //if is checked for remove from db this file url
                    if ($request['remove'][$key] == 'on') {
                        $post->$key = '';
                    }
                } elseif ($field['html_type'] == 'file_input' || $field['html_type'] == 'image_input') {
                    $post->$key = parse_url($request->$key, PHP_URL_PATH);
                } elseif ($field['html_type'] == 'password') {
                    if ($request->$key) {
                        $post->$key = bcrypt($request->$key);
                    }
                } elseif ($field['html_type'] == 'select_taxonomy') {
                    $data_taxonomies[$key] = $request->$key;
                } elseif ($field['html_type'] == 'select' && isset($field['multiple']) && $field['multiple']) {
                    if (!empty($request->$key)) {
                        $post->$key = json_encode($request->$key);
                    } else {
                        $post->$key = null;
                    }
                } elseif ($key == 'created_at') {
                    $post->$key = Carbon::parse($request->created_at)->format('Y-m-d H:i');
                } elseif ($key === 'gallery') {
                    $post->$key = serialize($request->metabox[$key]);
                } else {
//                if($request->$key)
                    $post->$key = $request->$key;
                }
            }

        }

        $post->save();

        //atach terms taxonomy
        if(isset($data_taxonomies) && $data_taxonomies != null){
            $model_taxonomy = $this->model_taxonomy;
            foreach ($data_taxonomies as $name_taxonomy=>$terms){
                $model_taxonomy::atach_terms_to_post($name_taxonomy, $terms, $post->id, $this->slug);
            }
        }

        //MetaBox
        $metabox = $this->meta_box;
        //Enabled Modules Controller
        if (isset($this->active_modules) && $this->active_modules) {
            $metabox['modules'] = self::set_modules();
        }

        if (isset($metabox) && $metabox) {
            $meta_requests = $request->metabox;
            if($meta_requests){

                $deleted_all_metabox = true;
                webus_help::delete_metabox($id, $this->slug);

                foreach ($meta_requests as $key => $meta){
                    if (isset($request['meta_translatable'][$key])) {
                        $convert_item_to_translatable = '';
                        foreach ($request['meta_translatable'][$key] as $key_translatable=>$item_translatable){
                            $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                        }

                        $meta_value = stripslashes($convert_item_to_translatable);
                        webus_help::insert_metabox($id, $this->slug, $key, $meta_value);
                    } else {
                        webus_help::insert_metabox($id, $this->slug, $key, $meta);
                    }
                }
            }

            //only for repeatable
            if (!isset($deleted_all_metabox)){
                webus_help::delete_metabox($id, $this->slug);
            }

            foreach ($metabox as $meta_key=>$meta_fields){
                if ($meta_fields['html_type'] == 'repeatable') {
                    if (isset($request->$meta_key)) {
                        $meta_value = [];
                        foreach ($request->$meta_key as $key=>$value){
                            $meta_value[] = $value;
                        }

                        webus_help::insert_metabox($id, $this->slug, $meta_key, $meta_value);
                    }
                }
            }
        }

        Session::flash('flash_message','Successfully saved.');
        return redirect(route($this->slug.'.edit', ['id' => $id]));
    }
}
