<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusAdminController;

class CustomMenuController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\CustomMenu'; //model
        $this->slug = 'custom-menu'; //id controller
        $this->title = 'Custom menu'; //title controller
        $this->icon = ''; //icon page
        $this->active_modules = false;
        $this->template_post = 'positions'; //template post type (default or positions)
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = [
            'name' => 'translatable',
        ]; //in list template

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'image' => [
                'title' => 'Image',
                'html_type' => 'image_input',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'url' => [
                'title' => 'Url',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'position' => [
                'title' => 'Position',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],


        );

        //meta_boxes
        $this->meta_box = false;
    }
}
