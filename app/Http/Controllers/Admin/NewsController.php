<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusAdminController;

class NewsController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\News'; //model
        $this->slug = 'news'; //id controller
        $this->title = 'News'; //title controller
        $this->icon = ''; //icon page
        $this->active_modules = true;
        $this->template_post = 'positions'; //template post type (default or positions)
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = ['name' => 'translatable', 'image' => 'image_src', 'slug']; //in list template

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'short_description' => [
                'title' => 'Short description',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'body' => [
                'title' => 'Body',
                'html_type' => 'body',
                'validate' => false,
                'custom_class' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'meta_title' => [
                'title' => 'Meta title',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'

            ],

            'meta_description' => [
                'title' => 'Meta description',
                'html_type' => 'textarea',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'

            ],

            'meta_keywords' => [
                'title' => 'Meta keywords',
                'html_type' => 'textarea',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'

            ],

            'slug' => [
                'title' => 'Slug (unique field)',
                'html_type' => 'text',
                'validate' => 'required|max:255|unique:news',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'big_image' => [
                'title' => 'Big image',
                'html_type' => 'image_input',
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'image' => [
                'title' => 'Image',
                'html_type' => 'image_input',
                'validate' => false,
                'custom_class' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],


        );

        //meta_boxes
        $this->meta_box = false;
    }
}
