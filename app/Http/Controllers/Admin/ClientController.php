<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Webus\WebusAdminController;
class ClientController extends WebusAdminController
{
    public function __construct()
    {

        $this->model_name = 'App\Client'; //model
        $this->slug = 'client'; //id controller
        $this->title = 'Client'; //title controller
        $this->icon = ''; //icon page
        $this->template_post = 'positions'; //template post type (default or positions)
        $this->active_modules = false; //enable modules controll on this page

//      $this->model_taxonomy = 'App\WebusModels\TaxonomyTerms'; //use taxonomy table
//      $taxonomy = $this->model_taxonomy;

        $this->per_page = '25';
        $this->search = 'name'; //field for search
        $this->orderby = 'created_at,desc';
        //Identical to migrated file
        $this->list_field = ['name'=>true, 'slug' =>true, 'post_status' => 'status']; //in list template

        $this->form_fields = array( //in form template
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'

            ],

            'link' => [
                'title' => 'Link (URL)',
                'html_type' => 'text',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'

            ],

            'logo' => [
                'title' => 'Logo',
                'html_type' => 'image_input',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'

            ],

            'template' => [
                'title' => 'Template',
                'html_type' => 'select',
                'options' => array(
                    'default' => 'Default',
                    'home' => 'Home',
                    'contacts' => 'Contacts',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

//            'category' => [
//                'title' => 'Categories',
//                'html_type' => 'select_taxonomy',
//                'options' => $taxonomy::get_terms_for_selected('category'),
//                'default_value_not_by_keys' => false, //false for none multiple taxonomy and none multiple value
//                'multiple' => true,
//                'validate' => false,
//                'custom_class' => '',
//                'translatable' => false,
//                'position' => 'sidebar'
//            ],


        );

        //meta_boxes
        $this->meta_box = false;

    }
}
