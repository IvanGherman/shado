<?php

namespace App\Http\Controllers\Admin;

use App\Http\Services\ChunkReadFilter;
use App\Http\Services\ErrorItem;
use App\Http\Services\ImportManager;
use App\Http\Services\ImportMapper\AttributeMapper;
use App\Http\Services\ImportMapper\AttributeValueMapper;
use App\Http\Services\ImportMapper\ColorMapper;
use App\Http\Services\ImportMapper\CountryMapper;
use App\Http\Services\ImportMapper\ImportMapper;
use App\Http\Services\ImportMapper\TextureMapper;
use App\Http\Services\ImportMapper\TransparencyMapper;
use App\Http\Services\ImportMapper\TreeTypeMapper;
use App\Texture;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusAdminController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AttributesController extends WebusAdminController
{
    public function __construct()
    {
        $this->model_name = 'App\Attribute'; //model
        $this->slug = 'attributes'; //id controller
        $this->title = 'Attributes'; //title controller
        $this->icon = ''; //icon page
        $this->active_modules = false;
        $this->template_post = 'positions'; //template post type (default or positions)
        //Identical to migrated file, attribute available(translatable,image_src)
        $this->list_field = [
            'name' => 'translatable',
        ]; //in list template

        $this->form_fields = array( //in form template

            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'content'
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
                'position' => 'content'
            ],

            'position' => [
                'title' => 'Position',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'show_in_filter' => [
                'title' => 'Show in filter',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Yes',
                    '0' => 'No',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

            'post_status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled',
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'slug' => true, //for slug
                'translatable' => false,
                'position' => 'sidebar'
            ],

        );

        //meta_boxes
        $this->meta_box = false;
    }

    public function import(Request $request)
    {
        $icon_page = '';
        if (isset($this->icon)){
            $icon_page = $this->icon;
        }

        $data = [
            'title' => 'Import attributes, colors, countries and etc.',
            'url_create' => '',
            'form_action' => route($this->slug.'.import_do'),
            'icon' => $icon_page
        ];

        return view('admin.attributes.import', ['data' => $data]);
    }

    public function importDo(Request $request)
    {
        $file = $request->file('categories');
        $clear = $request->get('clear') ? true : false;

        try {
            if (!$file) {
                throw new \Exception('File not found', 404);
            }

            if ($clear) {
                DB::statement('DELETE FROM attributes');
                DB::statement('DELETE FROM colors');
                DB::statement('DELETE FROM countries');
                DB::statement('DELETE FROM product_textures');
                DB::statement('DELETE FROM texture_types');
                DB::statement('UPDATE products set transparency_id = null');
                DB::statement('UPDATE products set tree_type_id = null');
                DB::statement('DELETE FROM tree_types');
                DB::statement('DELETE FROM transparencies');
            }

            $mappers = [
                'colors' => new ColorMapper(),
                'countries' => new CountryMapper(),
                'textures' => new TextureMapper(),
                'transparencies' => new TransparencyMapper(),
                'tree_types' => new TreeTypeMapper(),
                'attributes' => new AttributeMapper(),
                'values' => new AttributeValueMapper(),
            ];

            $finalErrors = [
                'colors' => [],
                'countries' => [],
                'textures' => [],
                'transparencies' => [],
                'tree_types' => [],
                'attributes' => [],
                'values' => [],
            ];

            $messages = [];
            /** @var ImportMapper $mapper */
            foreach ($mappers as $key => $mapper) {
                $importManager = new ImportManager($mapper);
                $importManager->manage($file);

                $mapper->skipEmptyRows();
                $mapper->saveModel();

                $errors = array_merge($mapper->getErrorsDetailed(), $mapper->getDBErrorsDetailed());

                if (!empty($errors)) {
                    $finalErrors[$key] = $errors;
                }

                $messages[] = sprintf('%s %s saved, %s skipped ', $mapper->getSaved(), $key, count($mapper->getErrors()));
            }

            Session::flash('flash_message_success', $messages);

            $this->handleDetailedErrors($finalErrors);
        } catch (\Exception $e) {
            Session::flash('flash_message', $e->getMessage());
        }

        return redirect(route($this->slug.'.import', []));
    }

    public function handleDetailedErrors(array $errors)
    {
        $messages = [];
        foreach ($errors as $prefix => $errorContainer) {
            foreach ($errorContainer as $error) {
                $messages[] = mb_strtoupper($prefix).': '.$error;
            }
        }

        Session::flash('flash_message_errors',$messages);
    }

    protected function handleErrors(array $errors, string $sheet)
    {
        $messages = [];
        foreach ($errors as $errorRow) {
            /** @var ErrorItem $error */
            foreach ($errorRow as $error) {
                $messages[] = sprintf('Error "%s" appeared in row %s column %s in sheet "%s"',
                    $error->getMessage(),
                    $error->getRow(),
                    $error->getColumn(),
                    $sheet
                );
            }
        }
        Session::flash('flash_message_errors',$messages);
    }
}
