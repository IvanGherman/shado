<?php

namespace App\Http\Controllers\Webus\Shop\Payments;

use App\Http\HelpersShop\webus_shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\WebusShop\Orders;
use Session;
use Cart;
use Illuminate\Support\Facades\Log;
use \YandexMoney\API;

class Yandex extends Controller
{

    public function __construct(){
        $this->info_app = array(
            'wallet' => '410013681720286',
            'client_id' => '0ABB4CC6FFBC9C8FD4378FFDDA38CB7142BB179F4F9DAF1FE45DC49B9BBE9E69',
            'redirect' => route('yandex_redirect')
        );
    }

    public function generate_payment(){
        $price = webus_shop::calculate_price(Cart::total(), 'RUB');

        if(Cart::count() == 0){return redirect('/');}
        //include library
        require_once app_path('Http/Controllers/Webus/Shop/Payments/YandexSDK/api.php');

        $scope = array(
            "payment.to-account(\"".$this->info_app['wallet']."\",\"account\").limit(,".$price.")",
            "money-source(\"wallet\",\"card\")"
        );

        $auth_url = API::buildObtainTokenUrl($this->info_app['client_id'], $this->info_app['redirect'], $scope);

        return redirect($auth_url);

    }

    public function redirect(Request $request){
        $order_id = $request->session()->get('order_id');
        if($order_id){
            $error = false;
            $price = webus_shop::calculate_price(Cart::total(), 'RUB');
            //include library
            require_once app_path('Http/Controllers/Webus/Shop/Payments/YandexSDK/api.php');
            $code = $request->code;
            $access_token_response = API::getAccessToken($this->info_app['client_id'], $code, $this->info_app['redirect'], $client_secret=NULL);
            if(property_exists($access_token_response, "error")) {
                // process error
            }
            $access_token = $access_token_response->access_token;
            if(!empty($access_token)){
                $api = new API($access_token);

                $message = 'payment to order '.$order_id;
                // make request payment
                $request_payment = $api->requestPayment(array(
                    "pattern_id" => "p2p",
                    "to" => $this->info_app['wallet'],
                    "amount_due" => $price,
                    "comment" => $message,
                    "message" => $message,
                    "label" => $order_id,
                ));

                switch($request_payment->status){
                    case 'success':
                        do{
                            // call process payment to finish payment
                            $process_payment = $api->processPayment(array(
                                "request_id" => $request_payment->request_id,
                            ));

                            if($process_payment->status == "in_progress") {
                                sleep(1);
                            }
                        }while ($process_payment->status == "in_progress");

                        self::updateStatus($process_payment, $order_id);
                        $error = false;
                        break;
                    case 'refused':
                        Log::error($message.' --- '.$request_payment->error);
                        self::setFailed($order_id, $request_payment->error);
                        $error = true;
                        return redirect(url('checkout/failed'));
                        break;
                    case 'hold_for_pickup':
                        Log::error($message.' --- '.$request_payment->error);
                        self::setFailed($order_id, $request_payment->error.' Получатель перевода не найден, будет отправлен перевод до востребования. Успешное выполнение.');
                        $error = true;
                        break;
                }

                if (!$error){
                    $remove_session = $request->session()->forget('order_id');
                    return redirect(route('checkout_success'));
                }else{
                    $remove_session = $request->session()->forget('order_id');
                    return redirect(route('checkout_failed'));
                }

            }else{
                $remove_session = $request->session()->forget('order_id');
                return redirect(route('checkout_failed'));
            }
        }else{
            $remove_session = $request->session()->forget('order_id');
            return redirect(route('checkout_failed'));
        }

    }

    private function updateStatus(&$resp, $order_id)
    {
        if ($resp->status == 'success')
        {
            $order_info = Orders::find($order_id);
            $order_info->order_status = 1;
            $order_info->save();
        }
        else
        {
            $order_info = Orders::find($order_id);
            $order_info->order_status = 9;
            $order_info->save();

            //$this->log_save('wallet_redirect: Error '.$this->descriptionError($resp->error));
        }
    }

    private function setFailed($order_id, $info)
    {
        $order_info = Orders::find($order_id);
        $order_info->order_status = 9;
        $order_info->info = $info;

        $order_info->save();
    }


    private function descriptionError($error)
    {
        $error_array = array(
            'invalid_request' => 'Your request is missing required parameters or settings are incorrect or invalid values',
            'invalid_scope' => 'The scope parameter is missing or has an invalid value or a logical contradiction',
            'unauthorized_client' => 'Invalid parameter client_id, or the application does not have the right to request authorization (such as its client_id blocked Yandex.Money)',
            'access_denied' => 'Has declined a request authorization application',
            'invalid_grant' => 'The issue access_token denied. Issued a temporary token is not Google search or expired, or on the temporary token is issued access_token (second request authorization token with the same time token)',
            'illegal_params' => 'Required payment options are not available or have invalid values.',
            'illegal_param_label' => 'Invalid parameter value label',
            'phone_unknown' => 'A phone number is not associated with a user account or payee',
            'payment_refused' => 'Магазин отказал в приеме платежа (например, пользователь пытался заплатить за товар, которого нет в магазине)',
            'limit_exceeded' => 'Exceeded one of the limits on operations: on the amount of the transaction for authorization token issued; transaction amount for the period of time for the token issued by the authorization; Yandeks.Deneg restrictions for different types of operations.',
            'authorization_reject' => 'In payment authorization is denied. Possible reasons are: transaction with the current parameters is not available to the user; person does not accept the Agreement on the use of the service "shops".',
            'contract_not_found' => 'None exhibited a contract with a given request_id',
            'not_enough_funds' => 'Insufficient funds in the account of the payer. Need to recharge and carry out a new delivery',
            'not-enough-funds' => 'Insufficient funds in the account of the payer. Need to recharge and carry out a new delivery',
            'money_source_not_available' => 'The requested method of payment (money_source) is not available for this payment',
            'illegal_param_csc' => 'tsutstvuet or an invalid parameter value cs',
        );
        if(array_key_exists($error,$error_array))
            $return = $error_array[$error];
        else
            $return = $error;
        return $return;
    }

}
