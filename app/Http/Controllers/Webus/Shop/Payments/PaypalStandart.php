<?php

namespace App\Http\Controllers\Webus\Shop\Payments;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\WebusShop\Orders;

class PaypalStandart extends Controller
{
    public static $paypal_email = 'dima.botezatu@gmail.com';

    public static function generate_payment($order_id, $products, $currency){
        $paypal_email = self::$paypal_email;

        $return_url = route('checkout_success');
        $cancel_url = url('/');
        $notify_url = route('paypal_standart_callback');


        $querystring = '';

        // Firstly Append paypal account to querystring
        $querystring .= "?business=".urlencode($paypal_email)."&";
        //order_id
        $querystring .= "custom=".urlencode($order_id)."&";

        //loop for products
        $i=0;
        foreach($products as $product){ $i++;
            $querystring .= 'item_name_'.$i.'='.$product['name'].'&';
            $querystring .= 'item_number_'.$i.'='.$product['id'].'&';
            $querystring .= 'amount_'.$i.'='.$product['price'].'&';
            $querystring .= 'quantity_'.$i.'='.$product['quantity'].'&';
        }

        //other fields
        $querystring .= "cmd=".urlencode('_cart')."&";
        $querystring .= "env=".urlencode('sandbox')."&";
        $querystring .= "upload=".urlencode('1')."&";
        $querystring .= "currency_code=".urlencode($currency)."&";
        $querystring .= "first_name=".urlencode(Auth::user()->name)."&";
        $querystring .= "last_name=".urlencode('0')."&";
        $querystring .= "address_override=".urlencode('0')."&";
        $querystring .= "email=".urlencode(Auth::user()->email)."&";
        $querystring .= "invoice=".urlencode('INV-'.$order_id)."&";
        $querystring .= "lc=".urlencode('en')."&";
        $querystring .= "rm=".urlencode('2')."&";
        $querystring .= "no_note=".urlencode('1')."&";
        $querystring .= "charset=".urlencode('utf-8')."&";
        $querystring .= "paymentaction=".urlencode('sale')."&";
        $querystring .= "bn=".urlencode(config('app.name'))."&";

        // Append paypal return addresses
        $querystring .= "return=".urlencode(stripslashes($return_url))."&";
        $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
        $querystring .= "notify_url=".urlencode($notify_url);

        //return generated url for payment
        return "https://www.paypal.com/cgi-bin/webscr'.$querystring";

    }

    public function callback(Request $request){
        if(isset($request->custom)){
            $order_id = $request->custom;
        }else{
            $order_id = 0;
        }

        $order = Orders::find($order_id);

        if($order){
            $request_paypal = 'cmd=_notify-validate';
            foreach ($request->all() as $key => $value) {
                $request_paypal .= '&' . $key . '=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
            }
            $curl = curl_init('https://www.paypal.com/cgi-bin/webscr');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request_paypal);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($curl);
            if ((strcmp($response, 'VERIFIED') == 0 || strcmp($response, 'UNVERIFIED') == 0) && isset($request->payment_status)) {
                $order_status_id = '';
                //$price_from_paypal = $request->mc_gross;
                switch($request->payment_status) {
                    case 'Canceled_Reversal':
                        $order_status_id = '0';
                        break;
                    case 'Completed':
                        if ((strtolower($request->receiver_email) == strtolower(self::$paypal_email))) {
                            $order_status_id = '1';
                        } else {
                            //$this->log->write('PP_STANDARD :: RECEIVER EMAIL MISMATCH! ' . strtolower($_POST['receiver_email']));
                        }
                        break;
                    case 'Denied':
                        $order_status_id = '2';
                        break;
                    case 'Expired':
                        $order_status_id = '3';
                        break;
                    case 'Failed':
                        $order_status_id = '4';
                        break;
                    case 'Pending':
                        $order_status_id = '5';
                        break;
                    case 'Processed':
                        $order_status_id = '6';
                        break;
                    case 'Refunded':
                        $order_status_id = '7';
                        break;
                    case 'Reversed':
                        $order_status_id = '8';
                        break;
                    case 'Voided':
                        $order_status_id = '9';
                        break;
                }

                //update status
                $order->order_status = $order_status_id;
                $order->price_payment = number_format($request->mc_gross, 2);
                $order->save();
            }

            curl_close($curl);

        }else{
            abort(401);
        }


    }
}
