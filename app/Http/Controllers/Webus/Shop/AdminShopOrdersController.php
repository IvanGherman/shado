<?php

namespace App\Http\Controllers\Webus\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WebusShop\Orders;
use Session;
class AdminShopOrdersController extends Controller
{
    public function index(Request $request){
        if(isset($request->search)){
            $orders = Orders::where('id', $request->search);
        }else{
            $orders = Orders::limit(-1);
        }
        $orders = $orders->orderBy('created_at', 'desc')->where('order_status', '<>', 0)->paginate(25);
        $data = [
            'title' => 'Orders',
            'icon' => 'window-list',
            'orders' => $orders
        ];

        return view('webus.shop.orders', ['data' => $data]);
    }
    
    public function edit($id){
        $order = Orders::with('products')->findOrFail($id);
        $data = [
            'title' => 'Order Nr.'.$id,
            'icon' => 'window-list',
        ];

        return view('webus.shop.view-order', ['data' => $data,  'record' => $order]);
    }
    
    public function update(Request $request, $id){
        $order = Orders::with('products')->findOrFail($id);
        $order->order_status = $request->order_status;
        $order->save();
        Session::flash('flash_message','The order has been updated!');
        return redirect()->back();
    }
    
    public function delete($id){
        $order = Orders::with('products')->findOrFail($id);
        $order->products()->delete();
        $order->delete();
        Session::flash('flash_message','The order '.$id.' has been deleted!');
        return redirect(route('admin_orders'));
    }
}
