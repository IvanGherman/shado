<?php

namespace App\Http\Controllers\Webus\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WebusShop\Orders;
use Illuminate\Support\Facades\Auth;
use App\User;
use Session;
use App\WebusShop\CustomerAdresses;
class AccountController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');

        $this->menu = array(
                'info' => array(
                  'title' => 'Моя учетная запись',
                  'items' => array(
                        route('account') => 'Основные данные',
                        route('account_reset_password') => 'Изменить свой пароль',
                        route('account_addresses') => 'Изменить мои адреса',
                        route('account_logout') => 'Выйти'
                    )
                ),
                'orders' => array(
                    'title' => 'Мои заказы',
                    'items' => array(
                        route('account_orders') => 'История заказов'
                    )
                )
        );
    }

    public function index(){
        $subtitle = 'Основные данные';
        return view('front.shop.account',[
            'title' => 'Личный кабинет',
            'subtitle' => 'Основные данные',
            'menu' => $this->menu,
            'record' => User::find(Auth::user()->id)
        ]);
    }

    public function info_action(Request $request){
        $validate_array['name'] = 'required';
        $this->validate($request, $validate_array);
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->save();
        Session::flash('flash_message','Данные были изменены!');
        return redirect()->back();
    }

    public function orders(){
        $subtitle = 'Мои заказы';
        return view('front.shop.account-orders',[
            'title' => 'Личный кабинет',
            'subtitle' => $subtitle,
            'menu' => $this->menu,
            'orders' => Orders::with('products')->where('user_id', Auth::user()->id)->where('order_status', '<>', 0)->orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    public function password(){
        $subtitle = 'Изменить свой пароль';
        return view('front.shop.account-password',[
            'title' => 'Личный кабинет',
            'subtitle' => $subtitle,
            'menu' => $this->menu
        ]);
    }

    public function password_action(Request $request){
        $validate_array['password'] = 'required|string|min:6|confirmed';
        $this->validate($request, $validate_array);
        
        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->confirm_password);
        $user->save();
        Session::flash('flash_message','Пароль был успешно изменен!');
        return redirect()->back();
        
    }

    public function addresses(){
        $subtitle = 'Мои адреса';
        return view('front.shop.account-addresses',[
            'title' => 'Личный кабинет',
            'subtitle' => $subtitle,
            'menu' => $this->menu,
            'addresses' => CustomerAdresses::customer_addresses()
        ]);
    }
    
    public function edit_address($id){
        $address = CustomerAdresses::where('user_id', Auth::user()->id)->findOrFail($id);
        $subtitle = 'Мои адреса';
        return view('front.shop.account-edit_address',[
            'title' => 'Личный кабинет',
            'subtitle' => $subtitle,
            'menu' => $this->menu,
            'record' => $address,
            'countries' => \App\Http\HelpersShop\webus_shop::get_countries()->pluck('name', 'id')
        ]);
    }
    
    public function edit_address_action(Request $request, $id){
        $update_address = CustomerAdresses::where('user_id', Auth::user()->id)->findOrFail($id);
        $validate_array['name'] = 'required';
        $validate_array['country_id'] = 'required';
        $validate_array['city'] = 'required';
        $validate_array['street'] = 'required';
        $validate_array['index'] = 'required';
        $validate_array['phone'] = 'required';
        $this->validate($request, $validate_array);

        //Update
        $update_address->name = $request->name;
        $update_address->country_id = $request->country_id;
        $update_address->city = $request->city;
        $update_address->street = $request->street;
        $update_address->index = $request->index;
        $update_address->phone = $request->phone;
        $update_address->save();
        Session::flash('flash_message','Адрес был успешно изменен');
        return redirect()->back();
    }

    public function delete_address($id){
        $remove_address = CustomerAdresses::where('user_id', Auth::user()->id)->findOrFail($id);
        $remove_address->delete();
        Session::flash('flash_message','Адрес был успешно удален');
        return redirect(route('account_addresses'));
    }

    public function logout(){
        Auth::logout();
        return redirect(url('/'));
    }

//    public function newsletter(){
//
//    }
}
