<?php

namespace App\Http\Controllers\Webus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WebusModels\Settings;
use Session;
use DB;
class WebusAdminSiteSettingsController extends Controller
{
    public function index(){
       $settings = Settings::get();
        return view('webus.settings.settings', [
            'settings' => $settings
        ]);
    }
    
    public function add(Request $request){
        $record = new Settings();
        $record->name = $request->name;
        $record->key = $request->key;
        $record->type = $request->type;
        $record->translatable = $request->translatable;
        $record->save();
        Session::flash('flash_message','Successfully added.');
        return redirect(route('settings'));
        
    }
    
    public function save_options(Request $request){
        $list_settings = Settings::get();
        foreach ($list_settings as $setting){
            //update setting
            $request_field = $setting->key;

            if($setting->translatable == 'true'){
                $convert_item_to_translatable = '';
                foreach ($request['translatable'][$setting->key] as $key_translatable=>$item_translatable){
                    if(!empty($item_translatable)){
                        $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                    }
                }
                $field_value = stripslashes($convert_item_to_translatable);
            }else{
                $field_value = $request->$request_field;
            }

                $affected_setting = DB::table('settings')->where('key', $setting->key)->update(['value' => $field_value]);

        }

        Session::flash('flash_message','Successfully saved.');
        return redirect(route('settings'));
    }

    public function delete(Request $request, $setting_id){
        $record = Settings::findOrFail($setting_id);
        $record->delete();
        Session::flash('flash_message','Successfully deleted.');
        return redirect(route('settings'));
    }
    
}

