<?php

namespace App\Http\Controllers\Webus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\webus_help;

class WebusMenuController extends WebusAdminController
{

    static function support_languages(){
        //Define true if this site support multilanguage
        return config('app.multilangual', false);
    }

    public function delete($id){
        $model_name = $this->model_name;
        $post = $model_name::findOrFail($id);
        $post->delete();
        //delete items
        $model_menu_items = 'App\WebusModels\WebusMenuItems';
        $item = $model_menu_items::where('menu_id', $id)->delete();

        Session::flash('flash_message','Successfully deleted.');
        return redirect(route($this->slug.'.'.$this->slug));
    }

    /**********************BUILD MENU ****************************/

    public function build($id){
        $model_menu_items = 'App\WebusModels\WebusMenuItems';

        $model_name = $this->model_name;

        $post = $model_name::findOrFail($id);

        $available_urls =  \App\Pages::get()->pluck('slug' );
        $available_urls = ($available_urls != null ? implode(',',explode(',',$available_urls)) : '');

        $translatable = self::support_languages();
        $data = [
            'menu_id' => $id,
            'model' => $post,
            'title' => 'Build menu',
            'items' => webus_help::menu($post->slug, 'default', $translatable), //get menu template by menu name and template, template by default is "default"
            'form_action_add_new_item' => route('menu.add_new_item', ['id' => $id]),
            'form_action_update_item' => route('menu.update_item', ['id' => $id]),
            'form_action_delete_item' => route('menu.delete_item', ['id' => $id]),
            'order_action' => route('menu.order', ['id' => $id]),
            'translatable' => $translatable
        ];

        return view('webus.menu.build', ['data' => $data, 'available_urls' => $available_urls]);
    }

    public function add_new_item(Request $request,$id){
        $model_name = $this->model_name;
        $if_exist_menu = $model_name::findOrFail($id);

        $translatable =  self::support_languages();
        if($translatable){
            $convert_item_to_translatable = '';
            foreach ($request['translatable']['name'] as $key_translatable=>$item_translatable){
                if(!empty($item_translatable)){
                    $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                }
            }
            $name = stripslashes($convert_item_to_translatable);
        }else{
            $name = $request->name;
        }

        $model_menu_items = 'App\WebusModels\WebusMenuItems';
        $insert_new_item = new $model_menu_items;
        $insert_new_item->menu_id = $id;
        $insert_new_item->parent_id = $request->parent_id;
        $insert_new_item->name = $name;
        $insert_new_item->url = $request->url;
        $insert_new_item->icon = $request->icon;
        $insert_new_item->class = $request->class;
        $insert_new_item->target = $request->target;
        $insert_new_item->save();

        return redirect(route($this->slug.'.build', ['id' => $id]));

    }


    public function update_item(Request $request,$id){

        $model_name = $this->model_name;
        $if_exist_menu = $model_name::findOrFail($id);

        $translatable =  self::support_languages();
        if($translatable){
            $convert_item_to_translatable = '';
            foreach ($request['translatable']['name'] as $key_translatable=>$item_translatable){
                if(!empty($item_translatable)){
                    $convert_item_to_translatable .= '[:'.$key_translatable.']'.$item_translatable;
                }
            }
            $name = stripslashes($convert_item_to_translatable);
        }else{
            $name = $request->name;
        }

        $model_menu_items = 'App\WebusModels\WebusMenuItems';
        $insert_new_item = $model_menu_items::findOrFail($request->item_id);
        $insert_new_item->menu_id = $id;
        $insert_new_item->parent_id = $request->parent_id;
        $insert_new_item->name = $name;
        $insert_new_item->url = $request->url;
        $insert_new_item->icon = $request->icon;
        $insert_new_item->class = $request->class;
        $insert_new_item->target = $request->target;
        $insert_new_item->save();

        return redirect(route($this->slug.'.build', ['id' => $id]));

    }

    public function delete_item(Request $request, $id){
        $model_menu_items = 'App\WebusModels\WebusMenuItems';
        $item = $model_menu_items::findOrFail($id);
        $item->delete();

        return redirect(route($this->slug.'.build', ['id' => $request->menu]));
    }

    public function order(Request $request, $id){

        $model_name = $this->model_name;
        $if_exist_menu = $model_name::findOrFail($id);

        $menuItemOrder = json_decode($request->input('order'));

        $this->orderMenu($menuItemOrder, null);


    }

    private function orderMenu(array $menuItems, $parentId)
    {
        $model_name = 'App\WebusModels\WebusMenuItems';

        foreach ($menuItems as $index => $menuItem) {
            $item = $model_name::findOrFail($menuItem->id);
            $item->item_order = $index + 1;
            $item->parent_id = $parentId;
            $item->save();

            if (isset($menuItem->children)) {
                $this->orderMenu($menuItem->children, $item->id);
            }
        }
    }



}
