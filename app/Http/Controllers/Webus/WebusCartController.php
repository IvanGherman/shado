<?php

namespace App\Http\Controllers\Webus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Products;
use Redirect;
use App\Http\HelpersShop\webus_shop;
class CartController extends Controller
{

    /**** Cart *****/
    /** Routes 
    Route::get('cart', ['uses' => 'Webus\WebusCartController@index', 'as' => 'cart']);
    Route::post('cart/add', ['uses' => 'Webus\WebusCartController@add', 'as' => 'cart_add']);
    Route::post('cart/remove', ['uses' => 'Webus\WebusCartController@remove', 'as' => 'cart_remove']);
    Route::post('cart/update', ['uses' => 'Webus\WebusCartController@update', 'as' => 'cart_update']);
    Route::post('cart/change_currency_product', ['uses' => 'Webus\WebusCartController@change_currency_product', 'as' => 'change_currency_product']);
    Route::get('cart/checkout', ['uses' => 'Webus\WebusCartController@checkout', 'as' => 'checkout']);
     * 
     **/
    private function get_cart(){
        $products = array();
        $cart = Cart::content();

        foreach ($cart as $product){
            $products[] = array(
                'key' => $product->rowId,
                'id' => $product->id,
                'quantity' => $product->qty,
                'name' => $product->name,
                'image' => Products::image($product->id),
                'price' => webus_shop::calculate_price($product->price),
                'price_total' => webus_shop::calculate_price($product->price*$product->qty)
            );
        }

        return $products;
    }

    public function index(){
        return view('front.cart', [
            'title' => trans('cart.text_heading'),
            'curren_currency' => webus_shop::get_currency(),
            'total' => webus_shop::calculate_price(Cart::total()),
            'products' => self::get_cart()
        ]);
    }

    public function add(Request $request){
        $product_quantity = 1;
        $product_id = $request->product_id;
        if(isset($request->product_quantity)){
            $product_quantity = $request->product_quantity;
        }
        if(!empty($product_id)){
            $product_price = Products::price($product_id);
            $product_name = Products::name($product_id);
            if($product_price != 0 && !empty($product_name)){
                Cart::add($product_id, $product_name, $product_quantity, number_format($product_price, 2, '.', ''));
            }
        }
        if($request->ajax()){
            return response()->json([
                'text' => 'Succes',
                'count' => Cart::count()
            ]);
        }else{
            return redirect('/cart');
        }
    }

    public function update(Request $request){
        $product_total_price = array();
        if(isset($request->product_key) && isset($request->product_quantity)){
            $get_product = Cart::get($request->product_key);
            Cart::update($request->product_key, $request->product_quantity);
            if (Cart::count() == 0){
                $response = array(
                    'code' => 'empty-cart',
                    'text_empty_cart' => trans('cart.text_empty_cart')
                );
            }elseif(Cart::count() > 0 && $request->product_quantity == 0){
                $response = array(
                    'code' => 'remove-product'
                );
            }else{
                $product_total_price = $get_product->price*$get_product->qty;
                $response = array(
                    'code' => 'update-product',
                    'product_total_price' => webus_shop::calculate_price($product_total_price)
                );
            }

        }else{
            $response = 'error';
        }

        $response['count'] = Cart::count();
        $response['total'] = webus_shop::calculate_price(Cart::total());
        $response['currency'] = webus_shop::get_currency();

        return response()->json($response);
    }

    public function remove(Request $request){
        if(isset($request->product_key) && !empty($request->product_key)){
            Cart::remove($request->product_key);
            if (Cart::count() == 0){
                $response = array(
                    'code' => 'empty-cart',
                    'text_empty_cart' => trans('cart.text_empty_cart')
                );
            }else{
                $response = array(
                    'code' => 'remove-product'
                );
            }

        }else{
            $response = array(
                'code' => 'error-product'
            );
        }

        $response['count'] = Cart::count();
        $response['total'] = webus_shop::calculate_price(Cart::total());
        $response['currency'] = webus_shop::get_currency();

        return response()->json($response);
    }

//    public function clear(){
//        Cart::destroy();
//    }

    /**** Currency *****/

    public function change_currency_product(Request $request){
        //save cookie 1 year
        if(isset($request->currency) && isset($request->product_id)){
            $product_price = Products::price($request->product_id);
            $get_price = webus_shop::calculate_price($product_price, $request->currency);
            return response()->json(['new_price' => $get_price])->cookie('currency', $request->currency, 525601);
        }
        return response('error');
    }

    /**** Checkout *****/

    public function checkout(){
        return view('front.checkout',[
            'title' => trans('cart.text_heading_checkout')
        ]);
    }

}
