<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusModulesController;
use DB;
class CustomText2 extends WebusModulesController
{

    public function __construct()
    {

        $this->title = 'CustomText2';
        $this->module_filename = 'CustomText2';

        $this->form_fields = array( //in form template

            //Required field (no translated)
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'body' => [
                'title' => 'Content',
                'html_type' => 'body',
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => true,
            ],

            //your fields here

            'status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled'
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]

        );

    }

    static function front($module_id){
        $get = DB::table('modules')->where('id', $module_id)->where('module_status', 1)->first();
        if($get != null){
            $module = json_decode($get->setting);
            return view('front.modules.CustomText2', ['module' => $module]);
        }
    }

}


