<?php
namespace App\Http\Controllers\Modules;

use App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusModulesController;
use DB;
class Clients extends WebusModulesController
{
    const VISIBLE_ITEMS = 18;
    const VISIBLE_MOBILE_ITEMS = 6;

    public function __construct()
    {

        $this->title = 'Clients';
        $this->module_filename = 'Clients';

        $this->form_fields = array( //in form template

            //Required field (no translated)
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            //your fields here

            'status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled'
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]

        );

    }

    static function front($module_id){
        $get = DB::table('modules')->where('id', $module_id)->where('module_status', 1)->first();
        if($get != null){
            $clients = Client::where('post_status', 1)->get()->toArray();

            $visible = array_slice($clients, 0, self::VISIBLE_ITEMS);
            $hidden = array_slice($clients, self::VISIBLE_ITEMS, count($clients));

            $visibleMobile = array_slice($clients, 0, self::VISIBLE_MOBILE_ITEMS);
            $hiddenMobile = array_slice($clients, self::VISIBLE_MOBILE_ITEMS, count($clients));

            $module = json_decode($get->setting);
            return view('front.modules.Clients', [
                'module' => $module,
                'visible' => $visible,
                'hidden' => $hidden,
                'visibleMobile' => $visibleMobile,
                'hiddenMobile' => $hiddenMobile,
            ]);
        }
    }

}


