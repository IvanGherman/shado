<?php
namespace App\Http\Controllers\Modules;

use App\Category;
use App\CustomMenu;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusModulesController;
use DB;
class Catalog extends WebusModulesController
{

    public function __construct()
    {

        $this->title = 'Catalog';
        $this->module_filename = 'Catalog';

        $this->form_fields = array( //in form template

            //Required field (no translated)
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            //your fields here

            'status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled'
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]

        );

    }

    static function front($module_id){
        $get = DB::table('modules')->where('id', $module_id)->where('module_status', 1)->first();
        if($get != null){
            $products = Product::where('post_status', '=', 1)
                ->where('show_in_catalog', '=', 1)
                ->orderBy('order_in_catalog', 'ASC')
                ->skip(0)
                ->take(12)
                ->get();
            $categories = Category::all();
            $module = json_decode($get->setting);

            $customMenu = CustomMenu::all();

            return view('front.modules.Catalog', [
                'module' => $module,
                'products' => $products,
                'categories' => $categories,
                'customMenu' => $customMenu,
            ]);
        }
    }

}


