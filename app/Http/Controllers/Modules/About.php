<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\WebusModels\Modules;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusModulesController;
use DB;
class About extends WebusModulesController
{

    public function __construct()
    {

        $this->title = 'About';
        $this->module_filename = 'About';

        $this->form_fields = array( //in form template

            //Required field (no translated)
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            'title' => [
                'title' => 'Title',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            'description' => [
                'title' => 'Description',
                'html_type' => 'body',
                'validate' => '',
                'custom_class' => '',
                'id' => '',
                'translatable' => true
            ],

            'images' => [
                'title' => 'Images',
                'html_type' => 'repeatable',
                'start_with' => 0,
                'max' => 50,
                'options' => array(
                    'image' => array(
                        'title' => 'Image',
                        'html_type' => 'image_input',
                        'validate' => false,
                        'translatable' => false,
                        'custom_class' => 'col-md-12'
                    )
                ),
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false,
                'metabox' => true,
                'position' => 'content'
            ],

            //your fields here

            'status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled'
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]

        );

    }

    static function front($module_id){
        $get = DB::table('modules')->where('id', $module_id)->where('module_status', 1)->first();
        if($get != null){
            $module = json_decode($get->setting);
            return view('front.modules.About', ['module' => $module]);
        }
    }
}


