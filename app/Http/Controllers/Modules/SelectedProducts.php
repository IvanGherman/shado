<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\SelectedProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusModulesController;
use DB;
class SelectedProducts extends WebusModulesController
{

    public function __construct()
    {

        $this->title = 'SelectedProducts';
        $this->module_filename = 'SelectedProducts';

        $this->form_fields = array( //in form template

            //Required field (no translated)
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            //your fields here

            'status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled'
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]

        );

    }

    static function front($module_id){
        $get = DB::table('modules')->where('id', $module_id)->where('module_status', 1)->first();

        $selected = SelectedProduct::all();
        if($get != null){
            $module = json_decode($get->setting);
            return view('front.modules.SelectedProducts', [
                'module' => $module,
                'selected' => $selected,
            ]);
        }
    }

}


