<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Webus\WebusModulesController;
use DB;
class News extends WebusModulesController
{

    public function __construct()
    {

        $this->title = 'News';
        $this->module_filename = 'News';

        $this->form_fields = array( //in form template

            //Required field (no translated)
            'name' => [
                'title' => 'Name',
                'html_type' => 'text',
                'validate' => 'required',
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ],

            //your fields here

            'status' => [
                'title' => 'Status',
                'html_type' => 'select',
                'options' => array(
                    '1' => 'Enabled',
                    '0' => 'Disabled'
                ),
                'multiple' => false,
                'validate' => false,
                'custom_class' => '',
                'id' => '',
                'translatable' => false
            ]

        );

    }

    static function front($module_id){
        $get = DB::table('modules')->where('id', $module_id)->where('module_status', 1)->first();
        if($get != null){
            $query = \App\News::where('post_status', 1)
                ->orderBy('created_at', 'DESC')
                ->skip(0)
                ->take(3)
            ;

            $news = $query->get();

            $module = json_decode($get->setting);
            return view('front.modules.News', [
                'module' => $module,
                'items' => $news,
            ]);
        }
    }

}


