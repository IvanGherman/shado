<?php

namespace App\Http\Middleware;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Make sure current locale exists.
        $locale = $request->segment(1);
        $segments = $request->segments();

        if (in_array($locale, ['lang'])) {
            return $next($request);
        }

        if (in_array($locale, ['admin'])) {
            return $next($request);
        }

        if ( ! array_key_exists($locale, app()->config->get('laravellocalization.supportedLocales'))) {
            if (session()->has('locale')) {
                $locale = session()->get('locale');
                app()->setLocale($locale);
                return redirect()->to('/'.$locale.'/'.implode('/', $segments));
            } else {
                $segments = $request->segments();
                $segments[0] = app()->config->get('app.fallback_locale');
                session()->put('locale', app()->config->get('app.fallback_locale'));
                return redirect()->to(implode('/', $segments));
            }
        }

        if (session()->get('locale') !== $locale) {
            $locale = session()->get('locale');
            app()->setLocale($locale);
            $segments[0] = $locale;
            return redirect()->to(implode('/', $segments));
        }

        app()->setLocale($locale);

        return $next($request);
    }
}