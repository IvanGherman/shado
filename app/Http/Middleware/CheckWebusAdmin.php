<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use App\User;

use jeremykenedy\LaravelRoles\Models\Role;

class CheckWebusAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       // return $next($request);

        if (!Auth::guest()) {
            //$user = User::model('User')->find(Auth::id());
            $user = User::find(Auth::id());
            //Checking For Roles
            return $user->hasRole('admin') ? $next($request) : redirect('/');
        }

        return redirect(route('admin_login'));

    }
}
