<?php

namespace App\Http\Middleware;

use Closure;

class PhoneChanger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($_GET['utm_source'])) {
            if($_GET['utm_source']=='facebook' && $_GET['utm_medium']=='refer'){
                setcookie('facebook','1', strtotime( '+30 days' ),'/' );
                $_COOKIE['facebook'] = true;
                setcookie('999',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['999'] = null;
                setcookie('instagram',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['instagram'] = null;
                setcookie('google',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['google'] = null;
            }
            elseif($_GET['utm_source']=='instagram' && $_GET['utm_medium']=='refer'){
                setcookie('instagram','1', strtotime( '+30 days' ), '/' );
                $_COOKIE['instagram'] = true;
                setcookie('facebook',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['facebook'] = null;
                setcookie('999',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['999'] = null;
                setcookie('google',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['google'] = null;
            }
            elseif($_GET['utm_source']=='google' && $_GET['utm_medium']=='cpc'){
                setcookie('google','1', strtotime( '+30 days' ),'/' );
                $_COOKIE['google'] = true;
                setcookie('instagram',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['instagram'] = null;
                setcookie('facebook',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['facebook'] = null;
                setcookie('999',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['999'] = null;
            }
            elseif($_GET['utm_source']=='999' && $_GET['utm_medium']=='refer'){
                setcookie('999','1', strtotime( '+30 days' ), '/' );
                $_COOKIE['999'] = true;
                setcookie('google',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['google'] = null;
                setcookie('instagram',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['instagram'] = null;
                setcookie('facebook',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['facebook'] = null;
            }
            elseif($_GET['utm_source']=='allbiz' && $_GET['utm_medium']=='refer'){
                setcookie('999','1', strtotime( '+30 days' ), '/' );
                $_COOKIE['999'] = null;
                setcookie('google',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['google'] = null;
                setcookie('instagram',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['instagram'] = null;
                setcookie('facebook',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['facebook'] = null;
                setcookie('prom',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['prom'] = null;
            }
            elseif($_GET['utm_source']=='prom' && $_GET['utm_medium']=='refer'){
                setcookie('999','1', strtotime( '+30 days' ), '/' );
                $_COOKIE['999'] = null;
                setcookie('google',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['google'] = null;
                setcookie('instagram',null, strtotime( '+30 days' ), '/' );
                $_COOKIE['instagram'] = null;
                setcookie('facebook',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['facebook'] = null;
                setcookie('allbiz',null, strtotime( '+30 days' ),'/' );
                $_COOKIE['allbiz'] = null;
            }
        }

        return $next($request);
    }
}
