<?php

/**********************************
Register your own shortcodes
 * this function use helper Shortcodes (from Wordpress), do not delete this file and Shortcodes.php file
/*********************************/


self::add_shortcode( 'gallery', function ($atts, $content = null){
    if (is_string($atts)) {
        $atts = ['identificator' => $atts];
    }
    $atts['identificator'] = !empty($atts['identificator']) ? $atts['identificator'] : $content;

    $gallery = \App\Galleries::where('identificator', $atts['identificator'])->first();

    if (!$gallery) {
        return '';
    }

    $images = unserialize($gallery->gallery);

    return view('front.partial.gallery', ['images' => $images]);
});

self::add_shortcode( 'custom_button', function ($atts, $content = null){
    return view('front.partial.button', [
        'url' => $atts['url'],
        'title' => $content,
    ]);
});

self::add_shortcode( 'custom_blockquote', function ($atts, $content = null){
//    var_dump($atts);exit;
    return '<blockquote>'.$content.'</blockquote>';
});

self::add_shortcode( 'one_half', function ($atts, $content = null){
    return '<div class="grid_6">'.shortcodes::do_shortcode($content).'</div>';
});


self::add_shortcode( 'divider', function ($atts, $content = null){
    return '<div class="clear"></div><div class="divider"></div>';
});

self::add_shortcode( 'video', function ($atts, $content = null){
    $id = str_random();
    $atts = self::shortcode_atts( array(
        'id' => '',
        'image' => '',
        'title' => '',
    ), $atts );

    return view('front.partial.custom_video', [
        'id' => $atts['id'],
        'preview' => $atts['image'],
        'title' => $atts['title'],
    ]);
});

