<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends CustomModel
{
    protected $table = 'categories';

    public function getInstallation()
    {
        return $this->getShortcoded($this->translate($this->installation));
    }
}
