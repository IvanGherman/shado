<?php

namespace App\WebusShop;

use Illuminate\Database\Eloquent\Model;

class OrdersProducts extends Model
{
    protected $table = 'orders_products';

    public $timestamps = false;
    
    static function add($order_id, $products, $currency){
        if($products != null){
            foreach ($products as $product){
                $add_product_to_order = new OrdersProducts();
                $add_product_to_order->product_id = $product['id'];
                $add_product_to_order->order_id = $order_id;
                $add_product_to_order->name = $product['name'];
                $add_product_to_order->quantity = $product['quantity'];
                $add_product_to_order->price = $product['price'];
                $add_product_to_order->currency = $currency;
                $add_product_to_order->save();
            }
        }
    }
    
    static function update_products($order_id, $products, $currency){
        $remove = OrdersProducts::where('order_id', $order_id)->delete();
        if($products != null){
            foreach ($products as $product){
                $add_product_to_order = new OrdersProducts();
                $add_product_to_order->product_id = $product['id'];
                $add_product_to_order->order_id = $order_id;
                $add_product_to_order->name = $product['name'];
                $add_product_to_order->quantity = $product['quantity'];
                $add_product_to_order->price = $product['price'];
                $add_product_to_order->currency = $currency;
                $add_product_to_order->save();
            }
        }
    }
}
