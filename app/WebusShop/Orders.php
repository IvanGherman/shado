<?php

namespace App\WebusShop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class Orders extends Model
{
  protected $table = 'orders';
  
  static function add($order_info){
    $add_order = new Orders;
    $add_order->user_id = Auth::id();
    $add_order->order_status = 0; 
    $add_order->customer_name = Auth::user()->name;
    $add_order->address_name = $order_info['address_name'];
    $add_order->address_street = $order_info['address_street'];
    $add_order->address_index = $order_info['address_index'];
    $add_order->address_country = $order_info['address_country'];
    $add_order->address_city = $order_info['address_city'];
    $add_order->address_phone = $order_info['address_phone'];
    $add_order->method_payment = $order_info['method_payment'];
    $add_order->sub_total = $order_info['sub_total'];
    $add_order->total = $order_info['total'];
    $add_order->totals = $order_info['totals'];
    $add_order->currency = $order_info['currency'];
    $add_order->save();
    
    return $add_order->id;
  }
  
  static function update_order($order_id, $order_info){
    $update_order = Orders::find($order_id);
    $update_order->user_id = Auth::id();
    $update_order->order_status = 0;
    $update_order->customer_name = Auth::user()->name;
    $update_order->address_name = $order_info['address_name'];
    $update_order->address_street = $order_info['address_street'];
    $update_order->address_index = $order_info['address_index'];
    $update_order->address_country = $order_info['address_country'];
    $update_order->address_city = $order_info['address_city'];
    $update_order->method_payment = $order_info['method_payment'];
    $update_order->sub_total = $order_info['sub_total'];
    $update_order->total = $order_info['total'];
    $update_order->totals = $order_info['totals'];
    $update_order->currency = $order_info['currency'];
    $update_order->save();

    return $update_order->id;
  }

  public function products(){
    return $this->hasMany('App\WebusShop\OrdersProducts', 'order_id', 'id');
  }

//  public function country(){
//    return $this->hasOne('App\WebusModels\TaxonomyTerms', 'id', 'country_id')->where('taxonomy', 'countries')->select(['id', 'name']);
//  }
}
