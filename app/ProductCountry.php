<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCountry extends Model
{
    protected $table = 'product_countries';
}
