<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreeType extends Model
{
    protected $table = 'tree_types';
}
