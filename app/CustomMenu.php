<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomMenu extends Model
{
    protected $table = 'custom_menu';
}
