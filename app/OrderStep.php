<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStep extends Model
{
    protected $table = 'order_steps';
}
