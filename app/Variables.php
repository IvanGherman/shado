<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variables extends Model
{
    public static function changePhone($variables, $number = 1)
    {
        if (isset($_COOKIE['facebook'])) {
            if ($number === 1) {
                if (!property_exists($variables, 'facebook_phone')) {
                    return '';
                }
                return $variables->facebook_phone;
            } else {
                if (!property_exists($variables, 'facebook_phone2')) {
                    return '';
                }
                return $variables->facebook_phone2;
            }
        } elseif (isset($_COOKIE['instagram'])) {
            if ($number === 1) {
                if (!property_exists($variables, 'instagram_phone')) {
                    return '';
                }
                return $variables->instagram_phone;
            } else {
                if (!property_exists($variables, 'instagram_phone2')) {
                    return '';
                }
                return $variables->instagram_phone2;
            }
        } elseif (isset($_COOKIE['google'])) {
            if ($number === 1) {
                if (!property_exists($variables, 'google_phone')) {
                    return '';
                }
                return $variables->google_phone;
            } else {
                if (!property_exists($variables, 'google_phone2')) {
                    return '';
                }
                return $variables->google_phone2;
            }
        } elseif (isset($_COOKIE['999'])) {
            if ($number === 1) {
                if (!property_exists($variables, '_999_phone')) {
                    return '';
                }
                return $variables->_999_phone;
            } else {
                if (!property_exists($variables, '_999_phone2')) {
                    return '';
                }
                return $variables->_999_phone2;
            }
        } elseif (isset($_COOKIE['allbiz'])) {
            if ($number === 1) {
                if (!property_exists($variables, 'allbiz_phone')) {
                    return '';
                }
                return $variables->allbiz_phone;
            } else {
                if (!property_exists($variables, 'allbiz_phone2')) {
                    return '';
                }
                return $variables->allbiz_phone2;
            }
        } elseif (isset($_COOKIE['prom'])) {
            if ($number === 1) {
                if (!property_exists($variables, 'prom_phone')) {
                    return '';
                }
                return $variables->prom_phone;
            } else {
                if (!property_exists($variables, 'prom_phone2')) {
                    return '';
                }
                return $variables->prom_phone2;
            }
        } else {
            if ($number === 1) {
                if (!property_exists($variables, 'phone')) {
                    return '';
                }
                return $variables->phone;
            } else {
                if (!property_exists($variables, 'phone2')) {
                    return '';
                }
                return $variables->phone2;
            }
        }
    }
}
