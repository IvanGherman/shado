<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Product extends Model
{
    const DEFAULT_IMAGE = '/assets/img/no_image.svg';

    protected $table = 'products';

    public $formattedImages = null;

    public function countries()
    {
        return $this->belongsToMany('App\Country', 'product_countries');
    }

    public function getCountries($locale)
    {
        $countries = ProductCountry::where('product_id', '=', $this->getId())->get();

        $items = [];
        foreach ($countries as $item) {
            $country = Country::find($item->country_id);
            $items[] = \App\Http\Helpers\webus_help::translate_manual($locale, $country->title);
        }

        return !empty($items) ? implode(', ', $items) : '';
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'product_categories');
    }

    public function colors()
    {
        return $this->belongsToMany('App\Color', 'product_colors');
    }

    public function getColors($locale)
    {
        $textures = ProductColor::where('product_id', '=', $this->getId())->get();

        $items = [];
        foreach ($textures as $item) {
            $color = Color::find($item->color_id);
            $items[] = \App\Http\Helpers\webus_help::translate_manual($locale, $color->title);
        }

        return !empty($items) ? implode(', ', $items) : '';
    }

    public function textures()
    {
        return $this->belongsToMany('App\Texture', 'product_textures', 'product_id', 'texture_type_id');
    }

    public function getTextures($locale)
    {
        $textures = ProductTexture::where('product_id', '=', $this->getId())->get();

        $items = [];
        foreach ($textures as $item) {
            $texture = Texture::find($item->texture_type_id);
            $items[] = \App\Http\Helpers\webus_help::translate_manual($locale, $texture->title);
        }

        return !empty($items) ? implode(', ', $items) : '';
    }

    public function popular_products()
    {
        return $this->belongsToMany('App\Product', 'popular_products', 'product_id', 'popular_product_id');
    }

    public function common_products()
    {
        return $this->belongsToMany('App\Product', 'common_products', 'product_id', 'common_product_id');
    }

    public function transparency()
    {
        return $this->hasOne('App\Transparency', 'id', 'transparency_id');
    }

    public function values()
    {
        return $this->hasMany('App\ProductAttributeValue', 'product_id', 'id');
    }

    public function getValues()
    {
        return ProductAttributeValue::where('product_id', '=', $this->getId())->get();
    }

    public function getImages()
    {
        if (is_array($this->formattedImages)) {
            return $this->formattedImages;
        }

        $images = json_decode($this->images);

        $formatted = [];
        if (!empty($images)) {
            foreach ($images as $image) {
                $formatted[] = $image->image;
            }
        }

        $this->formattedImages = $formatted;

        return $formatted;
    }

    public function getFirstImage()
    {
        if (!is_array($this->formattedImages)) {
            $this->getImages();
        }

        if (!empty($this->formattedImages) && file_exists(public_path().$this->formattedImages[0])) {
            return $this->formattedImages[0];
        }

        return self::DEFAULT_IMAGE;
    }

    public static function getAssetOrDefault($image)
    {
        if (file_exists(public_path().$image)) {
            return asset($image);
        }

        return self::DEFAULT_IMAGE;
    }

    public function getModelImage()
    {
        if (!empty($this->model_image) && file_exists(public_path().$this->model_image)) {
            return $this->model_image;
        }

        return self::DEFAULT_IMAGE;
    }

    public function getByModel()
    {
        $productsByModel = Product::where('model', $this->model)
            ->get()
            ;

        return $productsByModel;
    }

    public function getColor()
    {
        if ($color = $this->colors->first()) {
            return $color;
        }

        return null;
    }

    public function getColorTitle()
    {
        if ($color = $this->getColor()) {
            return $this->translate($color->title);
        }

        return '';
    }

    public function translate($text)
    {
        $locale = Config::get('app.locale');
        return \App\Http\Helpers\webus_help::translate_manual($locale, $text);
    }

    public function getImploded($field, $locale)
    {
        $product = Product::find($this->getId());

        $items = [];
        foreach ($product->$field as $item) {
            $items[] = \App\Http\Helpers\webus_help::translate_manual($locale, $item->title);
        }

        return !empty($items) ? implode(', ', $items) : '';
    }

    public function getId()
    {
        return !empty($this->product_id) ? $this->product_id : $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }
}
