<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeValue extends Model
{
    protected $table = 'product_attribute_values';

    public function attribute()
    {
        return $this->belongsTo('App\Attribute', 'attribute_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

    public function attributeValue()
    {
        return $this->belongsTo('App\AttributeValue', 'attribute_value_id', 'id');
    }
}
