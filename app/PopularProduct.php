<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopularProduct extends Model
{
    protected $table = 'popular_products';
}
